Implementation of the Paper

Sunanda Bose and Nandini Mukherjee (2020). “SenSchedule: Scheduling Heterogeneous Periodic Sensing Resources with Non Uniform Performance in IoT”. In: IEEE Transactions on Services Computing 15.4.
https://ieeexplore.ieee.org/abstract/document/9187904

This project simulates resource scheduling algorithms proposed in the above mentioned paper. The optimization problem is solved using [Mathematica++](https://gitlab.com/neel.basu/mathematicapp) library that provides interoperability between C++ and [Mathematica](https://www.wolfram.com/mathematica/). For 3D visualization [VTK](https://vtk.org) library has been used.

### SenSchedule Simulation Demonstration (https://www.youtube.com/watch?v=DZVy3vIRvsg)
[![SenSchedule Simulation Demonstration](https://img.youtube.com/vi/DZVy3vIRvsg/0.jpg)](https://www.youtube.com/watch?v=DZVy3vIRvsg)

