#include "capabilitydialog.h"
#include "ui_capabilitydialog.h"
#include <string>
#include <boost/math/special_functions/prime.hpp>

scheduler::CapabilityDialog::CapabilityDialog(const std::vector<scheduler::capabilitiy_ptr>& capabilities, QWidget *parent): QDialog(parent), _capabilities(capabilities), ui(new Ui::CapabilityDialog){
    ui->setupUi(this);
    boost::uint32_t prime = boost::math::prime(_capabilities.size());
    ui->primeEdit->setText(QString::number(prime));
}

scheduler::CapabilityDialog::~CapabilityDialog(){
    delete ui;
}

scheduler::capabilitiy_ptr scheduler::CapabilityDialog::capability() const{
    QString name = ui->nameEdit->text();
    scheduler::capabilitiy_ptr cap(new scheduler::capability(name.toStdString()));
    cap->prime = ui->primeEdit->text().toLongLong();
    cap->initial.min = ui->fromEdit->text().toInt();
    cap->initial.max = ui->toEdit->text().toInt();
    cap->restrictive = ui->restrictiveCheckBox->isChecked();
    return cap;
}
