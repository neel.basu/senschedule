#include "reviewwidget.h"
#include "ui_reviewwidget.h"
#include "resourceframe.h"
#include "requestframe.h"
#include <QFileDialog>
#include "mathematica++/m.h"
#include "mathematica++/io.h"
#include "mathematica++/declares.h"
#include <QDir>

MATHEMATICA_DECLARE(Association)
MATHEMATICA_DECLARE(Resource)
MATHEMATICA_DECLARE(Request)
MATHEMATICA_DECLARE(ResourceJSON)
MATHEMATICA_DECLARE(RequestJSON)
MATHEMATICA_DECLARE(ScalarizeF)
MATHEMATICA_DECLARE(ScheduleSlices)

scheduler::ReviewWidget::ReviewWidget(QWidget *parent): QWidget(parent), ui(new Ui::ReviewWidget){
    ui->setupUi(this);
    _resources_container_widget = new QWidget(this);
    _requests_container_widget  = new QWidget(this);

    _resources_layout = new QHBoxLayout(this);
    _requests_layout  = new QHBoxLayout(this);

    _resources_container_widget->setLayout(_resources_layout);
    _requests_container_widget->setLayout(_requests_layout);
    
    ui->resourcesScrollArea->setWidget(_resources_container_widget);
    ui->requestsScrollArea->setWidget(_requests_container_widget);
    
    connect(ui->backBtn, SIGNAL(clicked()), this, SIGNAL(backClicked()));
    connect(ui->solveBtn, SIGNAL(clicked()), this, SIGNAL(solveClicked()));
    connect(ui->generateSolverBtn, SIGNAL(clicked()), this, SIGNAL(generateSolverClicked()));

    ui->cdfPathEdit->setText(QDir::current().filePath("visualization.cdf"));
    ui->graphicsPathEdit->setText(QDir::current().filePath("graphics.obj"));
    ui->symEdit->setText(QDir::current().filePath("symbols.mx"));
    
    connect(ui->cdfBrowseBtn, &QPushButton::clicked, [this](bool){
        QString cdf_path = QFileDialog::getSaveFileName(this, tr("Save File"), ".", tr("CDF (*.cdf)"));
        ui->cdfPathEdit->setText(cdf_path);
    });
    connect(ui->graphicsBrowseBtn, &QPushButton::clicked, [this](bool){
        QString graphics_path = QFileDialog::getSaveFileName(this, tr("Save File"), ".", tr("CDF (*.obj)"));
        ui->graphicsPathEdit->setText(graphics_path);
    });
    connect(ui->symBrowseBtn, &QPushButton::clicked, [this](bool){
        QString sym_path = QFileDialog::getSaveFileName(this, tr("Save File"), ".", tr("CDF (*.mx)"));
        ui->symEdit->setText(sym_path);
    });
}

scheduler::ReviewWidget::~ReviewWidget(){
    delete ui;
}

void scheduler::ReviewWidget::addResource(const QJsonObject& json){
    ResourceFrame* resource_frame = new ResourceFrame(this);
    resource_frame->load(json);
    _resources_layout->addWidget(resource_frame);
}

void scheduler::ReviewWidget::addRequest(const QJsonObject& json){
    RequestFrame* request_frame = new RequestFrame(this);
    request_frame->load(json);
    _requests_layout->addWidget(request_frame);
}

QString scheduler::ReviewWidget::cdf() const{
    return ui->cdfPathEdit->text();
}
QString scheduler::ReviewWidget::graphics() const{
    return ui->graphicsPathEdit->text();
}
QString scheduler::ReviewWidget::sym() const{
    return ui->symEdit->text();;
}

double scheduler::ReviewWidget::epsilon() const{
    return ui->epsilonEdit->text().toDouble();
}

int scheduler::ReviewWidget::faultTolerance() const{
    return ui->faultTolerance->value();
}
