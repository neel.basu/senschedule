#ifndef REQUESTFRAME_H
#define REQUESTFRAME_H

#include <QFrame>
#include <QJsonObject>

namespace Ui {
class RequestFrame;
}

namespace scheduler{

class RequestFrame : public QFrame{
  Q_OBJECT
  public:
    explicit RequestFrame(QWidget *parent = 0);
    ~RequestFrame();
  private:
    Ui::RequestFrame *ui;
  public:
    void load(const QJsonObject& document);
};
    
}

#endif // REQUESTFRAME_H
