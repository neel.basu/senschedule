#include "capabilitiesmodel.h"
#include <QString>
#include <string>

scheduler::CapabilitiesModel::CapabilitiesModel(std::vector<capabilitiy_ptr>& capabilities, QObject *parent): QAbstractListModel(parent), _capabilities(capabilities){
}

QVariant scheduler::CapabilitiesModel::headerData(int section, Qt::Orientation orientation, int role) const{
    return "name";
}

int scheduler::CapabilitiesModel::rowCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return _capabilities.size();
}

QVariant scheduler::CapabilitiesModel::data(const QModelIndex &index, int role) const{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= _capabilities.size())
        return QVariant();
    
    if(role == Qt::DisplayRole){
        return QString("%1 <%2> (%3)").arg(QString::fromStdString(_capabilities[index.row()]->label)).arg(_capabilities[index.row()]->prime).arg(_capabilities[index.row()]->restrictive);
    }
    
    return QVariant();
}

void scheduler::CapabilitiesModel::addCapability(scheduler::capabilitiy_ptr capability){
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _capabilities.push_back(capability);
    endInsertRows();
}

scheduler::capabilitiy_ptr scheduler::CapabilitiesModel::capability(int index) const{
    return _capabilities[index];
}
