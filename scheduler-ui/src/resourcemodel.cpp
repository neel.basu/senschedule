#include "resourcemodel.h"
#include <QPixmap>
#include <QPainter>

scheduler::ResourceModel::ResourceModel(std::vector<resource_ptr>& resources, QObject *parent): QAbstractTableModel(parent), _resources(resources){

}

QVariant scheduler::ResourceModel::headerData(int section, Qt::Orientation orientation, int role) const{
    if (role != Qt::DisplayRole)
        return QVariant();
    if(orientation == Qt::Horizontal){
        if(section == 0)
            return "type";
        else if(section == 1)
            return "observations";
        else return "";
    }else{
        return section;
    }
}

int scheduler::ResourceModel::rowCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return _resources.size();
}

int scheduler::ResourceModel::columnCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return 2;
}

QVariant scheduler::ResourceModel::data(const QModelIndex &index, int role) const{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= _resources.size())
        return QVariant();

    scheduler::resource_ptr res = _resources[index.row()];
    if(role == Qt::DisplayRole){
        if(index.column() == 0){
            return QString("%1").arg(QString::fromStdString(res->_type->label));
        }else if(index.column() == 1){
            return QString::number(res->_observations.size());
        }
    }else if(role == Qt::DecorationRole){
        if(index.column() == 0){
            QColor color(Qt::darkYellow);
            if(res->capabilities_complete()){
                color = Qt::darkGreen;
            }
            if(res->capabilities_computed()){
                color = Qt::darkBlue;
            }
            if(res->partiions_computed()){
                color = Qt::darkRed;
            }
            
            QPixmap icon(16, 16);
            icon.fill(Qt::transparent);
            QPainter painter(&icon);
            painter.setRenderHint(QPainter::Antialiasing);
            painter.setPen(color);
            painter.setBrush(QBrush(color));
            painter.drawEllipse(QRectF(0, 0, 16, 16));
            return icon;
        }
    }
    return QVariant();
}

void scheduler::ResourceModel::addResource(scheduler::resource_ptr res){
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _resources.push_back(res);
    endInsertRows();
}

void scheduler::ResourceModel::removeRequest(scheduler::resource_ptr res){
    int row = -1;
    for(int i = 0; i < _resources.size(); ++i){
        if(_resources[i] == res){
            row = i;
        }
    }
    
    if(row < 0){
        return;
    }
    
    beginRemoveRows(QModelIndex(), row, row);
    _resources.erase(_resources.begin() + row);
    endRemoveRows();
}


scheduler::resource_ptr scheduler::ResourceModel::resource(unsigned int row){
    return _resources[row];
}

void scheduler::ResourceModel::updateResource(int row, scheduler::resource_ptr res){
    QModelIndex index_begin = createIndex(row, 0);
    QModelIndex index_end   = createIndex(row, columnCount());
    _resources[row] = res;
    emit dataChanged(index_begin, index_end);
}

void scheduler::ResourceModel::reset(){
    beginResetModel();
    endResetModel();
}
