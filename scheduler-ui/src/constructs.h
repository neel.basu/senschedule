/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CONSTRUCTS_H
#define CONSTRUCTS_H

#include <set>
#include <map>
#include <list>
#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>
#include "mathematica++/connector.h"
#include "mathematica++/m.h"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/version.hpp>

namespace scheduler{

struct capability_value;

struct capability{
    struct range{
        unsigned int min;
        unsigned int max;
        
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version){
            ar & BOOST_SERIALIZATION_NVP(min);
            ar & BOOST_SERIALIZATION_NVP(max);
        }
    };
    std::string   label;
    unsigned int  id;
    unsigned long prime;
    range         initial;
    bool          restrictive;
    
    capability(const std::string& label, unsigned int id=0);
    
  private:
    capability(): restrictive(false){}
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        ar & BOOST_SERIALIZATION_NVP(label);
        ar & BOOST_SERIALIZATION_NVP(id);
        ar & BOOST_SERIALIZATION_NVP(prime);
        ar & BOOST_SERIALIZATION_NVP(initial);
        if(version > 0){
            ar & BOOST_SERIALIZATION_NVP(restrictive);
        }
    }
};

typedef boost::shared_ptr<capability> capabilitiy_ptr;

struct box_type{
    typedef std::set<capabilitiy_ptr> capabilities_collection_type;
    
    std::string  label;
    std::string  name;
    unsigned int id;
    
    capabilities_collection_type _capabilities;
    
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        ar & BOOST_SERIALIZATION_NVP(label);
        ar & BOOST_SERIALIZATION_NVP(name);
        ar & BOOST_SERIALIZATION_NVP(id);
        ar & BOOST_SERIALIZATION_NVP(_capabilities);
    }
};

typedef boost::shared_ptr<box_type> box_type_ptr;

struct capability_organizer{
    typedef std::map<capabilitiy_ptr, capability::range> capability_values_collection_type;
    
    box_type_ptr                      _type;
    capability_values_collection_type _capability_values;
    std::string                       _capabilities_hash;
    unsigned long long                _hash_base;
    unsigned long long                _hash_updated;
    
    capability_organizer(box_type_ptr type);
    bool set_capability(capabilitiy_ptr key, const capability::range& value);
    mathematica::m capabilities() const;
    bool capabilities_complete() const;
    void compute_hash(mathematica::connector& mathematica);
    bool capabilities_computed() const;
    
  protected:
    capability_organizer(){}
    capability_organizer(const capability_organizer& other);
  private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        ar & BOOST_SERIALIZATION_NVP(_type);
        ar & BOOST_SERIALIZATION_NVP(_capability_values);
        ar & BOOST_SERIALIZATION_NVP(_capabilities_hash);
        ar & BOOST_SERIALIZATION_NVP(_hash_base);
        ar & BOOST_SERIALIZATION_NVP(_hash_updated);
    }
};

struct resource: public capability_organizer{
  typedef std::vector<int> observation_type;
  typedef std::list<observation_type> observations_collection_type;
  typedef std::map<capabilitiy_ptr, capability::range> capability_values_collection_type;
  typedef std::vector<std::vector<double>> matrix_type;

  struct resource_availability{
      unsigned int start;
      unsigned int delta;
      unsigned int recurrence;
      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version){
          ar & BOOST_SERIALIZATION_NVP(start);
          ar & BOOST_SERIALIZATION_NVP(delta);
          ar & BOOST_SERIALIZATION_NVP(recurrence);
      }
  };
  public:
    box_type_ptr                 _type;
    int                          _context;
    resource_availability        _availability;
    observations_collection_type _observations;
    
    bool                         _partitions_computed;
    std::string                  _partitions_key;
    matrix_type                  _partitions_matrix;
  public:
    void compute();
    bool partiions_computed() const;
  public:
    resource(box_type_ptr type);
    template <typename It>
    void addObservation(It begin, It end){
        observation_type observation;
        for(It i = begin; i != end; ++i){
            observation.push_back(observation_type::value_type(*i));
        }
        _observations.push_back(observation);
    }
    boost::shared_ptr<resource> clone() const;
  private:
    resource(const resource& other);
    
    resource(){}
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        ar & boost::serialization::make_nvp("capability_organizer", boost::serialization::base_object<capability_organizer>(*this));
        ar & BOOST_SERIALIZATION_NVP(_type);
        ar & BOOST_SERIALIZATION_NVP(_context);
        ar & BOOST_SERIALIZATION_NVP(_availability);
        ar & BOOST_SERIALIZATION_NVP(_observations);
        ar & BOOST_SERIALIZATION_NVP(_partitions_computed);
        ar & BOOST_SERIALIZATION_NVP(_partitions_key);
        ar & BOOST_SERIALIZATION_NVP(_partitions_matrix);
    }
};

typedef boost::shared_ptr<resource> resource_ptr;

struct request: public capability_organizer{
  typedef std::map<capabilitiy_ptr, capability::range> capability_values_collection_type;
    
  struct request_span{
      unsigned int start;
      unsigned int delta;
      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int version){
          ar & BOOST_SERIALIZATION_NVP(start);
          ar & BOOST_SERIALIZATION_NVP(delta);
      }
  };
  public:
    box_type_ptr                 _type;
    int                          _context;
    request_span                 _span;
    bool                         _computed;
  public:
    void compute();
    bool computed() const;
    request(box_type_ptr type);
    boost::shared_ptr<request> clone() const;
  private:
    request(){}
    request(const request& other);
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        ar & boost::serialization::make_nvp("capability_organizer", boost::serialization::base_object<capability_organizer>(*this));
        ar & BOOST_SERIALIZATION_NVP(_type);
        ar & BOOST_SERIALIZATION_NVP(_context);
        ar & BOOST_SERIALIZATION_NVP(_span);
        ar & BOOST_SERIALIZATION_NVP(_computed);
    }
};

typedef boost::shared_ptr<request> request_ptr;

}

BOOST_CLASS_VERSION(scheduler::capability, 1)

#endif // CONSTRUCTS_H
