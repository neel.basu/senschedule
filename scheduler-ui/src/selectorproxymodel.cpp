#include "selectorproxymodel.h"

scheduler::SelectorProxyModel::SelectorProxyModel(){
    
}

Qt::ItemFlags scheduler::SelectorProxyModel::flags(const QModelIndex& index) const{
    Qt::ItemFlags flags = sourceModel()->flags(index);
    if(index.column() == 0){
        return flags | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable;
    }
    return flags;
}

QVariant scheduler::SelectorProxyModel::data(const QModelIndex& proxyIndex, int role) const{
    if(role == Qt::CheckStateRole && proxyIndex.column() == 0){
        std::map<int, bool>::const_iterator it = _check_map.find(proxyIndex.row());
        if(it == _check_map.end()){
            _check_map.insert(std::make_pair(proxyIndex.row(), false));
        }
        return _check_map.at(proxyIndex.row()) ? Qt::Checked : Qt::Unchecked;
    }
    return sourceModel()->data(proxyIndex, role);
}

bool scheduler::SelectorProxyModel::setData(const QModelIndex &index, const QVariant &value, int role){
    if(role == Qt::CheckStateRole && index.column() == 0){
        std::map<int, bool>::const_iterator it = _check_map.find(index.row());
        if(it == _check_map.end()){
            _check_map.insert(std::make_pair(index.row(), false));
        }
        _check_map[index.row()] = (value == Qt::Checked);
        emit dataChanged(index, index);
        return true;
    }
    return sourceModel()->setData(index, role);
}

std::vector<int> scheduler::SelectorProxyModel::checked_rows() const{
    std::vector<int> list;
    for(int i = 0; i < rowCount(); ++i){
        QModelIndex index = createIndex(i, 0);
        if(data(index, Qt::CheckStateRole) == Qt::Checked){
            list.push_back(mapToSource(index).row());
        }
    }
    return list;
}

void scheduler::SelectorProxyModel::selectAll(){
    for(int i = 0; i < rowCount(); ++i){
        QModelIndex index = createIndex(i, 0);
        setData(index, QVariant(Qt::Checked), Qt::CheckStateRole);
    }
}

void scheduler::SelectorProxyModel::selectNone(){
    for(int i = 0; i < rowCount(); ++i){
        QModelIndex index = createIndex(i, 0);
        setData(index, QVariant(Qt::Unchecked), Qt::CheckStateRole);
    }

}
