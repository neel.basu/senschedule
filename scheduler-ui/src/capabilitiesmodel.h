#ifndef CAPABILITIESMODEL_H
#define CAPABILITIESMODEL_H

#include <QAbstractListModel>
#include "registry.h"

namespace scheduler{

class CapabilitiesModel : public QAbstractListModel{
  Q_OBJECT
  std::vector<capabilitiy_ptr>& _capabilities;
  public:
    explicit CapabilitiesModel(std::vector<capabilitiy_ptr>& capabilities, QObject *parent = 0);
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  public:
    void addCapability(scheduler::capabilitiy_ptr capability);
    capabilitiy_ptr capability(int index) const;
};

}

#endif // CAPABILITIESMODEL_H
