#ifndef TYPESMODEL_H
#define TYPESMODEL_H

#include <QAbstractListModel>
#include "registry.h"

namespace scheduler{

class TypesModel : public QAbstractListModel{
  Q_OBJECT
  std::vector<box_type_ptr>& _types;
  public:
    explicit TypesModel(std::vector<box_type_ptr>& boxtypes, QObject *parent = 0);
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  public:
    void addType(scheduler::box_type_ptr type);
    box_type_ptr boxtype(int index) const;
    int type_index(scheduler::box_type_ptr type) const;
};

}

#endif // TYPESMODEL_H
