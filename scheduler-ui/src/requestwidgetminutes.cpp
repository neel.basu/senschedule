#include "requestwidgetminutes.h"
#include "ui_requestwidgetminutes.h"
#include "mathematica++/m.h"
#include "mathematica++/io.h"
#include "mathematica++/variant.h"
#include <iostream>
#include <QDebug>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QValueAxis>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/filesystem/path.hpp>

scheduler::RequestWidgetMinutes::RequestWidgetMinutes(mathematica::connector& mathematica, CapabilitiesModel* capabilities_model, TypesModel* types_model, QWidget *parent): QGroupBox(parent), _mathematica(mathematica), _capabilities_model(capabilities_model), _types_model(types_model), ui(new Ui::RequestWidgetMinutes){
    ui->setupUi(this);
    _resource_capabilities_model = 0x0;
    connect(ui->typeCombo, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [this](int index){
        if(_resource_capabilities_model){
            delete _resource_capabilities_model;
        }
        box_type_ptr boxtype = _types_model->boxtype(index);
        request_ptr res(new scheduler::request(boxtype));
        _resource_capabilities_model = new ResourceCapabilitiesModel(res);
        ui->capablitiesTableView->setModel(_resource_capabilities_model);
        ui->capablitiesTableView->update();
        _target_request = res;
    });
    ui->typeCombo->setModel(_types_model);
    ui->typeCombo->setModelColumn(0);
    ui->typeCombo->setCurrentIndex(0);

    connect(ui->startDateTimeEdit, &QDateTimeEdit::dateTimeChanged, [this](const QDateTime& datetime){
        ui->sEdit->setText(QString::number(datetime.toTime_t()/60));
        ui->endDateTimeEdit->setMinimumDateTime(datetime.addSecs(60));
    });
    connect(ui->endDateTimeEdit, &QDateTimeEdit::dateTimeChanged, [this](const QDateTime& datetime){
        ui->dEdit->setText(QString::number(datetime.toTime_t()/60));
    });

    connect(ui->actionBtnBox, SIGNAL(accepted()), this, SIGNAL(okay()));
    connect(ui->actionBtnBox, SIGNAL(rejected()), this, SIGNAL(cancel()));
    
    connect(ui->computeCapabilitiesBtn, SIGNAL(clicked()), this, SLOT(compute_capabilities_hash()));
    connect(ui->viewComputedHashBtn, SIGNAL(clicked()), this, SLOT(view_computed_capabilities_hash()));
}

scheduler::RequestWidgetMinutes::~RequestWidgetMinutes(){
    delete ui;
}

scheduler::request_ptr scheduler::RequestWidgetMinutes::request() const{
    _target_request->_span.start = ui->startDateTimeEdit->dateTime().toTime_t()/60;
    _target_request->_span.delta   = ui->endDateTimeEdit->dateTime().toTime_t()/60 - _target_request->_span.start;
    return _target_request;
}

void scheduler::RequestWidgetMinutes::load(scheduler::request_ptr res){
    QDateTime start = QDateTime::fromTime_t(res->_span.start*60);
    QDateTime end   = QDateTime::fromTime_t(res->_span.start*60 + res->_span.delta*60);

    ui->sEdit->setText(QString::number(res->_span.start));
    ui->dEdit->setText(QString::number(res->_span.delta));

    ui->startDateTimeEdit->setDateTime(start);
    ui->endDateTimeEdit->setDateTime(end);

    ui->typeCombo->setCurrentIndex(_types_model->type_index(res->_type));
    
    if(!_resource_capabilities_model){
        _resource_capabilities_model = new ResourceCapabilitiesModel(res);
    }
    _resource_capabilities_model->setResource(res);
    _target_request = res;
}

void scheduler::RequestWidgetMinutes::compute_capabilities_hash(){
    using namespace mathematica;
    m cap_matrix = _target_request->capabilities();
    _mathematica.save() << m("ScalarizeF")(cap_matrix);
    ui->capabilitiesComputedKeyEdit->setText(QString::fromStdString(_mathematica.last_key()));
    ui->viewComputedHashBtn->setEnabled(true);
    _target_request->_capabilities_hash = _mathematica.last_key();
}

void scheduler::RequestWidgetMinutes::view_computed_capabilities_hash(){
    using namespace mathematica;
    value v;
    std::string hash_key = ui->capabilitiesComputedKeyEdit->text().toStdString();
    _mathematica << mathematica::m("ToString")(_mathematica.ref(hash_key));
    _mathematica >> v;
    QMessageBox::information(this, "Capabilities Hash", QString::fromStdString(v->stringify()));
    
//    _mathematica << mathematica::m("CreatePalette")(_mathematica.ref(hash_key));
//    _mathematica >> v;
//    std::cout << v << std::endl;
}
