#ifndef RESOURCEWIDGET_H
#define RESOURCEWIDGET_H

#include <QGroupBox>
#include "constructs.h"
#include "typesmodel.h"
#include "capabilitiesmodel.h"
#include "resourcecapabilitiesmodel.h"
#include "observationstablemodel.h"
#include "multiplotviewer.h"
#include "mathematica++/connector.h"
#include "mathematica++/declares.h"
#include <QtCharts/QVXYModelMapper>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChart>

namespace Ui {
class ResourceWidget;
}

using namespace QtCharts;

MATHEMATICA_DECLARE(OscillatingFit)

namespace scheduler{

class ResourceWidget : public QGroupBox{
  Q_OBJECT
  mathematica::connector&    _mathematica;
  resource_ptr               _target_resource;
  ResourceCapabilitiesModel* _resource_capabilities_model;
  CapabilitiesModel*         _capabilities_model;
  TypesModel*                _types_model;
  ObservationsTableModel*    _observation_model;
  box_type_ptr               _box_type;
  QList<QVXYModelMapper*>    _observation_mapper_list;
  QList<QLineSeries*>        _observation_series_list;
  QChart*                    _observations_chart;
  MultiPlotViewer*           _multiplot_viewer;
  public:
    explicit ResourceWidget(mathematica::connector& mathematica, CapabilitiesModel* capabilities_model, TypesModel* types_model, QWidget* parent = 0);
    ~ResourceWidget();
  private:
    Ui::ResourceWidget *ui;
  public:
    resource_ptr resource() const;
    void load(resource_ptr res);
  private:
    void addChartSeries(int col);
  private slots:
    void compute_capabilities_hash();
    void view_computed_capabilities_hash();
    void add_observation();
    void compute_observations();
    void manually_add_partition();
  signals:
    void okay();
    void cancel();
};

}

#endif // RESOURCEWIDGET_H
