#include "requestframe.h"
#include "ui_requestframe.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QDebug>

scheduler::RequestFrame::RequestFrame(QWidget *parent): QFrame(parent), ui(new Ui::RequestFrame){
    ui->setupUi(this);
}

scheduler::RequestFrame::~RequestFrame(){
    delete ui;
}

void scheduler::RequestFrame::load(const QJsonObject& document){
    QJsonObject root = document;
    QJsonValue id       = root["id"];
    QJsonValue type     = root["type"];
    QJsonValue context  = root["context"];
    QJsonArray slot     = root["slot"].toArray();
    QJsonValue start    = slot[0];
    QJsonValue end      = slot[1];
    QJsonArray capabilities = root["capabilities"].toArray();
    QJsonValue capmin = capabilities[0];
    QJsonValue capmax = capabilities[1];
    
    ui->idEdit->setText(QString::number(id.toInt()));
    ui->typeEdit->setText(type.toString());
    ui->contextEdit->setText(context.toString());
    ui->tsEdit->setText(QString::number(start.toInt()));
    ui->teEdit->setText(QString::number(end.toInt()));
    ui->capabilitiesMinEdit->setText(capmin.toString());
    ui->capabilitiesMaxEdit->setText(capmax.toString());
    
}
