/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef REGISTRY_H
#define REGISTRY_H

#include <vector>
#include "constructs.h"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>

namespace scheduler{

struct registry{
    std::vector<box_type_ptr>      _types;
    std::vector<capabilitiy_ptr>   _capabilities;
    std::vector<resource_ptr>      _resources;
    std::vector<request_ptr>       _requests;
    
    void add(box_type_ptr type);
    void add(capabilitiy_ptr capability);
    void add(resource_ptr resource);
    void add(request_ptr request);
  private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version){
        ar & BOOST_SERIALIZATION_NVP(_types);
        ar & BOOST_SERIALIZATION_NVP(_capabilities);
        ar & BOOST_SERIALIZATION_NVP(_resources);
        ar & BOOST_SERIALIZATION_NVP(_requests);
    }
};

}

#endif // REGISTRY_H
