#ifndef RESOURCEFRAME_H
#define RESOURCEFRAME_H

#include <QFrame>
#include <QJsonObject>

namespace Ui {
class ResourceFrame;
}

namespace scheduler{

class ResourceFrame : public QFrame{
  Q_OBJECT
  public:
    explicit ResourceFrame(QWidget *parent = 0);
    ~ResourceFrame();
  private:
    Ui::ResourceFrame *ui;
  public:
    void load(const QJsonObject& document);
};
    
}

#endif // RESOURCEFRAME_H
