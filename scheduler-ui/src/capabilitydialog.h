#ifndef CAPABILITYDIALOG_H
#define CAPABILITYDIALOG_H

#include <QDialog>
#include <vector>
#include "constructs.h"

namespace Ui {
class CapabilityDialog;
}

namespace scheduler{

class CapabilityDialog : public QDialog{
  Q_OBJECT
  const std::vector<scheduler::capabilitiy_ptr>& _capabilities;
  public:
    explicit CapabilityDialog(const std::vector<scheduler::capabilitiy_ptr>& capabilities, QWidget *parent = 0);
    ~CapabilityDialog();
  private:
    Ui::CapabilityDialog *ui;
  public:
    scheduler::capabilitiy_ptr capability() const;
};

}

#endif // CAPABILITYDIALOG_H
