#include "resourceframe.h"
#include "ui_resourceframe.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

scheduler::ResourceFrame::ResourceFrame(QWidget *parent): QFrame(parent), ui(new Ui::ResourceFrame){
    ui->setupUi(this);
}

scheduler::ResourceFrame::~ResourceFrame(){
    delete ui;
}

void scheduler::ResourceFrame::load(const QJsonObject& document){
    QJsonObject root = document;
    QJsonValue id       = root["id"];
    QJsonValue type     = root["type"];
    QJsonValue context  = root["context"];
    QJsonObject availability = root["availability"].toObject();
    QJsonValue start        = availability["start"];
    QJsonValue duration     = availability["duration"];
    QJsonValue recurrence   = availability["recurrence"];
    QJsonArray capabilities = root["capabilities"].toArray();
    QJsonValue capmin = capabilities[0];
    QJsonValue capmax = capabilities[1];
    QJsonValue expectations = root["expectations"];
    
    QJsonArray E = expectations.toArray();

    ui->idEdit->setText(QString::number(id.toInt()));
    ui->typeEdit->setText(type.toString());
    ui->contextEdit->setText(context.toString());
    ui->sEdit->setText(QString::number(start.toInt()));
    ui->dEdit->setText(QString::number(duration.toInt()));
    ui->kEdit->setText(QString::number(recurrence.toInt()));
    ui->capabilitiesMinEdit->setText(capmin.toString());
    ui->capabilitiesMaxEdit->setText(capmax.toString());
    
    ui->extectationsTableWidget->setRowCount(E.size());
    ui->extectationsTableWidget->setColumnCount(5);
    
    int i = 0;
    foreach(QJsonValue value, E){
        QJsonArray r = value.toArray();
        int j = 0;
        foreach(QJsonValue cell, r){
            double v = cell.toDouble();
            ui->extectationsTableWidget->setItem(i, j, new QTableWidgetItem(QString("%1").arg(v)));
            ++j;
        }
        ++i;
    }
}
