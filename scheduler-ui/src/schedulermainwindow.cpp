#include "schedulermainwindow.h"
#include "ui_schedulermainwindow.h"
#include "verticalpushbutton.h"
#include <QToolBox>
#include <QPushButton>
#include <QFileDialog>
#include "capabilitydialog.h"
#include "boxtypedialog.h"
#include "solverwidget.h"
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>

// #define USE_MINUTE_QUANTA 1 // Defined in CMakeLists.txt

#ifdef USE_MINUTE_QUANTA
#include "resourcewidgetminutes.h"
#include "requestwidgetminutes.h"
#else
#include "resourcewidget.h"
#include "requestwidget.h"
#endif

namespace scheduler {
#ifdef USE_MINUTE_QUANTA
    typedef ResourceWidgetMinutes ResourceWidgetT;
    typedef RequestWidgetMinutes  RequestWidgetT;
#else
    typedef ResourceWidget ResourceWidgetT;
    typedef RequestWidget  RequestWidgetT;
#endif
}

SchedulerMainWindow::SchedulerMainWindow(scheduler::registry& registry, mathematica::connector& shell, QWidget* parent): QMainWindow(parent), _mathematica(shell), _registry(registry), ui(new Ui::SchedulerMainWindow){
    ui->setupUi(this);

    _capability_model   = new scheduler::CapabilitiesModel(_registry._capabilities);
    _types_model        = new scheduler::TypesModel(_registry._types);
    _resource_model     = new scheduler::ResourceModel(_registry._resources);
    _request_model      = new scheduler::RequestModel(_registry._requests);

#ifdef USE_MINUTE_QUANTA
    _request_model->setMinuteEpoch(true);
#endif

    ui->resourceDock->setVisible(false);
    ui->requestDock->setVisible(false);
    ui->capabilitiesDock->setVisible(false);
    ui->typesDock->setVisible(false);

    VerticalPushButton* resource_button = new VerticalPushButton("Resources");
    resource_button->setCheckable(true);
    resource_button->setFlat(true);
    ui->sideToolBar->addWidget(resource_button);
    connect(resource_button, &VerticalPushButton::toggled, [this](bool checked){
        ui->resourceDock->setVisible(checked);
    });
    ui->sideToolBar->addSeparator();
    VerticalPushButton* request_button = new VerticalPushButton("Requests");
    request_button->setFlat(true);
    request_button->setCheckable(true);
    ui->sideToolBar->addWidget(request_button);
    connect(request_button, &VerticalPushButton::toggled, [this](bool checked){
        ui->requestDock->setVisible(checked);
    });
    ui->sideToolBar->addSeparator();
    VerticalPushButton* capabilities_button = new VerticalPushButton("Capabilities");
    capabilities_button->setFlat(true);
    capabilities_button->setCheckable(true);
    ui->sideToolBar->addWidget(capabilities_button);
    connect(capabilities_button, &VerticalPushButton::toggled, [this](bool checked){
        ui->capabilitiesDock->setVisible(checked);
    });
    ui->sideToolBar->addSeparator();
    VerticalPushButton* types_button = new VerticalPushButton("Types");
    types_button->setFlat(true);
    types_button->setCheckable(true);
    ui->sideToolBar->addWidget(types_button);
    connect(types_button, &VerticalPushButton::toggled, [this](bool checked){
        ui->typesDock->setVisible(checked);
    });
    
    ui->capabilitiesListView->setModel(_capability_model);
    ui->typesListView->setModel(_types_model);
    ui->resourceTableView->setModel(_resource_model);
    ui->requestTableView->setModel(_request_model);

    connect(ui->capabilityAddBtn, SIGNAL(clicked()), this, SLOT(addCapability()));
    connect(ui->typeAddBtn, SIGNAL(clicked()), this, SLOT(addType()));
    connect(ui->resourceAddBtn, SIGNAL(clicked()), SLOT(addResource()));
    connect(ui->resourceDelBtn, SIGNAL(clicked()), SLOT(removeResource()));
    connect(ui->requestAddBtn, SIGNAL(clicked()), SLOT(addRequest()));
    connect(ui->requestDelBtn, SIGNAL(clicked()), SLOT(removeRequest()));
    
    connect(ui->resourceDuplicateBtn, SIGNAL(clicked()), SLOT(copyResource()));
    connect(ui->requestDuplicateBtn, SIGNAL(clicked()), SLOT(copyRequest()));
    
    connect(ui->resourceTableView, SIGNAL(activated(const QModelIndex&)), this, SLOT(modifyResource(const QModelIndex&)));
    connect(ui->requestTableView, SIGNAL(activated(const QModelIndex&)), this, SLOT(modifyRequest(const QModelIndex&)));
    
    ui->resourceTableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->requestTableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    
    connect(ui->actionSolve, SIGNAL(triggered()), this, SLOT(showSolver()));
    
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(save()));
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(load()));
}

SchedulerMainWindow::~SchedulerMainWindow(){
    delete ui;
}

void SchedulerMainWindow::addCapability(){
    scheduler::CapabilityDialog dialog(_registry._capabilities, this);
    if(dialog.exec() == QDialog::Accepted){
        scheduler::capabilitiy_ptr cap = dialog.capability();
        _capability_model->addCapability(cap);
    }
}

void SchedulerMainWindow::addType(){
    scheduler::BoxTypeDialog dialog(_capability_model, this);
    if(dialog.exec() == QDialog::Accepted){
        scheduler::box_type_ptr boxtype = dialog.boxtype();
        _types_model->addType(boxtype);
    }
}

void SchedulerMainWindow::addResource(){
    scheduler::ResourceWidgetT* resource_widget = new scheduler::ResourceWidgetT(_mathematica, _capability_model, _types_model, this);
    int index = ui->centralTabWidget->addTab(resource_widget, "Add a new resource");
    ui->centralTabWidget->setCurrentIndex(index);
    connect(resource_widget, &scheduler::ResourceWidgetT::okay, [index, resource_widget, this](){
        scheduler::resource_ptr res = resource_widget->resource();
        _resource_model->addResource(res);
        ui->centralTabWidget->removeTab(index);
        resource_widget->deleteLater();
        ui->resourceTableView->clearSelection();
    });
    connect(resource_widget, &scheduler::ResourceWidgetT::cancel, [index, resource_widget, this](){
        ui->centralTabWidget->removeTab(index);
        resource_widget->deleteLater();
        ui->resourceTableView->clearSelection();
    });
}

void SchedulerMainWindow::removeResource(){
    QModelIndexList selection = ui->resourceTableView->selectionModel()->selectedRows();
    if(selection.size() == 0){
        return;
    }
    int row = selection.first().row();
    scheduler::resource_ptr selected_resource = _resource_model->resource(row);
    _resource_model->removeRequest(selected_resource);
}


void SchedulerMainWindow::addRequest(){
    scheduler::RequestWidgetT* request_widget = new scheduler::RequestWidgetT(_mathematica, _capability_model, _types_model, this);
    int index = ui->centralTabWidget->addTab(request_widget, "Add a new resource");
    ui->centralTabWidget->setCurrentIndex(index);
    connect(request_widget, &scheduler::RequestWidgetT::okay, [index, request_widget, this](){
        scheduler::request_ptr req = request_widget->request();
        _request_model->addRequest(req);
        ui->centralTabWidget->removeTab(index);
        request_widget->deleteLater();
        ui->resourceTableView->clearSelection();
    });
    connect(request_widget, &scheduler::RequestWidgetT::cancel, [index, request_widget, this](){
        ui->centralTabWidget->removeTab(index);
        request_widget->deleteLater();
        ui->resourceTableView->clearSelection();
    });
}

void SchedulerMainWindow::removeRequest(){
    QModelIndexList selection = ui->requestTableView->selectionModel()->selectedRows();
    if(selection.size() == 0){
        return;
    }
    int row = selection.first().row();
    scheduler::request_ptr selected_request = _request_model->request(row);
    _request_model->removeRequest(selected_request);
}


void SchedulerMainWindow::modifyResource(const QModelIndex & current){
    int row = current.row();
    scheduler::resource_ptr selected_resource = _resource_model->resource(row);
    std::map<scheduler::resource_ptr, int>::const_iterator it = _resources_beging_modified.find(selected_resource);
    if(it != _resources_beging_modified.end()){
        ui->centralTabWidget->setCurrentIndex(it->second);
        return;
    }
    
    scheduler::ResourceWidgetT* resource_widget = new scheduler::ResourceWidgetT(_mathematica, _capability_model, _types_model, this);
    int index = ui->centralTabWidget->addTab(resource_widget, "Modify a resource");
    ui->centralTabWidget->setCurrentIndex(index);
    _resources_beging_modified.insert(std::make_pair(selected_resource, index));
    connect(resource_widget, &scheduler::ResourceWidgetT::okay, [row, index, resource_widget, selected_resource, this](){
        scheduler::resource_ptr res = resource_widget->resource();
        _resource_model->updateResource(row, res);
        ui->centralTabWidget->removeTab(index);
        _resources_beging_modified.erase(selected_resource);
        resource_widget->deleteLater();
    });
    connect(resource_widget, &scheduler::ResourceWidgetT::cancel, [selected_resource,resource_widget, index, this](){
        ui->centralTabWidget->removeTab(index);
        _resources_beging_modified.erase(selected_resource);
        resource_widget->deleteLater();
    });
    resource_widget->load(selected_resource);
    ui->resourceTableView->clearSelection();
}

void SchedulerMainWindow::modifyRequest(const QModelIndex & current){
    int row = current.row();
    scheduler::request_ptr selected_request = _request_model->request(row);
    std::map<scheduler::request_ptr, int>::const_iterator it = _requests_beging_modified.find(selected_request);
    if(it != _requests_beging_modified.end()){
        ui->centralTabWidget->setCurrentIndex(it->second);
        return;
    }
    
    scheduler::RequestWidgetT* request_widget = new scheduler::RequestWidgetT(_mathematica, _capability_model, _types_model, this);
    int index = ui->centralTabWidget->addTab(request_widget, "Modify a request");
    ui->centralTabWidget->setCurrentIndex(index);
    _requests_beging_modified.insert(std::make_pair(selected_request, index));
    connect(request_widget, &scheduler::RequestWidgetT::okay, [row, index, request_widget, selected_request, this](){
        scheduler::request_ptr req = request_widget->request();
        _request_model->updateRequest(row, req);
        ui->centralTabWidget->removeTab(index);
        _requests_beging_modified.erase(selected_request);
        request_widget->deleteLater();
    });
    connect(request_widget, &scheduler::RequestWidgetT::cancel, [selected_request,request_widget, index, this](){
        ui->centralTabWidget->removeTab(index);
        _requests_beging_modified.erase(selected_request);
        request_widget->deleteLater();
    });
    request_widget->load(selected_request);
    ui->resourceTableView->clearSelection();
}

void SchedulerMainWindow::copyResource(){
    QModelIndexList selection = ui->resourceTableView->selectionModel()->selectedRows();
    if(selection.size() == 0){
        return;
    }
    int row = selection.first().row();
    scheduler::resource_ptr selected_resource = _resource_model->resource(row);
    _resource_model->addResource(selected_resource->clone());
}

void SchedulerMainWindow::copyRequest(){
    QModelIndexList selection = ui->requestTableView->selectionModel()->selectedRows();
    if(selection.size() == 0){
        return;
    }
    int row = selection.first().row();
    scheduler::request_ptr selected_request = _request_model->request(row);
    _request_model->addRequest(selected_request->clone());
}

void SchedulerMainWindow::showSolver(){
    scheduler::SolverWidget* widget = new scheduler::SolverWidget(_mathematica, _resource_model, _request_model, this);
    int index = ui->centralTabWidget->addTab(widget, "Solver");
    ui->centralTabWidget->setCurrentIndex(index);
}

void SchedulerMainWindow::save(){
    QString file_name = QFileDialog::getSaveFileName(this, tr("Save File"), ".", tr("SenSchedule Simulation Images (*.xml)"));
    if(!file_name.isEmpty()){
        std::ofstream stream(file_name.toStdString());
        {// scope required for boost 1.66 issue https://svn.boost.org/trac10/ticket/13400
            boost::archive::xml_oarchive oa(stream);
            oa << BOOST_SERIALIZATION_NVP(_registry);
        }
        stream.close();
    }
}

void SchedulerMainWindow::load(){
    QString file_name = QFileDialog::getOpenFileName(this, tr("Open File"), ".", tr("SenSchedule Simulation Images (*.xml)"));
    if(!file_name.isEmpty()){
        std::ifstream stream(file_name.toStdString());
        {// scope required for boost 1.66 issue https://svn.boost.org/trac10/ticket/13400
            boost::archive::xml_iarchive ia(stream);
            ia >> BOOST_SERIALIZATION_NVP(_registry);
        }
        stream.close();
        _resource_model->reset();
        _request_model->reset();
    }
}
