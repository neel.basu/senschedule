/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "constructs.h"
#include "mathematica++/io.h"
#include "mathematica++/declares.h"

scheduler::capability::capability(const std::string& label, unsigned int id): label(label), restrictive(false){
    
}

scheduler::capability_organizer::capability_organizer(scheduler::box_type_ptr type): _type(type), _hash_base(1), _hash_updated(1){
    std::for_each(_type->_capabilities.begin(), _type->_capabilities.end(), [this](capabilitiy_ptr cap){
        _hash_base  *= cap->prime;
    });
}

scheduler::capability_organizer::capability_organizer(const scheduler::capability_organizer& other): _type(other._type), _capability_values(other._capability_values), _capabilities_hash(other._capabilities_hash), _hash_base(other._hash_base), _hash_updated(other._hash_updated){
    
}

bool scheduler::capability_organizer::capabilities_complete() const{
    return (_hash_updated % _hash_base) == 0;
}

void scheduler::capability_organizer::compute_hash(mathematica::connector &mathematica){

}

bool scheduler::capability_organizer::capabilities_computed() const{
    return !_capabilities_hash.empty();
}

scheduler::resource::resource(scheduler::box_type_ptr type): capability_organizer(type), _type(type), _partitions_computed(false){

}

scheduler::resource::resource(const scheduler::resource& other): capability_organizer(other), _type(other._type), _context(other._context), _availability(other._availability), _observations(other._observations), _partitions_computed(other._partitions_computed), _partitions_key(other._partitions_key), _partitions_matrix(other._partitions_matrix){
    
}

bool scheduler::resource::partiions_computed() const{
    return _partitions_computed;
}

boost::shared_ptr<scheduler::resource> scheduler::resource::clone() const{
    return boost::shared_ptr<scheduler::resource>(new resource(*this));
}

mathematica::m scheduler::capability_organizer::capabilities() const{
    using namespace mathematica;
    mathematica::m matrix("List");
    std::for_each(_capability_values.begin(), _capability_values.end(), [&matrix](const capability_values_collection_type::value_type& capv){
        matrix.arg(List((int)capv.first->prime, (int)capv.second.min, (int)capv.second.max, (int)capv.first->restrictive));
    });
    matrix();
    return matrix;
}

scheduler::request::request(scheduler::box_type_ptr type): capability_organizer(type), _type(type){
    
}

scheduler::request::request(const scheduler::request& other): capability_organizer(other), _type(other._type), _context(other._context), _span(other._span), _computed(other._computed){
    
}

boost::shared_ptr<scheduler::request> scheduler::request::clone() const{
    return boost::shared_ptr<scheduler::request>(new scheduler::request(*this));
}
