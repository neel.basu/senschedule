#ifndef SCHEDULERQT_H
#define SCHEDULERQT_H

#include <map>
#include <QMainWindow>
#include "registry.h"
#include "capabilitiesmodel.h"
#include "typesmodel.h"
#include "resourcemodel.h"
#include "requestmodel.h"
#include "mathematica++/connector.h"

namespace Ui {
    class SchedulerMainWindow;
}

class SchedulerMainWindow : public QMainWindow{
  Q_OBJECT
  
  mathematica::connector&       _mathematica;
  scheduler::registry&          _registry;
  scheduler::CapabilitiesModel* _capability_model;
  scheduler::TypesModel*        _types_model;
  scheduler::ResourceModel*     _resource_model;
  scheduler::RequestModel*      _request_model;
  std::map<scheduler::resource_ptr, int> _resources_beging_modified;
  std::map<scheduler::request_ptr, int>  _requests_beging_modified;
  public:
    explicit SchedulerMainWindow(scheduler::registry& registry, mathematica::connector& shell, QWidget *parent = 0);
    ~SchedulerMainWindow();
  private:
    Ui::SchedulerMainWindow* ui;
  private slots:
    void addCapability();
    void addType();
    void addResource();
    void removeResource();
    void addRequest();
    void removeRequest();
    void modifyResource(const QModelIndex & current);
    void modifyRequest(const QModelIndex & current);
    void copyResource();
    void copyRequest();
    void showSolver();
    void save();
    void load();
};

#endif // SCHEDULERQT_H
