#include "resourcewidget.h"
#include "ui_resourcewidget.h"
#include "mathematica++/m.h"
#include "mathematica++/io.h"
#include "mathematica++/variant.h"
#include <iostream>
#include <QDebug>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QMessageBox>
#include <QValueAxis>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/filesystem/path.hpp>
#include "manualpartitionentrydialog.h"

scheduler::ResourceWidget::ResourceWidget(mathematica::connector& mathematica, CapabilitiesModel* capabilities_model, TypesModel* types_model, QWidget *parent): QGroupBox(parent), _mathematica(mathematica), _capabilities_model(capabilities_model), _types_model(types_model), ui(new Ui::ResourceWidget){
    ui->setupUi(this);
    _resource_capabilities_model = 0x0;
    connect(ui->typeCombo, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [this](int index){
        if(_resource_capabilities_model){
            delete _resource_capabilities_model;
        }
        box_type_ptr boxtype = _types_model->boxtype(index);
        resource_ptr res(new scheduler::resource(boxtype));
        _resource_capabilities_model = new ResourceCapabilitiesModel(res);
        ui->capablitiesTableView->setModel(_resource_capabilities_model);
        ui->capablitiesTableView->update();
        _target_resource = res;
    });
    ui->typeCombo->setModel(_types_model);
    ui->typeCombo->setModelColumn(0);
    ui->typeCombo->setCurrentIndex(0);

    connect(ui->startDateTimeEdit, &QDateTimeEdit::dateTimeChanged, [this](const QDateTime& datetime){
        ui->sEdit->setText(QString::number(datetime.toTime_t()));
        ui->endDateTimeEdit->setMinimumDateTime(datetime.addSecs(1));
    });
    connect(ui->endDateTimeEdit, &QDateTimeEdit::dateTimeChanged, [this](const QDateTime& datetime){
        ui->dEdit->setText(QString::number(datetime.toTime_t()-ui->startDateTimeEdit->dateTime().toTime_t()));
        ui->againDateTimeEdit->setMinimumDateTime(datetime.addSecs(1));
    });
    connect(ui->againDateTimeEdit, &QDateTimeEdit::dateTimeChanged, [this](const QDateTime& datetime){
        ui->kEdit->setText(QString::number(datetime.toTime_t()-ui->startDateTimeEdit->dateTime().toTime_t()));
    });

    connect(ui->actionBtnBox, SIGNAL(accepted()), this, SIGNAL(okay()));
    connect(ui->actionBtnBox, SIGNAL(rejected()), this, SIGNAL(cancel()));
    
    connect(ui->computeCapabilitiesBtn, SIGNAL(clicked()), this, SLOT(compute_capabilities_hash()));
    connect(ui->viewComputedHashBtn, SIGNAL(clicked()), this, SLOT(view_computed_capabilities_hash()));
    
    _observation_model = new ObservationsTableModel(this);
    ui->observationsTableView->setModel(_observation_model);
    
    connect(ui->addObservationBtn, SIGNAL(clicked()), this, SLOT(add_observation()));
    connect(ui->observationPlotBtn, SIGNAL(clicked(bool)), ui->observationChartWidget, SLOT(setVisible(bool)));
    
    ui->observationPlotBtn->setDown(true);
    
    _observations_chart = new QChart;
    QValueAxis* axis = new QValueAxis();
    axis->setTickCount(10);
    ui->observationChartWidget->setChart(_observations_chart);
    _observations_chart->setAxisY(axis);
    QFlags<QChartView::RubberBand> bands;
    bands |= QChartView::VerticalRubberBand;
    bands |= QChartView::HorizontalRubberBand;
    ui->observationChartWidget->setRubberBand(bands);
    
    connect(ui->computeObservationsBtn, SIGNAL(clicked()), this, SLOT(compute_observations()));
    
    _multiplot_viewer = new MultiPlotViewer(_mathematica, this);
    QHBoxLayout* layout = new QHBoxLayout(this);
    ui->visualizationTab->setLayout(layout);
    layout->addWidget(_multiplot_viewer);
    
    ui->partitionTableWidget->setColumnCount(5);
    QStringList headers;
    headers << "b" << "e" << "d" << "w" << "f";
    ui->partitionTableWidget->setHorizontalHeaderLabels(headers);
    
    connect(ui->addPartitionBtn,SIGNAL(clicked()), this, SLOT(manually_add_partition()));
}

scheduler::ResourceWidget::~ResourceWidget(){
    delete ui;
}

scheduler::resource_ptr scheduler::ResourceWidget::resource() const{
    _target_resource->_availability.start = ui->startDateTimeEdit->dateTime().toTime_t();
    _target_resource->_availability.delta   = ui->endDateTimeEdit->dateTime().toTime_t() - _target_resource->_availability.start;
    _target_resource->_availability.recurrence = ui->againDateTimeEdit->dateTime().toTime_t() - _target_resource->_availability.start;
    return _target_resource;
}

void scheduler::ResourceWidget::load(scheduler::resource_ptr res){
    QDateTime start = QDateTime::fromTime_t(res->_availability.start);
    QDateTime end   = QDateTime::fromTime_t(res->_availability.start + res->_availability.delta);
    QDateTime again = QDateTime::fromTime_t(res->_availability.start + res->_availability.recurrence);
    
    ui->sEdit->setText(QString::number(res->_availability.start));
    ui->dEdit->setText(QString::number(res->_availability.delta));
    ui->kEdit->setText(QString::number(res->_availability.recurrence));
    ui->startDateTimeEdit->setDateTime(start);
    ui->endDateTimeEdit->setDateTime(end);
    ui->againDateTimeEdit->setDateTime(again);
    
    ui->typeCombo->setCurrentIndex(_types_model->type_index(res->_type));
    
    if(!_resource_capabilities_model){
        _resource_capabilities_model = new ResourceCapabilitiesModel(res);
    }
    _resource_capabilities_model->setResource(res);
    for(scheduler::resource::observations_collection_type::const_iterator i = res->_observations.begin(); i != res->_observations.end(); ++i){
        const scheduler::resource::observation_type& observation = *i;
        _observation_model->addObservation(observation.begin(), observation.end());
    }
    if(_observation_model->columnCount() > 0){
        addChartSeries(_observation_model->columnCount());
    }
    
    if(res->_partitions_computed){
        typedef std::vector<std::vector<double>> matrix_type;
        matrix_type mat = res->_partitions_matrix;
        ui->partitionTableWidget->setRowCount(mat.size());
        
        for(int i = 0; i < mat.size(); ++i){
            for(int j = 0; j < mat[i].size(); ++j){
                QTableWidgetItem* item = new QTableWidgetItem(QString("%1").arg(mat[i][j]));
                ui->partitionTableWidget->setItem(i, j, item);
            }
        }
    }
    
    _target_resource = res;
}

void scheduler::ResourceWidget::compute_capabilities_hash(){
    using namespace mathematica;
    m cap_matrix = _target_resource->capabilities();
    _mathematica.save() << m("ScalarizeF")(cap_matrix);
    ui->capabilitiesComputedKeyEdit->setText(QString::fromStdString(_mathematica.last_key()));
    ui->viewComputedHashBtn->setEnabled(true);
    _target_resource->_capabilities_hash = _mathematica.last_key();
}

void scheduler::ResourceWidget::view_computed_capabilities_hash(){
    using namespace mathematica;
    value v;
    std::string hash_key = ui->capabilitiesComputedKeyEdit->text().toStdString();
    _mathematica << mathematica::m("ToString")(_mathematica.ref(hash_key));
    _mathematica >> v;
    QMessageBox::information(this, "Capabilities Hash", QString::fromStdString(v->stringify()));
    
//    _mathematica <<  mathematica::m("UsingFrontEnd")(mathematica::m("CreatePalette")(_mathematica.ref(hash_key)));
//    _mathematica >> v;
//    std::cout << v << std::endl;
}

void scheduler::ResourceWidget::add_observation(){
    QDialog dialog;
    QVBoxLayout* layout = new QVBoxLayout;
    QPlainTextEdit* editor = new QPlainTextEdit;
    layout->addWidget(new QLabel("Enter comma(,) seperated values"));
    layout->addWidget(editor);
    QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    
    connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));
    layout->addWidget(buttons);
    
    dialog.setLayout(layout);
    
    if(dialog.exec() == QDialog::Accepted){
        QString value = editor->toPlainText();
        QStringList list = value.split(",");
        std::vector<int> observation;
        bool is_ok;
        std::transform(list.begin(), list.end(), std::back_inserter(observation), boost::bind(&QString::toInt, _1, &is_ok, 10));
        _observation_model->addObservation(observation.begin(), observation.end());
        _target_resource->addObservation(observation.begin(), observation.end());
        _target_resource->_partitions_computed = false;
    }
    
    int ncolumns = _observation_model->columnCount();
    for(int i = 0; i < ncolumns; ++i){
        if(i % 2 == 0){
            ui->observationsTableView->setColumnHidden(i, true);
        }
    }
    addChartSeries(ncolumns);
}

void scheduler::ResourceWidget::addChartSeries(int col){
    QLineSeries* series = new QLineSeries;
    QVXYModelMapper* mapper = new QVXYModelMapper(this);
    mapper->setSeries(series);
    mapper->setModel(_observation_model);
    mapper->setXColumn(col-2);
    mapper->setYColumn(col-1);
    series->setName(QString("y%1").arg(col/2));
    _observation_mapper_list.append(mapper);
    _observation_series_list.append(series);
    
    _observations_chart->addSeries(series);
}

void scheduler::ResourceWidget::compute_observations(){
    if(!_target_resource){
        QMessageBox::critical(this, "Error", "Cannot compute unless resource is added to the repository");
        return;
    }
    
    typedef std::vector<std::vector<double>> matrix_type;
    if(ui->addObservationBtn->isEnabled()){
        using namespace mathematica;

        std::vector<double> data;
        _observation_model->fill(std::back_inserter(data));
        symbol x("x");
        value result;

        _mathematica.save() << OscillatingFit(data, x, (Rule("MeasureFlactuation") = false, Rule("ThresholdValue") = 64, Rule("VisualizationPath") = std::string("vis.png")));
        _target_resource->_partitions_key = _mathematica.last_key();
        _mathematica << _mathematica.last();
        _mathematica >> result;

        matrix_type mat = cast<matrix_type>(result->serialize());

        _target_resource->_partitions_matrix = mat;
        _target_resource->_partitions_computed = true;

        ui->partitionTableWidget->setRowCount(mat.size());

        for(int i = 0; i < mat.size(); ++i){
            for(int j = 0; j < mat[i].size(); ++j){
                QTableWidgetItem* item = new QTableWidgetItem(QString("%1").arg(mat[i][j]));
                ui->partitionTableWidget->setItem(i, j, item);
            }
        }

        _multiplot_viewer->render("Observation Partitions", boost::filesystem::path("vis.png"));
    }else{
        matrix_type mat;
        for(int i = 0; i < ui->partitionTableWidget->rowCount(); ++i){
            std::vector<double> row = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
            for(int j = 0; j < ui->partitionTableWidget->columnCount(); ++j){
                QDoubleSpinBox* widget = qobject_cast<QDoubleSpinBox*>(ui->partitionTableWidget->cellWidget(i, j));
                double value = widget->value();
                row[j] = value;
            }
            mat.push_back(row);
        }
        _target_resource->_partitions_matrix = mat;
        _target_resource->_partitions_computed = true;
    }
}

void scheduler::ResourceWidget::manually_add_partition(){
    ui->addObservationBtn->setDisabled(true);
    ManualPartitionEntryDialog dialog(this);
    dialog.exec();
    int row = ui->partitionTableWidget->rowCount();
    ui->partitionTableWidget->insertRow(row);

    QDoubleSpinBox* cell_begin = new QDoubleSpinBox;
    cell_begin->setMaximum(99999.99f);
    cell_begin->setValue(dialog.start());
    cell_begin->setButtonSymbols(QAbstractSpinBox::NoButtons);
    ui->partitionTableWidget->setCellWidget(row, 0, cell_begin);
    cell_begin->setDisabled(true);
    
    QDoubleSpinBox* cell_end = new QDoubleSpinBox;
    cell_end->setMaximum(99999.99f);
    cell_end->setValue(dialog.end());
    cell_end->setButtonSymbols(QAbstractSpinBox::NoButtons);
    ui->partitionTableWidget->setCellWidget(row, 1, cell_end);
    cell_end->setDisabled(true);
    
    QDoubleSpinBox* cell_duration = new QDoubleSpinBox;
    cell_duration->setMaximum(99999.99f);
    cell_duration->setValue(dialog.duration());
    cell_duration->setButtonSymbols(QAbstractSpinBox::NoButtons);
    ui->partitionTableWidget->setCellWidget(row, 2, cell_duration);
    cell_duration->setDisabled(true);
    
    QDoubleSpinBox* cell_weight = new QDoubleSpinBox;
    cell_weight->setMaximum(99999.99f);
    cell_weight->setValue(dialog.weight());
    cell_weight->setButtonSymbols(QAbstractSpinBox::NoButtons);
    ui->partitionTableWidget->setCellWidget(row, 3, cell_weight);
    cell_weight->setDisabled(true);
    
    QDoubleSpinBox* cell_flactuation = new QDoubleSpinBox;
    cell_flactuation->setMaximum(99999.99f);
    cell_flactuation->setValue(0.0f);
    cell_flactuation->setButtonSymbols(QAbstractSpinBox::NoButtons);
    ui->partitionTableWidget->setCellWidget(row, 4, cell_flactuation);
    cell_flactuation->setDisabled(true);
}
