#include "observationstablemodel.h"
#include <cmath>

scheduler::ObservationsTableModel::ObservationsTableModel(QObject *parent): QAbstractTableModel(parent), _max_count(0){

}

QVariant scheduler::ObservationsTableModel::headerData(int section, Qt::Orientation orientation, int role) const{
    if(role == Qt::DisplayRole){
        if(orientation == Qt::Horizontal){
            return QString(section % 2 ? "y%1" : "x%1").arg(int(section/2));
        }else if(orientation == Qt::Vertical){
            return section;
        }
    }
    return QVariant();
}

int scheduler::ObservationsTableModel::rowCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;
    
    return _max_count;
}

int scheduler::ObservationsTableModel::columnCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return 2*_observations.size();
}

QVariant scheduler::ObservationsTableModel::data(const QModelIndex &index, int role) const{
    if (!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole){
        int i = std::floor((double)index.column()/2.0f);
        const observation_type& observation = _observations.at(i);
        if(index.column() % 2 == 0){
            return index.row();
        }else if(index.row() < observation.size()){
            return observation.at(index.row());
        }
    }
    return QVariant();
}

void scheduler::ObservationsTableModel::addObservation(observation_type::const_iterator begin, observation_type::const_iterator end){
    beginInsertColumns(QModelIndex(), columnCount(), columnCount()+1);
    _observations.push_back(observation_type());
    size_t count = std::distance(begin, end);
    int i = 0;
    std::for_each(begin, end, [this, &i](double value){
        if(i >= _max_count){
            beginInsertRows(QModelIndex(), i, i);
        }
        _observations.back().push_back(value);
        if(i >= _max_count){
            endInsertRows();
        }
        ++i;
    });
    
    if(count > _max_count){
        _max_count = count;
    }
    endInsertColumns();
}
