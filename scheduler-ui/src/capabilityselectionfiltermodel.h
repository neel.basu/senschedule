#ifndef CAPABILITYSELECTIONFILTERMODEL_H
#define CAPABILITYSELECTIONFILTERMODEL_H

#include <set>
#include <QSortFilterProxyModel>
#include "constructs.h"

namespace scheduler{

class CapabilitySelectionFilterModel : public QSortFilterProxyModel{
  Q_OBJECT
  std::set<int> _rows_selected;
  public:
    CapabilitySelectionFilterModel(QObject* parent);
    void addSelectedRow(int row);
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    int sourceRow(int proxy_row) const;
    
};

}

#endif // CAPABILITYSELECTIONFILTERMODEL_H
