#include "boxtypedialog.h"
#include "ui_boxtypedialog.h"

scheduler::BoxTypeDialog::BoxTypeDialog(CapabilitiesModel* capabilities_model, QWidget *parent): QDialog(parent), _capabilities_model(capabilities_model), ui(new Ui::BoxTypeDialog){
    ui->setupUi(this);

    _selected_capabilities_model = new scheduler::CapabilitiesModel(_selected_capabilities, this);
    _filter_model = new scheduler::CapabilitySelectionFilterModel(this);
    _filter_model->setSourceModel(_capabilities_model);
    ui->capabilityCombo->setModel(_filter_model);
    ui->capabilityCombo->setModelColumn(0);

    ui->cababilityList->setModel(_selected_capabilities_model);

    connect(ui->capabilityAddBtn, SIGNAL(clicked(bool)), this, SLOT(capabilityAdded()));
}

scheduler::BoxTypeDialog::~BoxTypeDialog(){
    delete ui;
}

void scheduler::BoxTypeDialog::capabilityAdded(){
    int proxy_row = ui->capabilityCombo->currentIndex();
    int index = _filter_model->sourceRow(proxy_row);
    capabilitiy_ptr capability = _capabilities_model->capability(index);
    _selected_capabilities_model->addCapability(capability);
    _filter_model->addSelectedRow(index);
    ui->capabilityCombo->update();
    ui->capabilityCombo->setCurrentIndex(0);
    if(_filter_model->rowCount() == 0){
        ui->capabilityAddBtn->setDisabled(true);
    }
}

scheduler::box_type_ptr scheduler::BoxTypeDialog::boxtype() const{
    scheduler::box_type_ptr box_ptr(new scheduler::box_type);
    box_ptr->label = ui->labelEdit->text().toStdString();
    box_ptr->name = ui->nameEdit->text().toStdString();
    std::copy(_selected_capabilities.begin(), _selected_capabilities.end(), std::inserter(box_ptr->_capabilities, box_ptr->_capabilities.end()));
    return box_ptr;
}
