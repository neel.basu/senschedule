#include <QApplication>
#include <QStyleFactory>
#include "mathematica++/connector.h"
#include "schedulermainwindow.h"
#include "verticalpushbutton.h"
#include "constructs.h"
#include "registry.h"
#include "config.h"
#include <boost/shared_ptr.hpp>

int main(int argc, char *argv[]){
    scheduler::registry registry;
    
    mathematica::connector shell;
    shell.enable_transaction_lock(true);
    
    shell.import(M_SENSCHEDULE_LIB_PATH);
    
    QApplication app(argc, argv);
    
    SchedulerMainWindow mainw(registry, shell);
    mainw.show();

    return app.exec();
}

