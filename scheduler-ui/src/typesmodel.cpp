#include "typesmodel.h"
#include <QString>
#include <string>

scheduler::TypesModel::TypesModel(std::vector<box_type_ptr>& boxtypes, QObject *parent): QAbstractListModel(parent), _types(boxtypes){
}

QVariant scheduler::TypesModel::headerData(int section, Qt::Orientation orientation, int role) const{
    return "name";
}

int scheduler::TypesModel::rowCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return _types.size();
}

QVariant scheduler::TypesModel::data(const QModelIndex &index, int role) const{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= _types.size())
        return QVariant();
    
    if(role == Qt::DisplayRole){
        return QString("%1").arg(QString::fromStdString(_types[index.row()]->label));
    }
    
    return QVariant();
}

void scheduler::TypesModel::addType(box_type_ptr type){
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _types.push_back(type);
    endInsertRows();
}

scheduler::box_type_ptr scheduler::TypesModel::boxtype(int index) const{
    return _types[index];
}

int scheduler::TypesModel::type_index(scheduler::box_type_ptr type) const{
    for(std::vector<box_type_ptr>::const_iterator i = _types.cbegin(); i != _types.cend(); ++i){
        if(*i == type){
            return std::distance(_types.cbegin(), i);
        }
    }
    return -1;
}

