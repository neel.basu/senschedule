#ifndef SELECTORPROXYMODEL_H
#define SELECTORPROXYMODEL_H

#include <map>
#include <vector>
#include <QIdentityProxyModel>
#include "resourcemodel.h"

namespace scheduler{

class SelectorProxyModel : public QIdentityProxyModel{
  Q_OBJECT
  mutable std::map<int, bool> _check_map;
  public:
    SelectorProxyModel();
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant data(const QModelIndex &proxyIndex, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
  public:
    std::vector<int> checked_rows() const;
    void selectAll();
    void selectNone();
};

}

#endif // SELECTORPROXYMODEL_H
