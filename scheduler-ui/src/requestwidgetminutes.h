#ifndef REQUESTWIDGETMINUTES_H
#define REQUESTWIDGETMINUTES_H

#include <QGroupBox>
#include "constructs.h"
#include "typesmodel.h"
#include "capabilitiesmodel.h"
#include "resourcecapabilitiesmodel.h"
#include "multiplotviewer.h"
#include "mathematica++/connector.h"
#include "mathematica++/declares.h"
#include <QtCharts/QVXYModelMapper>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChart>

namespace Ui {
class RequestWidgetMinutes;
}

using namespace QtCharts;

namespace scheduler{

class RequestWidgetMinutes : public QGroupBox{
  Q_OBJECT
  mathematica::connector&    _mathematica;
  request_ptr                _target_request;
  ResourceCapabilitiesModel* _resource_capabilities_model;
  CapabilitiesModel*         _capabilities_model;
  TypesModel*                _types_model;
  public:
    explicit RequestWidgetMinutes(mathematica::connector& mathematica, CapabilitiesModel* capabilities_model, TypesModel* types_model, QWidget* parent = 0);
    ~RequestWidgetMinutes();
  private:
    Ui::RequestWidgetMinutes *ui;
  public:
    request_ptr request() const;
    void load(request_ptr res);
  private slots:
    void compute_capabilities_hash();
    void view_computed_capabilities_hash();
  signals:
    void okay();
    void cancel();
};

}

#endif // REQUESTWIDGETMINUTES_H
