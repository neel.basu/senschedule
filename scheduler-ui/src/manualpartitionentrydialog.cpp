#include "manualpartitionentrydialog.h"
#include "ui_manualpartitionentrydialog.h"
#include <QPushButton>

scheduler::ManualPartitionEntryDialog::ManualPartitionEntryDialog(QWidget *parent): QDialog(parent), ui(new Ui::ManualPartitionEntryDialog){
    ui->setupUi(this);
    
    connect(ui->startSpinBox,SIGNAL(valueChanged(double)), this, SLOT(startChangedSlot(double)));
    connect(ui->endSpinBox,SIGNAL(valueChanged(double)), this, SLOT(endChangedSlot(double)));
    connect(ui->durationSpinBox,SIGNAL(valueChanged(double)), this, SLOT(durationChangedSlot(double)));
}

scheduler::ManualPartitionEntryDialog::~ManualPartitionEntryDialog(){
    delete ui;
}

void scheduler::ManualPartitionEntryDialog::startChangedSlot(double value){
    if(ui->startSpinBox->hasFocus()){
        double end = ui->endSpinBox->value();
        ui->durationSpinBox->setValue(end - value);
        invalidate();
    }
}

void scheduler::ManualPartitionEntryDialog::endChangedSlot(double value){
    if(ui->endSpinBox->hasFocus()){
        double start = ui->startSpinBox->value();
        ui->durationSpinBox->setValue(value - start);
        invalidate();
    }
}

void scheduler::ManualPartitionEntryDialog::durationChangedSlot(double value){
    if(ui->durationSpinBox->hasFocus()){
        double start = ui->startSpinBox->value();
        ui->endSpinBox->setValue(start + value);
        invalidate();
    }
}

void scheduler::ManualPartitionEntryDialog::invalidate(){
    double start = ui->startSpinBox->value();
    double end = ui->endSpinBox->value();
    double duration = ui->durationSpinBox->value();
    
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    if (end < start) {
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }else if(duration != end - start){
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
}

double scheduler::ManualPartitionEntryDialog::start() const{
    return ui->startSpinBox->value();
}
double scheduler::ManualPartitionEntryDialog::end() const{
    return ui->endSpinBox->value();
}
double scheduler::ManualPartitionEntryDialog::duration() const{
    return ui->durationSpinBox->value();
}
double scheduler::ManualPartitionEntryDialog::weight() const{
    return ui->weightSpinBox->value();
}
