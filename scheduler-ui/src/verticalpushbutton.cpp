/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 * 
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 * 
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */

#include "src/verticalpushbutton.h"
#include <QStyle>
#include <QMenu>
#include <QDebug>
#include <QStylePainter>

VerticalPushButton::VerticalPushButton(QWidget* parent): QPushButton(parent){
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
}

VerticalPushButton::VerticalPushButton(const QString& text, QWidget* parent): QPushButton(text, parent){
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
}

VerticalPushButton::VerticalPushButton(const QIcon& icon, const QString& text, QWidget* parent): QPushButton(icon, text, parent){
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
}

QSize VerticalPushButton::sizeHint() const{
    QSize size = QPushButton::sizeHint();
    size.transpose();
    return size;
}

QSize VerticalPushButton::minimumSizeHint() const{
    QSize size = QPushButton::minimumSizeHint();
    size.transpose();
    return size;
}

void VerticalPushButton::paintEvent(QPaintEvent* event){
    QStylePainter painter(this);
    painter.rotate(90);
    painter.translate(0, -width());
    painter.drawControl(QStyle::CE_PushButton, styleOptions());
}

QStyleOptionButton VerticalPushButton::styleOptions() const{
    QStyleOptionButton options;
    options.initFrom(this);
    QSize size = options.rect.size();
    size.transpose();
    options.rect.setSize(size);
    options.features = QStyleOptionButton::None;
    
    if(isFlat())                                              options.features |= QStyleOptionButton::Flat;
    if(menu())                                                options.features |= QStyleOptionButton::HasMenu;
    if(autoDefault() || isDefault())                          options.features |= QStyleOptionButton::AutoDefaultButton;
    if(isDefault())                                           options.features |= QStyleOptionButton::DefaultButton;
    if(isDown() || (menu() && menu()->isVisible()))           options.state    |= QStyle::State_Sunken;
    if(isChecked())                                           options.state    |= QStyle::State_On;
    if(!isFlat() && (!isDown()))                              options.state    |= QStyle::State_Raised;
    
    options.text = text();
    options.icon = icon();
    options.iconSize = iconSize();
    return options;
}
