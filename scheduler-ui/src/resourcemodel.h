#ifndef RESOURCEMODEL_H
#define RESOURCEMODEL_H

#include <QAbstractTableModel>
#include "constructs.h"

namespace scheduler{

class ResourceModel : public QAbstractTableModel{
  Q_OBJECT
  std::vector<resource_ptr>& _resources;
  public:
    explicit ResourceModel(std::vector<resource_ptr>& resources, QObject *parent = nullptr);
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  public:
    void addResource(resource_ptr res);
    void removeRequest(resource_ptr res);
  public:
    resource_ptr resource(unsigned int row);
    void updateResource(int row, resource_ptr res);
  public:
    void reset();
};

}

#endif // RESOURCEMODEL_H
