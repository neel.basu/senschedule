#ifndef RESOURCEWIDGETMINUTES_H
#define RESOURCEWIDGETMINUTES_H

#include <QGroupBox>
#include "constructs.h"
#include "typesmodel.h"
#include "capabilitiesmodel.h"
#include "resourcecapabilitiesmodel.h"
#include "multiplotviewer.h"
#include "mathematica++/connector.h"
#include "mathematica++/declares.h"
#include "partitionwidget.h"

namespace Ui {
class ResourceWidgetMinutes;
}

namespace scheduler{

class ResourceWidgetMinutes : public QGroupBox{
  Q_OBJECT
  mathematica::connector&    _mathematica;
  resource_ptr               _target_resource;
  ResourceCapabilitiesModel* _resource_capabilities_model;
  CapabilitiesModel*         _capabilities_model;
  TypesModel*                _types_model;
  box_type_ptr               _box_type;
  PartitionWidget*           _partition_widget;
  public:
    explicit ResourceWidgetMinutes(mathematica::connector& mathematica, CapabilitiesModel* capabilities_model, TypesModel* types_model, QWidget* parent = 0);
    ~ResourceWidgetMinutes();
  private:
    Ui::ResourceWidgetMinutes *ui;
  public:
    resource_ptr resource() const;
    void load(resource_ptr res);
  private:
    void addChartSeries(int col);
  private slots:
    void compute_capabilities_hash();
    void view_computed_capabilities_hash();
    void compute_observations();
  signals:
    void okay();
    void cancel();
};

}

#endif // RESOURCEWIDGETMINUTES_H
