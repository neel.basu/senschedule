#ifndef RESOURCECAPABILITIESMODEL_H
#define RESOURCECAPABILITIESMODEL_H

#include <QAbstractTableModel>
#include "constructs.h"
#include <vector>

namespace scheduler {

class ResourceCapabilitiesModel : public QAbstractTableModel{
  Q_OBJECT
  boost::shared_ptr<scheduler::capability_organizer> _organizer;
  public:
    explicit ResourceCapabilitiesModel(boost::shared_ptr<scheduler::capability_organizer> organizer, QObject *parent = nullptr);
    void setResource(boost::shared_ptr<scheduler::capability_organizer> organizer);
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    boost::shared_ptr<scheduler::capability_organizer> organizer() const;
};

}

#endif // RESOURCECAPABILITIESMODEL_H
