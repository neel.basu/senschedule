#include "requestmodel.h"
#include <QPixmap>
#include <QPainter>
#include <QDateTime>

scheduler::RequestModel::RequestModel(std::vector<request_ptr>& requests, QObject *parent): QAbstractTableModel(parent), _requests(requests), minute_epoch(false){

}

QVariant scheduler::RequestModel::headerData(int section, Qt::Orientation orientation, int role) const{
    if (role != Qt::DisplayRole)
        return QVariant();
    if(orientation == Qt::Horizontal){
        if(section == 0)
            return "type";
        else if(section == 1)
            return "start";
        else if(section == 2)
            return "delta";
        else return "";
    }else{
        return section;
    }
}

int scheduler::RequestModel::rowCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return _requests.size();
}

int scheduler::RequestModel::columnCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return 3;
}

QVariant scheduler::RequestModel::data(const QModelIndex &index, int role) const{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= _requests.size())
        return QVariant();

    scheduler::request_ptr req = _requests[index.row()];
    if(role == Qt::DisplayRole){
        if(index.column() == 0){
            return QString("%1").arg(QString::fromStdString(req->_type->label));
        }else if(index.column() == 1){
            return QString::number(req->_span.start);
        }else if(index.column() == 2){
            return QString::number(req->_span.delta);
        }
    }else if(role == Qt::DecorationRole){
        if(index.column() == 0){
            QColor color(Qt::darkYellow);
            if(req->capabilities_complete()){
                color = Qt::darkGreen;
            }
            if(req->capabilities_computed()){
                color = Qt::darkBlue;
            }
            
            QPixmap icon(16, 16);
            icon.fill(Qt::transparent);
            QPainter painter(&icon);
            painter.setRenderHint(QPainter::Antialiasing);
            painter.setPen(color);
            painter.setBrush(QBrush(color));
            painter.drawEllipse(QRectF(0, 0, 16, 16));
            return icon;
        }
    }else if(role == Qt::ToolTipRole){
        if(index.column() == 1){
            return QDateTime::fromTime_t(req->_span.start * (minute_epoch ? 60 : 1));
        }else if(index.column() == 2){
            return QDateTime::fromTime_t((req->_span.start + req->_span.delta) * (minute_epoch ? 60 : 1));
        }
    }
    return QVariant();
}

void scheduler::RequestModel::addRequest(scheduler::request_ptr req){
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _requests.push_back(req);
    endInsertRows();
}

void scheduler::RequestModel::removeRequest(scheduler::request_ptr req){
    int row = -1;
    for(int i = 0; i < _requests.size(); ++i){
        if(_requests[i] == req){
            row = i;
        }
    }
    
    if(row < 0){
        return;
    }
    
    beginRemoveRows(QModelIndex(), row, row);
    _requests.erase(_requests.begin() + row);
    endRemoveRows();
}


scheduler::request_ptr scheduler::RequestModel::request(unsigned int row){
    return _requests[row];
}

void scheduler::RequestModel::updateRequest(int row, scheduler::request_ptr res){
    QModelIndex index_begin = createIndex(row, 0);
    QModelIndex index_end   = createIndex(row, columnCount());
    _requests[row] = res;
    emit dataChanged(index_begin, index_end);
}

void scheduler::RequestModel::reset(){
    beginResetModel();
    endResetModel();
}
