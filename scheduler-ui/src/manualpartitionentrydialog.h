#ifndef MANUALPARTITIONENTRYDIALOG_H
#define MANUALPARTITIONENTRYDIALOG_H

#include <QDialog>

namespace Ui {
class ManualPartitionEntryDialog;
}

namespace scheduler{

class ManualPartitionEntryDialog : public QDialog{
  Q_OBJECT
  public:
    explicit ManualPartitionEntryDialog(QWidget *parent = 0);
    ~ManualPartitionEntryDialog();
  private:
    Ui::ManualPartitionEntryDialog *ui;
  private slots:
    void startChangedSlot(double value);
    void endChangedSlot(double value);
    void durationChangedSlot(double value);
  private:
    void invalidate();
  public:
    double start() const;
    double end() const;
    double duration() const;
    double weight() const;
};
    
}

#endif // MANUALPARTITIONENTRYDIALOG_H
