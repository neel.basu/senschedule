#ifndef BOXTYPEDIALOG_H
#define BOXTYPEDIALOG_H

#include <QDialog>
#include "constructs.h"
#include "capabilitiesmodel.h"
#include "capabilityselectionfiltermodel.h"

namespace Ui {
class BoxTypeDialog;
}

namespace scheduler {

class BoxTypeDialog : public QDialog{
  Q_OBJECT
  scheduler::CapabilitiesModel* _capabilities_model;
  scheduler::CapabilitiesModel* _selected_capabilities_model;
  scheduler::CapabilitySelectionFilterModel* _filter_model;
  std::vector<capabilitiy_ptr> _selected_capabilities;
  public:
    explicit BoxTypeDialog(scheduler::CapabilitiesModel* capabilities_model, QWidget *parent = 0);
    ~BoxTypeDialog();
  private:
    Ui::BoxTypeDialog *ui;
  private slots:
    void capabilityAdded();
  public:
    scheduler::box_type_ptr boxtype() const;
};

}

#endif // BOXTYPEDIALOG_H
