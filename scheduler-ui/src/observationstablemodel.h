#ifndef OBSERVATIONSTABLEMODEL_H
#define OBSERVATIONSTABLEMODEL_H

#include <vector>
#include <QAbstractTableModel>
#include <boost/foreach.hpp>

namespace scheduler{

class ObservationsTableModel : public QAbstractTableModel{
  Q_OBJECT
  typedef std::vector<int> observation_type;
  typedef std::vector<observation_type> observation_collection_type;

  observation_collection_type _observations;
  size_t _max_count;
  public:
    explicit ObservationsTableModel(QObject *parent = 0);
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  public:
    void addObservation(observation_type::const_iterator begin, observation_type::const_iterator end);
    template <typename T>
    void fill(T it){
        BOOST_FOREACH(const observation_type& observation, _observations){
            std::copy(observation.begin(), observation.end(), it);
        }
    }
};

}

#endif // OBSERVATIONSTABLEMODEL_H
