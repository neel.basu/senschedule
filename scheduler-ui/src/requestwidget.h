#ifndef REQUESTWIDGET_H
#define REQUESTWIDGET_H

#include <QGroupBox>
#include "constructs.h"
#include "typesmodel.h"
#include "capabilitiesmodel.h"
#include "resourcecapabilitiesmodel.h"
#include "multiplotviewer.h"
#include "mathematica++/connector.h"
#include "mathematica++/declares.h"
#include <QtCharts/QVXYModelMapper>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChart>

namespace Ui {
class RequestWidget;
}

using namespace QtCharts;

namespace scheduler{

class RequestWidget : public QGroupBox{
  Q_OBJECT
  mathematica::connector&    _mathematica;
  request_ptr                _target_request;
  ResourceCapabilitiesModel* _resource_capabilities_model;
  CapabilitiesModel*         _capabilities_model;
  TypesModel*                _types_model;
  public:
    explicit RequestWidget(mathematica::connector& mathematica, CapabilitiesModel* capabilities_model, TypesModel* types_model, QWidget* parent = 0);
    ~RequestWidget();
  private:
    Ui::RequestWidget *ui;
  public:
    request_ptr request() const;
    void load(request_ptr res);
  private slots:
    void compute_capabilities_hash();
    void view_computed_capabilities_hash();
  signals:
    void okay();
    void cancel();
};

}

#endif // REQUESTWIDGET_H
