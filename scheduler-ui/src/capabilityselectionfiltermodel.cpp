#include "capabilityselectionfiltermodel.h"

scheduler::CapabilitySelectionFilterModel::CapabilitySelectionFilterModel(QObject* parent): QSortFilterProxyModel(parent){
    
}

void scheduler::CapabilitySelectionFilterModel::addSelectedRow(int row){
    beginResetModel();
    _rows_selected.insert(row);
    endResetModel();
}

bool scheduler::CapabilitySelectionFilterModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const{
    return (_rows_selected.find(source_row) == _rows_selected.end());
}

int scheduler::CapabilitySelectionFilterModel::sourceRow(int proxy_row) const{
    QModelIndex proxyIndex = index(proxy_row, 0);
    QModelIndex sourceIndex = mapToSource(proxyIndex);
    return sourceIndex.row();
}
