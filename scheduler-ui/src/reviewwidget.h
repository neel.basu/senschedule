#ifndef REVIEWWIDGET_H
#define REVIEWWIDGET_H

#include <QWidget>
#include <QHBoxLayout>
#include <QJsonObject>

namespace Ui {
class ReviewWidget;
}

namespace scheduler{

class ReviewWidget : public QWidget{
  Q_OBJECT
  QWidget* _resources_container_widget;
  QWidget* _requests_container_widget;
  QHBoxLayout* _resources_layout;
  QHBoxLayout* _requests_layout;
  public:
    explicit ReviewWidget(QWidget *parent = 0);
    ~ReviewWidget();
  private:
    Ui::ReviewWidget *ui;
  public:
    void addResource(const QJsonObject& json);
    void addRequest(const QJsonObject& json);
  signals:
    void backClicked();
    void solveClicked();
    void generateSolverClicked();
  private slots:
  public:
    QString cdf() const;
    QString graphics() const;
    QString sym() const;
    double epsilon() const;
    int faultTolerance() const;
};
    
}

#endif // REVIEWWIDGET_H
