#include "solverwidget.h"
#include "ui_solverwidget.h"
#include "mathematica++/m.h"
#include "mathematica++/io.h"
#include "mathematica++/declares.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QDateTime>
#include <QDesktopServices>
#include <QUrl>

#include <vtkOBJReader.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkOBJImporter.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkActorCollection.h>
#include <vtkCollectionIterator.h>

MATHEMATICA_DECLARE(Association)
MATHEMATICA_DECLARE(Resource)
MATHEMATICA_DECLARE(Request)
MATHEMATICA_DECLARE(ResourceJSON)
MATHEMATICA_DECLARE(RequestJSON)
MATHEMATICA_DECLARE(ScalarizeF)
MATHEMATICA_DECLARE(ScalarizeRestrictedF)
MATHEMATICA_DECLARE(ScheduleSlices)
MATHEMATICA_DECLARE(CreateNotebook)
MATHEMATICA_DECLARE(Part)
MATHEMATICA_DECLARE(Values)
MATHEMATICA_DECLARE(Length)
MATHEMATICA_DECLARE(Show)
MATHEMATICA_DECLARE(Export)
MATHEMATICA_DECLARE(Set)

scheduler::SolverWidget::SolverWidget(mathematica::connector& conn, scheduler::ResourceModel* resource_model, scheduler::RequestModel* request_model, QWidget *parent): QStackedWidget(parent), _conn(conn), ui(new Ui::SolverWidget), _resource_model(resource_model), _request_model(request_model){
    ui->setupUi(this);
    _resource_proxy_model = new scheduler::SelectorProxyModel;
    _request_proxy_model  = new scheduler::SelectorProxyModel;
    _resource_proxy_model->setSourceModel(_resource_model);
    _request_proxy_model->setSourceModel(_request_model);
    ui->resourceTableView->setModel(_resource_proxy_model);
    ui->requestTableView->setModel(_request_proxy_model);
    
    connect(ui->resourcesSelectAllBtn, SIGNAL(clicked()), this, SLOT(selectAllResources()));
    connect(ui->resourcesSelectNoneBtn, SIGNAL(clicked()), this, SLOT(selectNoResources()));
    connect(ui->requestsSelectAllBtn, SIGNAL(clicked()), this, SLOT(selectAllRequests()));
    connect(ui->requestsSelectNoneBtn, SIGNAL(clicked()), this, SLOT(selectNoRequests()));
    
    ui->resourceTableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->resourceTableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    
    _reviewWidget = new ReviewWidget(this);
    
    insertWidget(1, _reviewWidget);
    
    connect(ui->proceedBtn, SIGNAL(clicked()), this, SLOT(prepare()));
    connect(_reviewWidget, &ReviewWidget::backClicked, [this](){
        setCurrentIndex(0);
    });
    connect(_reviewWidget, SIGNAL(solveClicked()), this, SLOT(solve()));
    connect(_reviewWidget, SIGNAL(generateSolverClicked()), this, SLOT(generateSolver()));
    connect(ui->cdfOpenBtn, &QPushButton::clicked, [this](){
        QDesktopServices::openUrl(QUrl::fromLocalFile(ui->cdfPathEdit->text()));
    });
    
    _graphs_multiplotviewer = new scheduler::MultiPlotViewer(_conn, this);
    _layers_compared_multiplotviewer = new scheduler::MultiPlotViewer(_conn, this);
    _layers_compact_multiplotviewer = new scheduler::MultiPlotViewer(_conn, this);
    _layers_balanced_multiplotviewer = new scheduler::MultiPlotViewer(_conn, this);
    _dependencies_multiplotviewer = new scheduler::MultiPlotViewer(_conn, this);
    
    _visualization_compact_viewer = new QVTKWidget(this);
    _visualization_balanced_viewer = new QVTKWidget(this);
    
    ui->solutionTabWidget->insertTab(1, _graphs_multiplotviewer, "Graphs");
    ui->solutionTabWidget->insertTab(2, _layers_compared_multiplotviewer, "Layers Compared");
    ui->solutionTabWidget->insertTab(3, _layers_compact_multiplotviewer, "Layers Compact");
    ui->solutionTabWidget->insertTab(4, _layers_balanced_multiplotviewer, "Layers Balanced");
    ui->solutionTabWidget->insertTab(5, _dependencies_multiplotviewer, "Dependencis");
    ui->solutionTabWidget->insertTab(6, _visualization_compact_viewer, "Visualization Compact");
    ui->solutionTabWidget->insertTab(7, _visualization_balanced_viewer, "Visualization Balanced");
}

scheduler::SolverWidget::~SolverWidget(){
    delete ui;
}

void scheduler::SolverWidget::prepare(){
    using namespace mathematica;

    symbol x("x");
    symbol y("y");

    symbol R("R");
    symbol Q("Q");

    m resources_list("List");
    m requests_list("List");

    std::vector<int> selected_resource_indexes = _resource_proxy_model->checked_rows();
    std::vector<int> selected_request_indexes  = _request_proxy_model->checked_rows();

    std::vector<resource_ptr> selected_resources;
    std::vector<request_ptr> selected_requests;

    int ri = 0;
    std::for_each(selected_resource_indexes.begin(), selected_resource_indexes.end(), [this, &ri, &selected_resources, &resources_list](int row){
        resource_ptr res = _resource_model->resource(row);
        selected_resources.push_back(res);
        m r = Association(
            Rule("id") = ri++,
            Rule("type") = res->_type->label,
            Rule("context") = 2,
            Rule("contextuals") = List(),
            Rule("availability") = Association(
                Rule("start") = res->_availability.start,
                Rule("duration") = res->_availability.delta,
                Rule("recurrence") = res->_availability.recurrence
            ),
            Rule("capabilities") = ScalarizeF(res->capabilities()),
            Rule("capabilitiesr") = ScalarizeRestrictedF(res->capabilities()),
            Rule("expectations") = res->_partitions_matrix
        );
        resources_list.arg(r);
    });
    resources_list();

    int qi = 0;
    std::for_each(selected_request_indexes.begin(), selected_request_indexes.end(), [this, &qi, &selected_requests, &requests_list](int row){
        request_ptr req = _request_model->request(row);
        selected_requests.push_back(req);
        m q = Association(
            Rule("id") = qi++,
            Rule("type") = req->_type->label,
            Rule("context") = 2,
            Rule("contextuals") = List(),
            Rule("slot") = List(req->_span.start, req->_span.start + req->_span.delta),
            Rule("capabilities") = ScalarizeF(req->capabilities()),
            Rule("capabilitiesr") = ScalarizeRestrictedF(req->capabilities())
        );
        requests_list.arg(q);
    });
    requests_list();

    _conn, R = resources_list;
    _conn, Q = requests_list;
    value Rv, Qv;
    _conn << ResourceJSON(R);
    _conn >> Rv;
    _conn << RequestJSON(Q);
    _conn >> Qv;

    std::cout << Rv << std::endl;
    std::cout << Qv << std::endl;
    {
        std::string resources_json = cast<std::string>(Rv);
        QString resources_json_str = QString::fromStdString(resources_json);
        
        QJsonDocument document;
        document = QJsonDocument::fromJson(resources_json_str.toUtf8());
        QJsonArray resources_json_array = document.array();
        
        foreach(QJsonValue rv, resources_json_array){
            QJsonObject resource_json_object = rv.toObject();
            _reviewWidget->addResource(resource_json_object);
        }
    }{
        std::string requests_json = cast<std::string>(Qv);
        QString requests_json_str = QString::fromStdString(requests_json);
        
        QJsonDocument document;
        document = QJsonDocument::fromJson(requests_json_str.toUtf8());
        QJsonArray requests_json_array = document.array();
        
        foreach(QJsonValue qv, requests_json_array){
            QJsonObject request_json_object = qv.toObject();
            _reviewWidget->addRequest(request_json_object);
        }
    }
    setCurrentIndex(1);
}

void scheduler::SolverWidget::solve(){
    using namespace mathematica;
    
    symbol x("x");
    symbol y("y");
    
    symbol R("R");
    symbol Q("Q");
    
    value res;
    
    double epsilon = _reviewWidget->epsilon();
    QString cdf_path = _reviewWidget->cdf();
    QString graphics_path = _reviewWidget->graphics();
    QString sym_path = _reviewWidget->sym();
    
    _conn << ScheduleSlices(R, Q, x, y, epsilon, (Rule("FaultTolerence") = _reviewWidget->faultTolerance(), Rule("Visualize") = false, Rule("HighlightBlocks") = true, Rule("ShowLegends") = true, Rule("ExportCDF") = cdf_path.toStdString(), Rule("ExportSymbols") = sym_path.toStdString(), Rule("ShowLayerEdges") = false));
    _conn >> res;
    std::cout << res << std::endl;
    
    symbol result("result");
    _conn, result = Import(sym_path.toStdString());
    
    value time;
    value epsln;
    value leases;
    value actions;
    
    _conn << Part(result, std::string("time"));
    _conn >> time;
    _conn << Part(result, std::string("epsilon"));
    _conn >> epsln;
    _conn << Values(Part(result, std::string("lease")));
    _conn >> leases;
    _conn << Values(Part(result, std::string("action")));
    _conn >> actions;
    
    typedef std::vector<std::vector<double>> lease_type;
    typedef std::vector<lease_type> lease_collection_type;
    
    std::cout << actions << std::endl;
    
    lease_collection_type migration_collection = cast<lease_collection_type>(actions);
    lease_collection_type lease_collection = cast<lease_collection_type>(leases);
    int nhops = std::accumulate(lease_collection.begin(), lease_collection.end(), 0, [](double a, const lease_type& lease){
        return a + lease.size();
    });
    
    ui->timeEdit->setText(QString::number(cast<double>(time)));
    ui->epslnEdit->setText(QString::number(cast<double>(epsln)));
    ui->nhopsEdit->setText(QString::number(nhops));
    
    for(lease_collection_type::const_iterator li = lease_collection.cbegin(); li != lease_collection.cend(); ++li){
        QTableWidget* table = new QTableWidget(this);
        const lease_type& lease = *li;
        table->setRowCount(lease.size());
        table->setColumnCount(3);
        for(lease_type::const_iterator ri = lease.cbegin(); ri != lease.cend(); ++ri){
            const std::vector<double>& row = *ri;
            for(std::vector<double>::const_iterator ci = row.cbegin(); ci != row.cend(); ++ci){
                QTableWidgetItem* item;
                if(std::distance(row.cbegin(), ci) != 2){
#ifdef USE_MINUTE_QUANTA
                    item = new QTableWidgetItem(QDateTime::fromTime_t(*ci * 60).toString());
#else
                    item = new QTableWidgetItem(QDateTime::fromTime_t(*ci).toString());
#endif
                }else{
                    item = new QTableWidgetItem(QString::number(*ci));
                }
                table->setItem(std::distance(lease.cbegin(), ri), std::distance(row.cbegin(), ci), item);
            }
        }
        QStringList headers;
        headers << "begin" << "end" << "resource";
        table->setHorizontalHeaderLabels(headers);
        ui->leaseTabWidget->addTab(table, QString::number(std::distance(lease_collection.cbegin(), li)+1));
    }
    
    for(lease_collection_type::const_iterator mi = migration_collection.cbegin(); mi != migration_collection.cend(); ++mi){
        QTableWidget* table = new QTableWidget(this);
        const lease_type& migration = *mi;
        table->setRowCount(migration.size());
        table->setColumnCount(3);
        for(lease_type::const_iterator ri = migration.cbegin(); ri != migration.cend(); ++ri){
            const std::vector<double>& row = *ri;
            for(std::vector<double>::const_iterator ci = row.cbegin(); ci != row.cend(); ++ci){
                QTableWidgetItem* item;
                if(std::distance(row.cbegin(), ci) == 0){
#ifdef USE_MINUTE_QUANTA
                    item = new QTableWidgetItem(QDateTime::fromTime_t(*ci * 60).toString());
#else
                    item = new QTableWidgetItem(QDateTime::fromTime_t(*ci).toString());
#endif
                }else{
                    item = new QTableWidgetItem(*ci == 0 ? QString("") : QString::number(*ci));
                }
                table->setItem(std::distance(migration.cbegin(), ri), std::distance(row.cbegin(), ci), item);
            }
        }
        QStringList headers;
        headers << "time" << "from" << "to";
        table->setHorizontalHeaderLabels(headers);
        ui->migrationsTabWidget->addTab(table, QString::number(std::distance(migration_collection.cbegin(), mi)+1));
    }
    
    ui->cdfPathEdit->setText(cdf_path);
    
    value layersn;
    _conn << Length(Part(result, std::string("LayersBalanced")));
    _conn >> layersn;
    std::cout << "layersn " << layersn << std::endl;
    value depsn;
    _conn << Length(Part(result, std::string("dependency")));
    _conn >> depsn;
    std::cout << "depsn " << depsn << std::endl;
    
    for(int i = 0; i != cast<int>(layersn); ++i){
        _graphs_multiplotviewer->render(Part(result, std::string("graphs"), i+1), QString("Q%1").arg(i+1).toStdString());
        _layers_compared_multiplotviewer->render(Part(result, std::string("LayersComparison"), i+1), QString("Q%1").arg(i+1).toStdString());
        _layers_compact_multiplotviewer->render(Part(result, std::string("LayersCompact"), i+1), QString("Q%1").arg(i+1).toStdString());
        _layers_balanced_multiplotviewer->render(Part(result, std::string("LayersBalanced"), i+1), QString("Q%1").arg(i+1).toStdString());
    }
    
    for(int i = 0; i != cast<int>(depsn); ++i){
        _dependencies_multiplotviewer->render(Part(result, std::string("dependency"), i+1), QString("R%1").arg(i+1).toStdString());
    }
    
    {
        QString graphics_path_compact = graphics_path;
        value exported_graphics_path;
        _conn << Export(graphics_path_compact.toStdString(), Show(Part(result, std::string("VisualizationCompact"))));
        _conn >> exported_graphics_path;
        std::cout << "graphics obj: " << exported_graphics_path << std::endl;
        
        vtkSmartPointer<vtkOBJImporter> importer = vtkSmartPointer<vtkOBJImporter>::New();
        importer->SetFileName(graphics_path_compact.toStdString().c_str());
        importer->SetFileNameMTL(graphics_path_compact.append(".mtl").toStdString().c_str());
        importer->Read();
        importer->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
    #if VTK_MAJOR_VERSION >= 7 && VTK_MINOR_VERSION > 1
        importer->GetRenderer()->UseFXAAOn();
    #endif
        vtkActorCollection* actors_collection = importer->GetRenderer()->GetActors();
        actors_collection->InitTraversal();
        while(vtkActor* actor = actors_collection->GetNextActor()) {
            actor->GetProperty()->SetOpacity(0.5);
        }
        importer->GetRenderWindow()->LineSmoothingOn();
        importer->GetRenderWindow()->PointSmoothingOn();
        importer->GetRenderWindow()->PolygonSmoothingOn();
        _visualization_compact_viewer->SetRenderWindow(importer->GetRenderWindow());
    }
    
    {
        QString graphics_path_balanced = graphics_path;
        value exported_graphics_path;
        _conn << Export(graphics_path_balanced.toStdString(), Show(Part(result, std::string("VisualizationBalanced"))));
        _conn >> exported_graphics_path;
        std::cout << "graphics obj: " << exported_graphics_path << std::endl;
        
        vtkSmartPointer<vtkOBJImporter> importer = vtkSmartPointer<vtkOBJImporter>::New();
        importer->SetFileName(graphics_path_balanced.toStdString().c_str());
        importer->SetFileNameMTL(graphics_path_balanced.append(".mtl").toStdString().c_str());
        importer->Read();
        importer->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
    #if VTK_MAJOR_VERSION >= 7 && VTK_MINOR_VERSION > 1
        importer->GetRenderer()->UseFXAAOn();
    #endif
        vtkActorCollection* actors_collection = importer->GetRenderer()->GetActors();
        actors_collection->InitTraversal();
        while(vtkActor* actor = actors_collection->GetNextActor()) {
            actor->GetProperty()->SetOpacity(0.5);
        }
        importer->GetRenderWindow()->LineSmoothingOn();
        importer->GetRenderWindow()->PointSmoothingOn();
        importer->GetRenderWindow()->PolygonSmoothingOn();
        _visualization_balanced_viewer->SetRenderWindow(importer->GetRenderWindow());
    }
    
    setCurrentIndex(2);
}

void scheduler::SolverWidget::generateSolver(){
    using namespace mathematica;
    
    symbol x("x");
    symbol y("y");
    
    symbol R("R");
    symbol Q("Q");
    
    double epsilon = _reviewWidget->epsilon();
    QString cdf_path = _reviewWidget->cdf();
    QString graphics_path = _reviewWidget->graphics();
    QString sym_path = _reviewWidget->sym();
    
    value result;
    
    m problem = ScheduleSlices(R, Q, x, y, epsilon, (Rule("Visualize") = false, Rule("HighlightBlocks") = true, Rule("ShowLegends") = true, Rule("ExportCDF") = cdf_path.toStdString(), Rule("ExportSymbols") = sym_path.toStdString()));
    _conn << Export(std::string("/home/neel/Projects/senschedule/build/scheduler-ui/problem.mx"), Association((Rule("resources") = R, Rule("requests") = Q)));
    _conn >> result;
    
    std::cout << "Generate " << result << std::endl;
}

void scheduler::SolverWidget::selectAllResources(){
    _resource_proxy_model->selectAll();
}

void scheduler::SolverWidget::selectNoResources(){
    _resource_proxy_model->selectNone();
}

void scheduler::SolverWidget::selectAllRequests(){
    _request_proxy_model->selectAll();
}

void scheduler::SolverWidget::selectNoRequests(){
    _request_proxy_model->selectNone();
}

