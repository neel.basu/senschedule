#ifndef SOLVERWIDGET_H
#define SOLVERWIDGET_H

#include <QStackedWidget>
#include "resourcemodel.h"
#include "requestmodel.h"
#include "reviewwidget.h"
#include "multiplotviewer.h"
#include "selectorproxymodel.h"
#include "mathematica++/connector.h"
#include <QVTKWidget.h>

namespace Ui {
class SolverWidget;
}

namespace scheduler{

class SolverWidget : public QStackedWidget{
  Q_OBJECT
  mathematica::connector& _conn;
  scheduler::ResourceModel* _resource_model;
  scheduler::RequestModel*  _request_model;
  scheduler::SelectorProxyModel* _resource_proxy_model;
  scheduler::SelectorProxyModel* _request_proxy_model;
  scheduler::ReviewWidget* _reviewWidget;
    
  scheduler::MultiPlotViewer* _graphs_multiplotviewer;
  scheduler::MultiPlotViewer* _layers_compared_multiplotviewer;
  scheduler::MultiPlotViewer* _layers_compact_multiplotviewer;
  scheduler::MultiPlotViewer* _layers_balanced_multiplotviewer;
  scheduler::MultiPlotViewer* _dependencies_multiplotviewer;
    
  QVTKWidget* _visualization_compact_viewer;
  QVTKWidget* _visualization_balanced_viewer;
  public:
    explicit SolverWidget(mathematica::connector& conn, scheduler::ResourceModel* resource_model, scheduler::RequestModel* request_model, QWidget *parent = 0);
    ~SolverWidget();
  private:
    Ui::SolverWidget *ui;
  private slots:
    void prepare();
    void solve();
    void generateSolver();
    void selectAllResources();
    void selectNoResources();
    void selectAllRequests();
    void selectNoRequests();
};
    
}

#endif // SOLVERWIDGET_H
