#include "resourcewidgetminutes.h"
#include "ui_resourcewidgetminutes.h"
#include "mathematica++/m.h"
#include "mathematica++/io.h"
#include "mathematica++/variant.h"
#include <iostream>
#include <QDebug>
#include <QMessageBox>
#include <QPlainTextEdit>
#include <QMessageBox>
#include <QValueAxis>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/filesystem/path.hpp>
#include "manualpartitionentrydialog.h"

scheduler::ResourceWidgetMinutes::ResourceWidgetMinutes(mathematica::connector& mathematica, CapabilitiesModel* capabilities_model, TypesModel* types_model, QWidget *parent): QGroupBox(parent), _mathematica(mathematica), _capabilities_model(capabilities_model), _types_model(types_model), ui(new Ui::ResourceWidgetMinutes), _partition_widget(0x0){
    ui->setupUi(this);
          
    _resource_capabilities_model = 0x0;
    connect(ui->typeCombo, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [this](int index){
        if(_resource_capabilities_model){
            delete _resource_capabilities_model;
        }
        box_type_ptr boxtype = _types_model->boxtype(index);
        resource_ptr res(new scheduler::resource(boxtype));
        _resource_capabilities_model = new ResourceCapabilitiesModel(res);
        ui->capablitiesTableView->setModel(_resource_capabilities_model);
        ui->capablitiesTableView->update();
        _target_resource = res;
    });
    ui->typeCombo->setModel(_types_model);
    ui->typeCombo->setModelColumn(0);
    ui->typeCombo->setCurrentIndex(0);

    connect(ui->startDateTimeEdit, &QDateTimeEdit::dateTimeChanged, [this](const QDateTime& datetime){
        ui->sEdit->setText(QString::number(datetime.toTime_t()/60));
        ui->endDateTimeEdit->setMinimumDateTime(datetime.addSecs(60));
    });
    connect(ui->endDateTimeEdit, &QDateTimeEdit::dateTimeChanged, [this](const QDateTime& datetime){
        ui->dEdit->setText(QString::number((datetime.toTime_t()-ui->startDateTimeEdit->dateTime().toTime_t())/60));
        ui->againDateTimeEdit->setMinimumDateTime(datetime.addSecs(60));
    });
    connect(ui->againDateTimeEdit, &QDateTimeEdit::dateTimeChanged, [this](const QDateTime& datetime){
        ui->kEdit->setText(QString::number((datetime.toTime_t()-ui->startDateTimeEdit->dateTime().toTime_t())/60));
    });
    
    connect(ui->actionBtnBox, &QDialogButtonBox::accepted, [this](){
        if(_target_resource){
            if(_partition_widget){
                compute_observations();
            }
            okay();
        }else{
            QMessageBox::critical(this, "Error", "Resource type not selected (cancelling.. )");
            cancel();
        }
    });

    connect(ui->actionBtnBox, SIGNAL(rejected()), this, SIGNAL(cancel()));
    
    connect(ui->computeCapabilitiesBtn, SIGNAL(clicked()), this, SLOT(compute_capabilities_hash()));
    connect(ui->viewComputedHashBtn, SIGNAL(clicked()), this, SLOT(view_computed_capabilities_hash()));
}

scheduler::ResourceWidgetMinutes::~ResourceWidgetMinutes(){
    delete ui;
}

scheduler::resource_ptr scheduler::ResourceWidgetMinutes::resource() const{
    _target_resource->_availability.start = ui->startDateTimeEdit->dateTime().toTime_t()/60;
    _target_resource->_availability.delta   = (ui->endDateTimeEdit->dateTime().toTime_t()/60 - _target_resource->_availability.start);
    _target_resource->_availability.recurrence = (ui->againDateTimeEdit->dateTime().toTime_t()/60 - _target_resource->_availability.start);
    return _target_resource;
}

void scheduler::ResourceWidgetMinutes::load(scheduler::resource_ptr res){
    QDateTime start = QDateTime::fromTime_t(res->_availability.start*60);
    QDateTime end   = QDateTime::fromTime_t(res->_availability.start*60 + res->_availability.delta*60);
    QDateTime again = QDateTime::fromTime_t(res->_availability.start*60 + res->_availability.recurrence*60);
    
    ui->sEdit->setText(QString::number(res->_availability.start));
    ui->dEdit->setText(QString::number(res->_availability.delta));
    ui->kEdit->setText(QString::number(res->_availability.recurrence));
    ui->startDateTimeEdit->setDateTime(start);
    ui->endDateTimeEdit->setDateTime(end);
    ui->againDateTimeEdit->setDateTime(again);
    
    ui->typeCombo->setCurrentIndex(_types_model->type_index(res->_type));
    
    if(!_resource_capabilities_model){
        _resource_capabilities_model = new ResourceCapabilitiesModel(res);
    }
    _resource_capabilities_model->setResource(res);
//     for(scheduler::resource::observations_collection_type::const_iterator i = res->_observations.begin(); i != res->_observations.end(); ++i){
//         const scheduler::resource::observation_type& observation = *i;
//         // TODO Load observation from resource to UI (However in this widget there is no observation only partition)
//     }

    _target_resource = res;
    
    _partition_widget = new PartitionWidget(this);
    ui->toolBox->addItem(_partition_widget, "Performance");
    _partition_widget->setLimit(_target_resource->_availability.delta, Qt::Horizontal);
    if(res->_partitions_computed){
        // TODO load already computed partitions into UI
        _partition_widget->clear();
        typedef std::vector<std::vector<double>> matrix_type;
        matrix_type mat = res->_partitions_matrix;
        double max = 0;
        for(int i = 0; i < mat.size(); ++i){// leave the first cursor
            double begin       = mat[i][0];
            double end         = mat[i][1];
            double delta       = mat[i][2];
            double weight      = mat[i][3];
            double flactuation = mat[i][4];
            if(weight > max){
                max = weight;
                _partition_widget->setLimit(max, Qt::Vertical);
            }
            if(i != mat.size() -1){
                qDebug() << __LINE__ << i << delta << weight;
                _partition_widget->push(delta, weight);
            }else{
                _partition_widget->setValue(i+1, weight);
            }
        }
        _partition_widget->update();
    }
}

void scheduler::ResourceWidgetMinutes::compute_capabilities_hash(){
    using namespace mathematica;
    m cap_matrix = _target_resource->capabilities();
    _mathematica.save() << m("ScalarizeF")(cap_matrix);
    ui->capabilitiesComputedKeyEdit->setText(QString::fromStdString(_mathematica.last_key()));
    ui->viewComputedHashBtn->setEnabled(true);
    _target_resource->_capabilities_hash = _mathematica.last_key();
}

void scheduler::ResourceWidgetMinutes::view_computed_capabilities_hash(){
    using namespace mathematica;
    value v;
    std::string hash_key = ui->capabilitiesComputedKeyEdit->text().toStdString();
    _mathematica << mathematica::m("ToString")(_mathematica.ref(hash_key));
    _mathematica >> v;
    QMessageBox::information(this, "Capabilities Hash", QString::fromStdString(v->stringify()));
}


void scheduler::ResourceWidgetMinutes::compute_observations(){
    if(!_target_resource){
        QMessageBox::critical(this, "Error", "Cannot compute unless resource is added to the repository");
        return;
    }
    
    typedef std::vector<std::vector<double>> matrix_type;
    matrix_type mat;
    //TODO save the partitions from UI to the resource
    for(PartitionWidget::Iterator i = _partition_widget->begin(); i != _partition_widget->end(); ++i){
        const PartitionCursor& cursor = *i;
        std::vector<double> row;
        qDebug() << __LINE__ << cursor.range().first << cursor.range().second << cursor.value();
        row.push_back(std::floor(cursor.range().first));
        row.push_back(std::floor(cursor.range().second));
        row.push_back(std::floor(cursor.range().second)-std::floor(cursor.range().first));
        row.push_back(std::floor(cursor.value()));
        row.push_back(0.0f);
        mat.push_back(row);
    }
    _target_resource->_partitions_matrix = mat;
    _target_resource->_partitions_computed = true;
}


