#ifndef REQUESTMODEL_H
#define REQUESTMODEL_H

#include <QAbstractTableModel>
#include "constructs.h"

namespace scheduler{

class RequestModel : public QAbstractTableModel{
  Q_OBJECT
  std::vector<request_ptr>& _requests;
  bool minute_epoch;
  public:
    explicit RequestModel(std::vector<request_ptr>& requests, QObject *parent = nullptr);
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  public:
    void addRequest(request_ptr res);
    void removeRequest(request_ptr res);
  public:
    request_ptr request(unsigned int row);
    void updateRequest(int row, request_ptr res);
  public:
    void reset();
  public:
    void setMinuteEpoch(bool flag=true) {minute_epoch = flag;}
    bool minuteEpoch() const {return minute_epoch;}
};

}

#endif // REQUESTMODEL_H
