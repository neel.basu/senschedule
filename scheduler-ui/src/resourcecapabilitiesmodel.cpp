#include "resourcecapabilitiesmodel.h"
#include <iostream>

scheduler::ResourceCapabilitiesModel::ResourceCapabilitiesModel(boost::shared_ptr<capability_organizer> organizer, QObject *parent): QAbstractTableModel(parent), _organizer(organizer){
    std::for_each(_organizer->_type->_capabilities.begin(), _organizer->_type->_capabilities.end(), [this](capabilitiy_ptr cap){
        _organizer->_capability_values.insert(scheduler::capability_organizer::capability_values_collection_type::value_type(cap, capability::range()));
    });
}

void scheduler::ResourceCapabilitiesModel::setResource(boost::shared_ptr<capability_organizer> organizer){
   beginResetModel();
   _organizer = organizer;
   endResetModel();
}

Qt::ItemFlags scheduler::ResourceCapabilitiesModel::flags(const QModelIndex &index) const{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);
    if (index.column() == 1 || index.column() == 2)
        flags |= Qt::ItemIsEditable;
    return flags;
}

QVariant scheduler::ResourceCapabilitiesModel::headerData(int section, Qt::Orientation orientation, int role) const{
    if(orientation == Qt::Vertical)
        return QVariant();
    if(role == Qt::DisplayRole){
        if(section == 0){
            return "capability";
        }else if(section == 1){
            return "minimum";
        }else if(section == 2){
            return "maximum";
        }
    }
    return QAbstractTableModel::headerData(section, orientation, role);
}

int scheduler::ResourceCapabilitiesModel::rowCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return _organizer->_capability_values.size();
}

int scheduler::ResourceCapabilitiesModel::columnCount(const QModelIndex &parent) const{
    if (parent.isValid())
        return 0;

    return 3;
}

QVariant scheduler::ResourceCapabilitiesModel::data(const QModelIndex &index, int role) const{
    if (!index.isValid())
        return QVariant();

    if(role == Qt::DisplayRole){
        scheduler::capability_organizer::capability_values_collection_type::const_iterator i = _organizer->_capability_values.begin();
        std::advance(i, index.row());
        if(index.column() == 0){
            return QString::fromStdString((*i).first->label);
        }else if(index.column() == 1){
            return (*i).second.min;
        }else if(index.column() == 2){
            return (*i).second.max;
        }
    }
    return QVariant();
}

bool scheduler::ResourceCapabilitiesModel::setData(const QModelIndex &index, const QVariant &value, int role){
    if (index.column() < 1 || index.column() > 2)
        return false;
    scheduler::capability_organizer::capability_values_collection_type::iterator i = _organizer->_capability_values.begin();
    std::advance(i, index.row());
    if(index.column() == 1){
        (*i).second.min = value.toInt();
    }else if(index.column() == 2){
        (*i).second.max = value.toInt();
    }
    if(_organizer->_hash_updated % (*i).first->prime != 0)
        _organizer->_hash_updated *= (*i).first->prime;
    return true;
}

boost::shared_ptr<scheduler::capability_organizer> scheduler::ResourceCapabilitiesModel::organizer() const{
    return _organizer;
}
