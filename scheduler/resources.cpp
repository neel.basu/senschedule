/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "resources.h"
#include <boost/foreach.hpp>

// sensiaas::resources::abstract_resource::abstract_resource(): id(counter++){}
// 
// int sensiaas::resources::abstract_resource::counter = 0;

sensiaas::resources::resource_availability& sensiaas::resources::resource_availability::operator=(std::initializer_list<long> values){
    start = (*(values.begin()));
    duration = (*(values.begin()+1));
    recurrence = (*(values.begin()+2));
    return *this;
}

mathematica::m sensiaas::resources::resource_availability::matrix() const{
    return mathematica::List(start, duration, recurrence);
}

bool sensiaas::resources::resource_availabilities::add(const sensiaas::resources::resource_availability& slot){
    _slots.push_back(slot);
    return true;
}

mathematica::m sensiaas::resources::resource_availabilities::matrix() const{
    mathematica::m list("List");
    for(resource_availability slot: _slots){
        list.arg(slot.matrix());
    }
    return list();
}
