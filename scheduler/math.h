/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef SENSIAAS_MATH_H
#define SENSIAAS_MATH_H

#include <boost/array.hpp>

namespace sensiaas{
namespace math{
namespace curves{
struct hilbert;
struct morton;
}
}
}

/**
 * D is the number of dimensions
 * S is the integer type for values on each dimensions
 * Z is the integer type for the curve
 * 
 * in each dimension we accept an 8 bit unsigned integer
 * to encode we need 8*D bit long unsigned integer type
 * e.g. for 4 dimensions return type would be 32 bit uint
 * for 8 dimensions return type would be 64 bit unsigned
 */
template <int D, typename S, typename Z>
struct z_order_curve{
    typedef S source_type;
    typedef Z target_type;
    enum{dimensions = D};
    
    target_type encode(const boost::array<source_type, dimensions>& vector){
        
    }
    boost::array<source_type, dimensions> decode(const target_type& scaler){
        
    }
};

#endif // SENSIAAS_MATH_H
