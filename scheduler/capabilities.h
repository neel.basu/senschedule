/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef CAPABILITIES_H
#define CAPABILITIES_H

#include "box.h"

namespace sensiaas{
    namespace capabilities{
        namespace sensing{
            namespace params{
                struct sensitivity{
                    enum {prime = 2};
                    static constexpr const char* label = "sensitivity";
                };
                struct error{
                    enum {prime = 3};
                    static constexpr const char* label = "error";
                };
                struct throughput{
                    enum {prime = 5};
                    static constexpr const char* label = "throughput";
                };
            }
        }
    }
}

#endif // CAPABILITIES_H
