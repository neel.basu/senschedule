/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef BOX_H
#define BOX_H

#include <cmath>
#include <string>
#include <vector>
#include <boost/tuple/tuple.hpp>
#include "mathematica++/m.h"
#include "mathematica++/declares.h"

namespace sensiaas{
    
template <typename T>
struct range{
    T min;
    T max;
    
    range(){}
    range(T value): min(value), max(value){}
    range(T min_value, T max_value): min(min_value), max(max_value){}
};
    
/**
 * maps a prime number to a parameter T
 * \note asignable and convertible
 * param<contexts::params::sensitivity>
 */
template <typename T, typename V=long long>
struct dimension{
    enum {prime = T::prime};
    typedef T key_type;
    typedef V value_type;
    typedef range<V> range_type;
    typedef dimension<key_type, value_type> self_type;
    
    range_type _value;
    
    self_type& operator=(const range_type& value){
        _value = value;
        return *this;
    }
    self_type& operator=(const value_type& value){
        _value = range_type(value);
        return *this;
    }
    self_type& operator=(std::initializer_list<value_type> values){
        _value = range_type(*(values.begin()), *(values.begin()+1));
        return *this;
    }
    self_type& operator=(const std::pair<value_type, value_type>& value){
        _value.min = value.first;
        _value.max = value.second;
        return *this;
    }
    range_type value() const{
        return _value;
    }
    std::string label() const{
        return std::string(T::label);
    }

};

// http://stackoverflow.com/a/38819454
template <typename V, typename Head, typename... Tail>
struct box;

template <typename V, typename Head>
struct box<V, Head>{
    typedef V                  value_type;
    typedef dimension<Head, V> dimension_type;
    
    enum{length = 1};
    
    dimension_type _head;
    
    dimension_type& get(const dimension_type& /*h*/){
        return _head;
    }
    const dimension_type& get(const dimension_type& /*h*/) const{
        return _head;
    }  
    template <typename F>
    void visit(F& f){
        f(_head);
    }
    protected:
        void params(std::vector<range<V>>& list) const{
            list.push_back(_head.value());
        }
        void matrix(mathematica::m& mat) const{
            mat.arg(mathematica::List((long)_head.prime, (long)_head.value().min, (long)_head.value().max));
        }
};

template <typename V, typename Head, typename... Tail>
struct box: public box<V, Head>, public box<V, Tail...>{
    typedef box<V, Head>    head_type;
    typedef box<V, Tail...> tail_type;
    typedef range<V>        range_type;
    typedef std::vector<range_type> list_type;
    
    enum{length = tail_type::length +1};
    
    using head_type::get;
    using tail_type::get;
    
    template <typename T>
    auto& param(){
        return get(dimension<T, V>());
    }
    
    template <typename T>
    void param(const range_type& range){
        get(dimension<T, V>()) = range;
    }
    
    template <typename T>
    void param(const V& min, const V& max){
        get(dimension<T, V>()) = range_type(min, max);
    }
    
    template <typename T>
    const auto& param() const{
        return get(dimension<T, V>());
    }
    list_type params() const{
        list_type list;
        params(list);
        return list;
    }
    mathematica::m matrix() const{
        mathematica::m list("List");
        matrix(list);
        return list();
    }
    template <typename F>
    void visit(F& f){
        head_type::visit(f);
        tail_type::visit(f);
    }
    protected:
        void params(list_type& list) const{
            head_type::params(list);
            tail_type::params(list);
        }
        void matrix(mathematica::m& mat) const{
            head_type::matrix(mat);
            tail_type::matrix(mat);
        }
};



namespace visitors{
struct print{
    std::ostream& _stream;
    print(std::ostream& stream);
    template <typename Param>
    void operator()(const Param& p){
        _stream << p.label() << "<" << p.prime << "| " << p.value().min << " -> " << p.value().max << ">" << "\n";
    }
};

template <typename T>
struct scalarize_prime{
    range<T> acc;
    
    scalarize_prime(const T& initial=1): acc(initial){}
    
    template <typename Param>
    void operator()(const Param& p){
        acc.min = acc.min * T(std::pow(T(p.prime), p.value().min));
        acc.max = acc.max * T(std::pow(T(p.prime), p.value().max));
    }
    const range<T>& value(){return acc;}
};
}

}


#endif // BOX_H
