/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef RESOURCES_H
#define RESOURCES_H

#include "box.h"
#include "capabilities.h"
#include "mathematica++/tokens.h"
#include "mathematica++/accessor.h"
#include "mathematica++/connector.h"
#include "mathematica++/variant.h"
#include "mathematica++/m.h"
#include "mathematica++/io.h"
#include "mathematica++/operators.h"
#include "mathematica++/declares.h"
#include <vector>
#include <boost/cstdint.hpp>

using namespace mathematica;

namespace sensiaas{
    namespace resources{
        enum types{
            sensor_temperature,
            sensor_humidity,
            sensor_light
        };
        
        struct resource_availability{
            long start;
            long duration;
            long recurrence;
            
            resource_availability& operator=(std::initializer_list<long> values);
            mathematica::m matrix() const;
        };
        
        struct resource_availabilities{
            std::vector<resource_availability> _slots;
            
            /**
             * adds an availability slot in the list and returns sucess value
             * \note if the given slot overlaps with any of the existing slots it doesn't add and returns false
             */
            bool add(const resource_availability& slot);
            /**
             * converts to a mathematica matrix
             */
            mathematica::m matrix() const;
        };

        template <types t, typename Capabilities>
        struct resource{
            typedef std::vector<std::vector<double>> matrix_type;
            enum {type = t};
            
            int id;
            Capabilities _capabilities;
            resource_availability _availability;
            std::vector<long> _observations;
            
            resource(int n): id(n){}
            
            typename Capabilities::list_type params() const{
                return _capabilities.params();
            }
            Capabilities& capabilities(){
                return _capabilities;
            }
            const Capabilities& capabilities() const{
                return _capabilities;
            }
            
            const resource_availability& availability() const{
                return _availability;
            }
            resource_availability& availability(){
                return _availability;
            }
            template <typename T>
            void add_observation(T begin, T end){
                for(T i=begin; i != end; ++i){
                    _observations.push_back(*i);
                }
            }
            matrix_type expectations(mathematica::wrapper& shell) const{
                std::cout << ">> building performance expectation matrix for resource " << id << std::endl;
                symbol x("x");
                value result;
                shell << OscillatingFit(_observations, x, (Rule("MeasureFlactuation") = false, Rule("ThresholdValue") = 64));
                shell >> result;
                matrix_type mat = cast<matrix_type>(result);
                return mat;
            }
            mathematica::m m(mathematica::wrapper& shell) const{
                return mathematica::m("Resource")(id, int(type), 2, _availability.matrix(), capabilities().matrix(), expectations(shell));
            }
        };
        
        typedef sensiaas::box<int,
            sensiaas::capabilities::sensing::params::sensitivity,
            sensiaas::capabilities::sensing::params::error,
            sensiaas::capabilities::sensing::params::throughput
        > sensing_capabilities;
        
        typedef resource<sensor_temperature, sensing_capabilities>  temperature;
        typedef resource<sensor_humidity, sensing_capabilities>     humidity;
        typedef resource<sensor_light, sensing_capabilities>        light;
    }
}

template <sensiaas::resources::types t, typename Capabilities>
mathematica::wrapper& operator<<(mathematica::wrapper& stream, const sensiaas::resources::resource<t, Capabilities>& resource){
    stream << resource.m();
    return stream;
}

#endif // RESOURCES_H
