/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef MULTIPLOTVIEWER_H
#define MULTIPLOTVIEWER_H

#include <QWidget>
#include <QGraphicsScene>
#include "mathematica++/connector.h"
#include "mathematica++/m.h"
#include <boost/filesystem.hpp>
#include <QFuture>
#include <QMutex>
#include <QFutureWatcher>

namespace Ui{
    class MultiPlotViewer;
}

class MultiPlotViewer: public QWidget{
  Q_OBJECT
  private:
    Ui::MultiPlotViewer* ui;
    QGraphicsScene* _scene;
    mathematica::connector& _conn;
    QMutex _mutex;
    int _count;
  public:
    MultiPlotViewer(mathematica::connector& conn, QWidget* parent=0);
  public:
    QGraphicsScene* scene() const;
    /**
     * render's result of the given mathematica expression as an image window (QLabel) in the Graphics Scene
     */
    void render(const mathematica::m& expression, const std::string& label, const boost::filesystem::path& path="");
    void render(const std::string& label, const boost::filesystem::path& path);
  private slots:
    void rendered_slot(const QImage& image, const QString& label);
  private:
    QImage ev(const mathematica::m& expression, const std::string& label, const boost::filesystem::path& path);
  private:
    std::list<QFuture<QImage>>         _futures;
    std::list<QFutureWatcher<QImage>*> _watchers;
  private:
    void watch(const QFuture<QImage>& future, const std::string& label);
  signals:
    void rendered(const QImage& image, const QString& label);
  private slots:
    void task_finished_slot(const QFuture<QImage>& image, QFutureWatcher<QImage>* watcher);
  public slots:
    void reset();
};

#endif // MULTIPLOTVIEWER_H
