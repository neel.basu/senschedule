#include "schedulerqt.h"
#include "ui_schedulerqt.h"
#include "resourcewidget.h"
#include "availabilitywidget.h"
#include "capabilitieswidget.h"
#include <QToolBox>
#include <QPushButton>

SchedulerMainWindow::SchedulerMainWindow(mathematica::connector& conn, QWidget* parent): QMainWindow(parent), _conn(conn), ui(new Ui::schedulerqt){
    ui->setupUi(this);
    
    _viewer = new MultiPlotViewer(_conn);
    ui->multiPlotViewerDock->setWidget(_viewer);
    
    ui->resourcesToolBox->removeItem(0);
    ui->resourcesToolBox->addItem(new ResourceWidget(_conn, _viewer), QString("Resource %1").arg(ui->resourcesToolBox->count()+1));
    
    connect(ui->addResourceAction, &QAction::triggered, [this](){
       ui->resourcesToolBox->addItem(new ResourceWidget(_conn, _viewer), QString("Resource %1").arg(ui->resourcesToolBox->count()+1)); 
    });
    
    ui->solutionDockWidget->setFeatures(QDockWidget::DockWidgetVerticalTitleBar | QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetClosable);
    
    ui->mainToolBar->addAction(ui->solutionDockWidget->toggleViewAction());
    ui->mainToolBar->addAction(ui->multiPlotViewerDock->toggleViewAction());
}

SchedulerMainWindow::~SchedulerMainWindow(){
    delete ui;
}
