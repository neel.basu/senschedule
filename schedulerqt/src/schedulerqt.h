#ifndef SCHEDULERQT_H
#define SCHEDULERQT_H

#include "mathematica++/connector.h"
#include <QMainWindow>
#include "multiplotviewer.h"

namespace Ui {
    class schedulerqt;
}

class SchedulerMainWindow : public QMainWindow{
  Q_OBJECT
  mathematica::connector& _conn;
  public:
    explicit SchedulerMainWindow(mathematica::connector& conn, QWidget *parent = 0);
    ~SchedulerMainWindow();
  private:
    Ui::schedulerqt* ui;
    MultiPlotViewer* _viewer;
};

#endif // SCHEDULERQT_H
