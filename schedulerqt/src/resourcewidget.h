/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef RESOURCEWIDGET_H
#define RESOURCEWIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include <QtSvg/QSvgWidget>
#include "multiplotviewer.h"
#include "mathematica++/connector.h"

namespace Ui{
    class ResourceWidget;
}

class AvailabilityWidget;
class CapabilitiesWidget;

class ResourceWidget : public QWidget{
  Q_OBJECT
  private:
    Ui::ResourceWidget* ui;
    AvailabilityWidget* availability_widget;
    CapabilitiesWidget* capabilities_widget;
    MultiPlotViewer*    _viewer;
    mathematica::connector& _conn;
  public:
    explicit ResourceWidget(mathematica::connector& conn, MultiPlotViewer* viewer, QWidget* parent=0);
    virtual ~ResourceWidget();
  public slots:
    void add_observation_slot();
    void compute_visualizations_slot();
    void compute_oscifit_slot();
    
};

#endif // RESOURCEWIDGET_H
