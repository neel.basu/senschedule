/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <QLabel>
#include <QTime>
#include <QCheckBox>
#include <QLayoutItem>
#include <string>
#include <boost/algorithm/string.hpp>
#include <boost/bind.hpp>
#include "multiplotviewer.h"
#include "ui_multiplotviewer.h"
#include "mathematica++/io.h"
#include "mathematica++/declares.h"
#include <QString>
#include <QByteArray>
#include <QtConcurrent/QtConcurrent>
#include <QtConcurrent/QtConcurrentRun>

MATHEMATICA_DECLARE(Export)
MATHEMATICA_DECLARE(ExportString)

MultiPlotViewer::MultiPlotViewer(mathematica::connector& conn, QWidget* parent): ui(new Ui::MultiPlotViewer), _conn(conn), _count(0){
    setWindowTitle("Plot Viewer");
    ui->setupUi(this);
    _scene = new QGraphicsScene;
    ui->graphicsView->setScene(_scene);
    connect(this, &MultiPlotViewer::rendered, this, &MultiPlotViewer::rendered_slot);
}

QGraphicsScene * MultiPlotViewer::scene() const{
    return _scene;
}

QImage MultiPlotViewer::ev(const mathematica::m& expression, const std::string& label, const boost::filesystem::path& path){
    QImage image;
    _mutex.lock();
    std::cout << std::endl << "mbegin " << label << " " << QTime::currentTime().toString().toStdString() << std::endl;
    try{
        if(!path.empty()){//export to path and restore in QImage
            _conn << Export(path.string(), expression);
            image.load(path.string().c_str());
        }else{// decode the base64 encoded return and restore in QImage
            mathematica::value b64encoded_buffer;
            _conn << ExportString(ExportString(expression, std::string("PNG")), std::string("Base64"));
            _conn >> b64encoded_buffer;
            // std::cout << b64encoded_buffer << std::endl;
            std::string b64encoded_string = b64encoded_buffer->stringify();
            boost::replace_all(b64encoded_string, "\\012", "");
            QByteArray buffer = QByteArray::fromBase64(b64encoded_string.c_str());

            image.loadFromData(buffer);
        }
    }catch(const std::exception& ex){
        std::cout << ex.what() << std::endl;
    }
    std::cout << std::endl << "mend " << label << " " << QTime::currentTime().toString().toStdString() << std::endl;
    _mutex.unlock();
    return image;
}

void MultiPlotViewer::render(const mathematica::m& expression, const std::string& label, const boost::filesystem::path& path){
    QFuture<QImage> future = QtConcurrent::run(boost::bind(&MultiPlotViewer::ev, this, expression, label, path));
    watch(future, label);   
    
    ++_count;
}

void MultiPlotViewer::render(const std::string& label, const boost::filesystem::path& path){
    QImage image;
    image.load(path.string().c_str());
    rendered_slot(image, QString::fromStdString(label));
    
    ++_count;
}

void MultiPlotViewer::watch(const QFuture<QImage>& future, const std::string& label){
    QFutureWatcher<QImage>* watcher = new QFutureWatcher<QImage>;
    watcher->setFuture(future);
    connect(watcher, &QFutureWatcher<QImage>::finished, boost::bind(&MultiPlotViewer::task_finished_slot, this, future, watcher));
    _watchers.push_back(watcher);
    _futures.push_back(future);
    watcher->setProperty("label", QString::fromStdString(label));
}

void MultiPlotViewer::task_finished_slot(const QFuture<QImage>& future, QFutureWatcher<QImage>* watcher){
    QImage image = future.result();
    QString label = watcher->property("label").toString();
    _futures.remove(future);
    _watchers.remove(watcher);
    delete watcher;
    watcher = 0x0;
    emit rendered(image, label);
}

void MultiPlotViewer::rendered_slot(const QImage& image, const QString& label){
    std::cout << "qbegin " << QTime::currentTime().toString().toStdString() << std::endl;
    QLabel* label_window = new QLabel;
    label_window->setWindowTitle(label);
    label_window->setScaledContents(true);
    label_window->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    _scene->addWidget(label_window, Qt::Window);
    label_window->setPixmap(QPixmap::fromImage(image));
    ui->status->setText(QString("%1/%2").arg(_count-_futures.size()).arg(_count));
    
    QCheckBox* checkbox = new QCheckBox(label);
    ui->checkLayout->addWidget(checkbox);
    checkbox->setCheckState(Qt::Checked);
    
    connect(checkbox, &QCheckBox::stateChanged, label_window, &QLabel::setVisible);
    std::cout << "qend " << QTime::currentTime().toString().toStdString() << std::endl;
}

void MultiPlotViewer::reset(){
    _scene->clear();
    while(QLayoutItem* item = ui->checkLayout->takeAt(0)){
        if(QWidget* widget = item->widget()) widget->deleteLater();
    }
    _count = 0;
}
