/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "resourcewidget.h"
#include "ui_resourcewidget.h"
#include "availabilitywidget.h"
#include "capabilitieswidget.h"
#include <QLayout>
#include <QVBoxLayout>
#include <QListWidget>
#include <QTableWidget>
#include <QDialog>
#include <QInputDialog>
#include <QLineEdit>
#include <QLabel>
#include <QPlainTextEdit>
#include <QDialogButtonBox>
#include <QPlainTextEdit>
#include <QStringList>
#include <QImage>
#include <QByteArray>
#include <QString>
#include <QPixmap>
#include <QSize>
#include <QGraphicsScene>
#include <QtSvg/QSvgWidget>
#include <QGraphicsProxyWidget>
#include <QTableWidgetItem>
#include <QDir>

#include <algorithm>
#include <functional>
#include <list>
#include <vector>
#include <boost/bind.hpp>
#include <QDebug>

#include <iostream>

// #include <boost/algorithm/string.hpp>

#include "mathematica++/tokens.h"
#include "mathematica++/accessor.h"
#include "mathematica++/connector.h"
#include "mathematica++/variant.h"
#include "mathematica++/m.h"
#include "mathematica++/io.h"
#include "mathematica++/operators.h"
#include "mathematica++/declares.h"
#include <boost/tuple/tuple_io.hpp>
#include <boost/filesystem/path.hpp>

#include "config.h"

MATHEMATICA_DECLARE(OscillatingFit)
MATHEMATICA_DECLARE(ListLinePlot)
MATHEMATICA_DECLARE(Rasterize)
MATHEMATICA_DECLARE(MatrixForm)
MATHEMATICA_DECLARE(TableForm)
MATHEMATICA_DECLARE(Export)
MATHEMATICA_DECLARE(ExportString)

using namespace mathematica;

ResourceWidget::ResourceWidget(connector& conn, MultiPlotViewer* viewer, QWidget* parent): ui(new Ui::ResourceWidget), _viewer(viewer), _conn(conn){
    ui->setupUi(this);
    availability_widget = new AvailabilityWidget(this);
    capabilities_widget = new CapabilitiesWidget(this);
    ui->availability_layout->addWidget(availability_widget);
    ui->availability_layout->addStretch();
    ui->capabilities_layout->addWidget(capabilities_widget);
    ui->capabilities_layout->addStretch();
    
    connect(ui->add_observation, SIGNAL(clicked()), this, SLOT(add_observation_slot()));
    connect(ui->plot_observations, SIGNAL(clicked()), this, SLOT(compute_visualizations_slot()));
    connect(ui->compute_oscifit, SIGNAL(clicked()), this, SLOT(compute_oscifit_slot()));
}

ResourceWidget::~ResourceWidget(){
    delete ui;
    if(availability_widget) delete availability_widget;
    if(capabilities_widget) delete capabilities_widget;
}

void ResourceWidget::add_observation_slot(){
    QDialog dialog;
    QVBoxLayout* layout = new QVBoxLayout;
    QPlainTextEdit* editor = new QPlainTextEdit;
    layout->addWidget(new QLabel("Enter comma(,) seperated values"));
    layout->addWidget(editor);
    QDialogButtonBox* buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    
    connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));
    layout->addWidget(buttons);
    
    dialog.setLayout(layout);
    
    if(dialog.exec() == QDialog::Accepted){
        QString value = editor->toPlainText();
        ui->observations_list_widget->addItem(value);
    }
}

void ResourceWidget::compute_visualizations_slot(){
    for(int i=0; i < ui->observations_list_widget->count(); ++i){
        QString value = ui->observations_list_widget->item(i)->text();
        QStringList list = value.split(",");
        std::vector<int> values;
        std::transform(list.begin(), list.end(), std::back_inserter(values), boost::bind(&QString::toInt, _1, (bool*)0x0, 10));
        std::copy(values.begin(), values.end(), std::ostream_iterator<int>(std::cout << std::endl));

        _viewer->render(ListLinePlot(values), QString("Plot %1").arg(i).toStdString());
    }
    _viewer->show();
}

void ResourceWidget::compute_oscifit_slot(){
    std::vector<int> data;
    for(int i=0; i < ui->observations_list_widget->count(); ++i){
        QString value = ui->observations_list_widget->item(i)->text();
        QStringList list = value.split(",");
        std::transform(list.begin(), list.end(), std::back_inserter(data), boost::bind(&QString::toInt, _1, (bool*)0x0, 10));
    }
    
//     std::vector<int> data = {102,5,0,753,51,135,159,226,198,481,865,537,97,243,328,300,85,181,152,227,192,119,113,135,119,130,119,175,136,193,345,339,158,345,334,187,79,119,305,294,232,215,323,243,85,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,203,0,0,0,0,0,0,0,0,12,1595,412,476,152,51,0,0,0,34,0,0,0,0,0,0,0,0,1392,175,17,0,0,0,911,282,198,210,186,153,102,175,153,113,215,62,0,0,0,0,0,0,0,747,0,0,0,0,2708,716,724,660,663,610,743,670,680,618,700,683,664,612,717,638,664,598,696,656,665,596,720,674,634,634,703,706,667,604,714,648,659,601,729,671,683,607,708,681,686,618,699,678,673,613,689,651,671,624,681,671,663,592,702,693,686,617,717,673,666,628,699,679,686,611,724,666,644,606,718,650,660,602,677,644,662,620,709,430,357,45,6,260,226,136,45,28,40,79,266,198,130,28,102,108,118,272,1810,1149,552,711,678,465,158,182,159,277,198,147,130,34,0,0,0,0,0,11,23,0,0,0,0,0,0,0,0,0,73,23,0,0,0,0,0,0,0,0,0,0,0,0,0,2434,599,315,114,73,130,102,107,46,220,142,169,63,288,272,265,164,464,238,164,181,266,215,220,187,175,210,209,164,226,272,899,2121,671,690,612,702,670,655,604,705,675,659,608,290,888,689,733,724,659,651,600,720,663,673,619,702,660,646,619,686,691,671,591,701,755,754,689,331,560,396,531,102,2313,677,606,709,653,669,601,710,665,656,580,712,640,677,597,726,648,657,628,642,656,668,617,716,654,657,598,721,707,660,613,712,655,665,624,710,670,688,597,708,657,675,612,708,662,677,625,718,673,636,627,735,681,659,593,728,626,625,628,727,646,638,621,702,674,631,610,679,664,630,638,807,716,666,634,143,256,147,288,153,277,317,435,645,1267,136,175,119,192,102,57,68,22,40,124,0,0,0,0,0,0,0,0,1296,73,85,17,62,130,0,0,0,0,0,0,0,0,6,28,0,0,0,0,0,187,0,96,119,186,74,90,63,90,57,164,152,232,187,283,22,164,181,295,198,152,130,193,169,311,238,311,153,0,0,0,1007,560,837,1301,741,196,147,135,97,124,102,96,0,0,0,0,0,0,0,0,0,0,651,520,882,1810,911,599,711,675,686,593,699,670,651,612,681,640,699,631,688,666,690,624,703,677,653,596,695,672,673,633,708,650,653,598,720,665,654,620,701,657,663,603,672,648,670,593,701,656,682,608,724,643,616,579,419,639,922,734,720,651,698,612,705,692,645,607,719,634,691,592,326,1055,655,607,725,679,643,580,766,656,647,340,153,192,91,107,238,345,232,169,136,419,254,0,0,0,12,0,0,0,0,0,0,0,5,6,0,0,0,0,0,0,130,0,0,0,0,0,571,193,169,204,136,526,475,209,158,1861,843,622,919,644,642,355,40,51,164,214,46,333,408,407,701,2247,590,715,689,670,648,624,715,657,691,610,704,654,676,631,706,642,667,609,710,645,672,606,713,687,668,624,711,671,119,130,0,2447,658,627,694,681,644,622,711,676,694,561,762,662,670,598,703,670,673,603,703,673,648,619};
    
    _conn.import(M_SENSCHEDULE_LIB_PATH);
    mathematica::symbol x("x");
    mathematica::value result;
    
    _conn << OscillatingFit(data, x, (Rule("MeasureFlactuation") = false, Rule("ThresholdValue") = 64, Rule("VisualizationPath") = std::string("vis.png")));
    _conn >> result;
    
    std::cout << result << std::endl;

    typedef std::vector<std::vector<double>> matrix_type;
    matrix_type mat = cast<matrix_type>(result->serialize());

    std::cout << mat.size() << std::endl;
    BOOST_FOREACH(auto row, mat){
        BOOST_FOREACH(double cell, row){
            std::cout << cell << ", ";
        }
        std::cout << std::endl;
    }
    
    ui->partitionTable->setColumnCount(5);
    ui->partitionTable->setRowCount(mat.size());
    
    for(int i = 0; i < mat.size(); ++i){
        for(int j = 0; j < mat[i].size(); ++j){
             QTableWidgetItem* item = new QTableWidgetItem(QString("%1").arg(mat[i][j]));
             ui->partitionTable->setItem(i, j, item);
        }
    }
    
    _viewer->render("OsciFit", boost::filesystem::path("vis.png"));
    _viewer->show();
}
