#include "schedulerqt.h"
#include <QApplication>
#include <QStyleFactory>
#include "mathematica++/connector.h"
#include <QDebug>

int main(int argc, char *argv[]){
//     mathematica::connector shell(argc, argv);
    mathematica::connector shell;
    QApplication app(argc, argv);
//    qDebug() << QStyleFactory::keys();
//    app.setStyle(QStyleFactory::create("Fusion"));
    
    SchedulerMainWindow w(shell);
    w.show();

    return app.exec();
}

