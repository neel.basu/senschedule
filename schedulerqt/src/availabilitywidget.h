/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef AVAILABILITYWIDGET_H
#define AVAILABILITYWIDGET_H

#include <QWidget>
#include <QDateTime>

namespace Ui{
    class AvailabilityWidget;
}

class AvailabilityWidget : public QWidget{
    Q_OBJECT
    private:
      Ui::AvailabilityWidget* ui;
    public:
      explicit AvailabilityWidget(QWidget* parent = 0);
      virtual ~AvailabilityWidget();
    public slots:
      void on_sedit_changed(const QDateTime& datetime);
      void on_dedit_changed(const QDateTime& datetime);
      void on_kedit_changed(const QDateTime& datetime);
};

#endif // AVAILABILITYWIDGET_H
