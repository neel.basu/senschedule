/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright (C) 2017  <copyright holder> <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "availabilitywidget.h"
#include "ui_availabilitywidget.h"
#include <QString>


AvailabilityWidget::AvailabilityWidget(QWidget* parent): QWidget(parent) ,ui(new Ui::AvailabilityWidget){
    ui->setupUi(this);
    connect(ui->sedit, SIGNAL(dateTimeChanged(const QDateTime&)), this, SLOT(on_sedit_changed(const QDateTime&)));
    connect(ui->dedit, SIGNAL(dateTimeChanged(const QDateTime&)), this, SLOT(on_dedit_changed(const QDateTime&)));
    connect(ui->kedit, SIGNAL(dateTimeChanged(const QDateTime&)), this, SLOT(on_kedit_changed(const QDateTime&)));
}

AvailabilityWidget::~AvailabilityWidget(){
    delete ui;
}

void AvailabilityWidget::on_sedit_changed(const QDateTime& datetime){
    ui->sline->setText(QString::number(datetime.toTime_t()));
    ui->dedit->setMinimumDateTime(datetime.addSecs(1));
}
void AvailabilityWidget::on_dedit_changed(const QDateTime& datetime){
    ui->dline->setText(QString::number(datetime.toTime_t()));
    ui->kedit->setMinimumDateTime(datetime.addSecs(1));
}
void AvailabilityWidget::on_kedit_changed(const QDateTime& datetime){
    ui->kline->setText(QString::number(datetime.toTime_t()));
}
