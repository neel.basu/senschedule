(* ::Package:: *)

BeginPackage["SenSchedule`"]
OscillatingFit::usage ="Transforms a time series to a characteristic expression which is a set of N consecutive Partitions where each partition is expressed as a tuple (begin, end, size, weight, flactuation)";
MakeZone::usage = "Transforms a set of K characteristic expressions into zones by intersecting the partitions";
PlotPartitions::usage = "Plot partitions of a sernsor";
FindSchedule::usage = "Finds a schedule for a given set of partitions of multiple sensors Using Shortest Path";
ScalarizeF::usage = "Scalarize Capabilities matrix"
ScalarizeRestrictedF::usage = "Scalarize restricted parametsr of Capabilites matrix"
Resource::usage = "";
Request::usage = "";
OverlapsF::usage = "";
OverlappingF::usage = "";
FeasibilityMatrix::usage = "";
ResourceJSON::usage = "";
RequestJSON::usage = "";
ScheduleSlices::usage = "";
id::usage = "";
type::usage = "";
availability::usage = "";
context::usage = "";
capabilities::usage="";
expectations::usage = "";
slot::usage = "";
Begin["`Private`"]


Unprotect[LongRightArrow];
SetAttributes[LongRightArrow,HoldRest]
LongRightArrow[obj_,property_]:= obj[SymbolName[property]];
LongRightArrow[obj_,head_[f_,args___]]:=head[LongRightArrow[obj,f],args];
Protect[LongRightArrow];

WallQ=Function[{range,length},\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{
RowBox[{"-", "1"}], 
RowBox[{
RowBox[{"First", "[", "range", "]"}], "==", "1"}]},
{"1", 
RowBox[{
RowBox[{"Last", "[", "range", "]"}], "==", "length"}]},
{"0", "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\)];
SplitF=Function[{row,pos},
	Block[{bk,ek,ep,bq,s},
		bk=row[[1]];ek=row[[2]];ep=pos;bq=pos;s=row[[3]];
		With[{\[Rho]p=s/(pos-bk),\[Rho]q=s/(ek-pos)},({
		 {bk, ep, s/\[Rho]p, row[[4]], row[[5]]},
		 {bq, ek, s/\[Rho]q, row[[4]], row[[5]]}
		})]
	]
];
MergeF=Function[{l},With[{
	b=Min[l[[All,1]]],
	e=Max[l[[All,2]]],
	p=Total[l[[All,3]]],
	w=\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(Length[l]\)]\(l[\([i, 3]\)]*l[\([i, 4]\)]/Total[l[\([All, 3]\)]]\)\),
	f=\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(Length[l]\)]\(l[\([i, 3]\)]*l[\([i, 5]\)]/Total[l[\([All, 3]\)]]\)\)
	},{b,e,p,w,f}]
];
FixF=Function[{begin,range,end,matrix},
	Block[{k,s=Flatten[{begin,range,end}],l,mat=matrix},
		l=MinMax[(s/.Null->Nothing)];
		k=MergeF[matrix[[First[range];;Last[range]]]];
		k=MergeF[{If[TrueQ[begin==Null],Nothing,matrix[[begin]]],k,If[TrueQ[end==Null],Nothing,matrix[[end]]]}];
		mat[[First[l]]]=k;
		mat[[First[l]+1;;Last[l]]]=0Range[Length[k]];
		mat
	]
];
DistributeF=Function[{begin,range,end,matrix},
	Block[{m=With[{pl=matrix[[begin,3]],pr=matrix[[end,3]],a=matrix[[First[range],1]],b=matrix[[Last[range],2]]},(pr*a+pl*b)/(pl+pr)],db,adb,i,v,r,left,right,mat=matrix},
		db=Select[matrix[[First[range];;Last[range],1]]-m,#1<=0&];
		adb=MapThread[Flatten[{#1,#2}]&,{db, Range[Length[db]]+begin}];
		{v,i}=Last[adb];(*index of the nearest partition*)
		r=SplitF[matrix[[i]],m];
		left=MergeF[Append[matrix[[begin;;i-1]],r[[1]]]];
		right=MergeF[Prepend[matrix[[i+1;;end]],r[[2]]]];
		mat[[begin]]=left;
		mat[[end]]=right;
		mat[[begin+1;;end-1]]=0Range[Length[left]];
		mat
	]
];
SmoothF=Function[{mat,row},
	Block[{sum,r=Reverse[row]},
		sum=Total[row[[All,3]]];
		If[sum>=threshold,
			FixF[Null,{First[r][[6]],Last[r][[6]]},Null,mat],
			Block[{wall},
				wall=WallQ[r[[All,6]],Length[mat]];
				Switch[wall,
					-1,FixF[Null,{First[r][[6]],Last[r][[6]]},Last[r][[6]]+1,mat],
					+1,FixF[First[r][[6]]-1,{First[r][[6]],Last[r][[6]]},Null,mat],
					0,If[EvenQ[Length[r]],
						DistributeF[First[r][[6]]-1,{First[r][[6]],Last[r][[6]]},Last[r][[6]]+1,mat],
						FixF[First[r][[6]]-1,{First[r][[6]],Last[r][[6]]},Last[r][[6]]+1,mat]
					]
				]
			]
		]
	]
];
PlotPartitions[data_, partitions_, smooth_, t_Symbol:Null, fourierfit_:Null, apprxfit_:Null]:=Block[
{mean,intersections=Prepend[partitions[[All,2]],0],texts,cuts={},switch=0,plottables={}},
	mean = Mean[data];
	AppendTo[cuts,#]&/@{{#[[1]],#[[3]]+mean},{#[[2]],#[[3]]+mean}}&/@smooth[[All,{1,2,4}]];
	texts=Tooltip[ Text[StringForm["(`1`) `2`/`3`",switch+1,#[[4]]+mean,#[[5]]], 
		{#[[2]]-#[[3]]/2, (#[[4]]+mean)*(Mod[switch++,3]+1.2)}, Background->LightYellow],
		#[[4]]+mean]&/@smooth;
	AppendTo[plottables,ListLinePlot[data,PlotStyle->Red]];
	If[Not[t===Null],
		If[Not[fourierfit===Null],AppendTo[plottables,Plot[fourierfit+mean,{t, 0,Length[data]},PlotStyle->{Green,{}}]]];
		If[Not[apprxfit===Null],AppendTo[plottables,Plot[apprxfit+mean,{t, 0,Length[data]},PlotStyle->{Pink,{}}]]];
	];
	AppendTo[plottables,ListLinePlot[cuts,PlotStyle->Blue]];
	AppendTo[plottables,Graphics[texts]];
	Show[plottables,GridLines->{intersections, {}},GridLinesStyle->Directive[Magenta,Dashed]]
];
Options[OscillatingFit]={MeasureFlactuation->True, ThresholdValue->64,ApproximationHarmonics->8,VisualizationPath->"vis.png"};
OscillatingFit[data_, t_Symbol, OptionsPattern[]]:=Block[
{d,mean,complex , mpf, magphases, freqs, eqs, fourierfit, unsorted, sorted, apprxfit, intersections, partitions={},regions, smoothregions, cuts={},apprx=OptionValue[ApproximationHarmonics]},
	mean = Mean[data];
	d= data - mean;
	complex = Fourier[d, FourierParameters-> {1,-1}];
	mpf = Block[
	{i=0, T =Length[d](* observation time *)},
	Function[x, { Abs[x], Arg[x], i++/T}]/@complex
	 ] ;
	mpf = mpf[[1;;Floor[Length[mpf]/2]]];
	eqs = Function[{f}, With[
	{amplitude = f[[1]]/Length[d], phase = f[[2]], w=f[[3]]},
	2*amplitude* Cos[2 \[Pi] w*t+phase]
	]]/@ mpf ;
	unsorted=Thread[{mpf[[All, 1]], eqs}];
	sorted = Reverse[SortBy[unsorted, #[[1]] &]];
	(*Print[unsorted//MatrixForm, sorted //MatrixForm];*)
	fourierfit = \!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(Length[eqs]\)]\(eqs[\([i]\)]\)\);
	apprxfit={\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(apprx\)]\(sorted[\([i, \ 2]\)]\)\), \!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(apprx\)]\(unsorted[\([i, \ 2]\)]\)\)};
	intersections=Block[{sfreqs, gcd, period},
		sfreqs = Reverse[SortBy[mpf[[All, {1, 3}]], #[[1]] &]][[1;;apprx, 2]];
		gcd = Apply[GCD,sfreqs];
		period = gcd^-1;
		t/.Normal[Solve[Rationalize[apprxfit[[1]]]==0&&0<=t<=period, t, Reals]]
	];
	PrependTo[intersections, 0];
	AppendTo[intersections, Length[d]];
	partitions = Table[{intersections[[i]], intersections[[i+1]]}, {i, 1, Length[intersections]-1}];
	(*Print[partitions//MatrixForm];*)
	regions=Block[{integral,weights},
		integral=1/(v-u) \!\(
\*SubsuperscriptBox[\(\[Integral]\), \(u\), \(v\)]\(Rationalize[fourierfit] \[DifferentialD]t\)\);
		weights = Block[{b,e, w,s},
			{b,e}=#;
			w=integral/.{u->b,v->e};
			s=If[TrueQ@OptionValue[MeasureFlactuation],
				t/.Normal[Solve[Rationalize[fourierfit]==w && b<=t<=e, t, Reals]],
				0
			];
			(*s = t/.Normal[Solve[Rationalize[fourierfit]\[Equal]w && b\[LessEqual]t\[LessEqual]e, t, Reals]];*)
			{e-b, w,Length[s]/(e-b)}
		]&/@partitions;
		MapThread[Flatten[{#1,#2}]&,{partitions, weights}]
	];
	(*Print[regions//MatrixForm];*)
	smoothregions=Block[{annotated,sortedregions, diffs, max,threshold, small, g},
		annotated=MapThread[Flatten[{#1,#2}]&,{regions, Range[Length[regions]]}];
		sortedregions = Reverse[SortBy[annotated, #[[3]] &]];
		diffs = Differences[sortedregions[[All, 3]]];
		threshold=OptionValue[ThresholdValue];
		max = Length[(sortedregions[[All, 3]]-threshold)/.x_/;x<0->Nothing];
		small =Reverse[SortBy[sortedregions[[max+1;;Length[sortedregions]]], #[[6]]&]];
		g=Block[{groups={},back=0},
			Function[row,
				If[row[[6]]+1!=back,
					AppendTo[groups,{row}],
					groups[[Length[groups]]]=Append[groups[[Length[groups]]],row]
				];
				back=row[[6]];
			]/@small;
			groups
		];
		Fold[SmoothF,regions,g] /. ConstantArray[0,Length[First@regions]]->Nothing
	];
	(*Print[regions//MatrixForm,smoothregions//MatrixForm];*)
	(*{"partitions"-> regions, "smooth"-> smoothregions,"fit"->fourierfit,"approximated"->apprxfit[[1]]}*)
	Export[OptionValue[VisualizationPath],PlotPartitions[data,regions,smoothregions,t,fourierfit,apprxfit[[1]]],ImageSize->Large];
	smoothregions+ConstantArray[{0, 0, 0, mean, 0}, Length[smoothregions]]
];
MakeZone[ps_]:= Block[{ends=#[[All,2]]&/@ps,intersections,lines,zones,f},
	intersections = Sort[Union[Flatten[ends]]];
	f=Function[{psi},
		Block[{u},
			u = Complement[intersections,psi[[All,2]]];
			Fold[Block[{points=#1[[All,2]],pos,w,splitted},
				pos = Length[(points-#2)/.x_/;x>0->Nothing]+1;
				splitted=SplitF[#1[[pos,All]], #2];
				ArrayReshape[#1/.#1[[pos,All]]->splitted,{Length[#1]+1,5}]
			]&,psi,u]
		]
	];
	zones =f/@ps
];

HopCostF[ps_,i_,j_,p_,q_,\[Epsilon]_]:=Block[{source,target},
	source = ps[[i,j]];(*j'th partition row of i'th resource*)
	target = ps[[p,q]];(*q'th partition row of p'th resource*)
	\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{"0", 
RowBox[{"p", "==", "i"}]},
{"\[Epsilon]", "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False], 
RowBox[{
RowBox[{
RowBox[{"target", "[", 
RowBox[{"[", "1", "]"}], "]"}], "<=", 
RowBox[{"source", "[", 
RowBox[{"[", "2", "]"}], "]"}]}], "&&", " ", 
RowBox[{
RowBox[{"target", "[", 
RowBox[{"[", "2", "]"}], "]"}], ">", " ", 
RowBox[{"source", "[", 
RowBox[{"[", "2", "]"}], "]"}], " "}]}]},
{"\[Infinity]", "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\)
];
Options[HopCostEdges]={IncludeInfinity->False,WeightCombined->False};
HopCostEdges[ps_,\[Epsilon]_,OptionsPattern[]]:=Block[{nr = Length[ps] (*n resources*),np=Length/@ps (*n partitions*),edges={},c=0},
	For[i=1,i<=nr,i++,
		For[j=1,j<= np[[i]],j++,
			For[p=1,p<=nr,p++,
				For[q=1,q<= np[[p]],q++,
					Block[{h},
						h=HopCostF[ps,i,j,p,q,\[Epsilon]];
						If[OptionValue[WeightCombined],h= (ps[[p,q,4]]*ps[[p,q,3]])-h];
						If[OptionValue[IncludeInfinity]||Abs[h]!=\[Infinity],
							AppendTo[edges,
								{Complex[i,j]\[DirectedEdge]Complex[p,q], h, Complex[i,j]\[DirectedEdge]Complex[p,q]->Placed[h, (4+(Mod[c++,3]))/8]}
							]
						]
					]
				]
			]
		]
	];
	edges
];

HighlightGraphR[g_, vertices_] := Block[{vt, gt},
    vt = ToString /@ vertices;
    gt = Fold[SetProperty[{#1,#2},EdgeLabels->Block[{e=#2,vp=List@@#2},
        ToString[First[vp]]\[DirectedEdge]ToString[Last[vp]]->PropertyValue[{#1,e},EdgeWeight]
    ]&]&, g, EdgeList[g]];    
    gt = VertexReplace[gt, Thread[VertexList[gt] -> ToString /@ VertexList[gt]]];    
    HighlightGraph[gt, PathGraph[vt, DirectedEdges->True]]
]

VertexShapeF[{xc_, yc_}, name_, {w_, h_}] := {
	LightYellow, 
	Rectangle[{xc - w, yc - h}, {xc + w, yc + h}], 
	Inset[Style[Text[Piecewise[{
		{"S", (First[#]==0 && Last[#]==0)}, 
		{"T", (First[#]==-1 && Last[#]==-1)}
	}, Subscript["P", First[#],Last[#]]]]&[{Re[name], Im[name]}], 12, Black], {xc, yc}]
};

Options[HopCostGraph]={IncludeInfinity->False,WeightCombined->False,NegeteEdgeWeight->False};
HopCostGraph[ps_,\[Epsilon]_,OptionsPattern[]]:=Block[{edges={},vertices={},vlabels={},nr = Length[ps] (*n resources*),np=Length/@ps (*n partitions*)},
	edges=HopCostEdges[ps,\[Epsilon],IncludeInfinity->OptionValue[IncludeInfinity],WeightCombined->OptionValue[WeightCombined]];
	For[i=1,i<=nr,i++,
		For[j=1,j<= np[[i]],j++,
			AppendTo[vertices, (* map of vertex weights *)
				If[OptionValue[WeightCombined], (* when weightcombined is on cost is put on edges, so vertex weight (reward) is 0; otherwise reward is s*w *)
					0, 
					Complex[i,j]->(ps[[i,j,4]]*ps[[i,j,3]])
				]
			];
			AppendTo[vlabels,
				If[OptionValue[WeightCombined],
					Complex[i,j]->Subscript["P", i, j],
					Complex[i,j]->ps[[i,j,4]]Subscript["P", i, j]
				]
			];
		]
	];
	Block[{},
		For[i=1,i<=nr,i++,
			AppendTo[edges, (* a list of tuple {edge, weight, edge\[Rule]weight} where weight = s*w  *)
				{Complex[0,0]\[DirectedEdge]Complex[i,1], (ps[[i,1,4]]*ps[[i,1,3]]), Complex[0,0]\[DirectedEdge]Complex[i,1]->(ps[[i,1,4]]*ps[[i,1,3]])}
			];
			AppendTo[edges,
				{Complex[i,Length[ps[[i]]]]\[DirectedEdge]Complex[-1,-1], 0, Complex[i,Length[ps[[i]]]]\[DirectedEdge]Complex[-1,-1]->0}
			];
			AppendTo[vertices,#]&/@{Complex[0,0]->0,Complex[-1,-1]->0};
			AppendTo[vlabels,#]&/@{Complex[0,0]->"S",Complex[-1,-1]->"T"};
		]
	];
	Graph[edges[[All,1]],
		EdgeWeight->If[OptionValue[NegeteEdgeWeight],-1edges[[All,2]],edges[[All,2]]],
		EdgeLabels->edges[[All,3]],
		(*VertexLabels->vlabels,*)
		VertexWeight->vertices,
		VertexShapeFunction -> VertexShapeF,
		VertexSize->Medium
	]
];
Options[FindSchedule]={HighlightShortestPath->False};
FindSchedule[ps_,\[Epsilon]_]:=Block[{zones,g,sg,v},
	zones=MakeZone[ps];
	Print[#//MatrixForm]&/@zones;
	g=HopCostGraph[zones,\[Epsilon], IncludeInfinity->False,WeightCombined->True,NegeteEdgeWeight->True];
	v=FindShortestPath[g,Complex[0,0],Complex[-1,-1]];
	sg=HighlightGraph[g,Style[PathGraph[v,DirectedEdges->True], {Red, Thick}]];
	Print[sg];
	Block[{p={Re[#], Im[#]}},
		Piecewise[{
			{"S", First[p]==0},
			{"T", First[p]==-1}
		}, p]
	]&/@ v 
];

Options[Resource]={id->0, type->Null, availability->Null, context->Null, contextuals->{}, capabilities->{}, expectations->{} UseHilbertCurve->False};
Resource[OptionsPattern[]]:=Block[{},
	<|"id"->OptionValue[id], "type"->OptionValue[type], "availability"->OptionValue[availability], "context"->OptionValue[context], "contextuals"->OptionValue[contextuals], "capabilities"->ScalarizeF[OptionValue[capabilities]], "capabilitiesr"->ScalarizeRestrictedF[OptionValue[capabilities]], "expectations"->OptionValue[expectations]|>
];

Options[Request]={id->0, type->Null, slot->Null, context->Null, contextuals->{}, capabilities->{}, UseHilbertCurve->False};
Request[OptionsPattern[]]:=Block[{},
	<|"id"->OptionValue[id], "type"->OptionValue[type], "slot"->OptionValue[slot], "context"->OptionValue[context], "contextuals"->OptionValue[contextuals], "capabilities"->ScalarizeF[OptionValue[capabilities]], "capabilitiesr"->ScalarizeRestrictedF[OptionValue[capabilities]]|>
];

ResourceJSON[R_]:=Block[{Rp},
	Rp=Block[{rp=#},
		rp["capabilities"] = ToString/@#["capabilities"];
		rp
	]&/@R;
	ExportString[Rp,"JSON","Compact"->True]
];

RequestJSON[R_]:=Block[{Rp},
	Rp=Block[{rp=#},
		rp["capabilities"] = ToString/@#["capabilities"];
		rp
	]&/@R;
	ExportString[Rp,"JSON","Compact"->True]
];

(**
 * Takes a 2xN matrix and returns a 2x1 matrix
 *)
ScalarizeF[CM_] := Block[{l=Length[CM],M},
	M=If[Length[#]==3,Append[#,0],#]&/@CM;
	{\!\(
\*UnderoverscriptBox[\(\[Product]\), \(i = 1\), \(l\)]
\*SuperscriptBox[\(M[\([i, 1]\)]\), \(M[\([i, 2]\)]\)]\), \!\(
\*UnderoverscriptBox[\(\[Product]\), \(i = 1\), \(l\)]
\*SuperscriptBox[\(M[\([i, 1]\)]\), \(M[\([i, 3]\)]\)]\)}
];
ScalarizeRestrictedF[CM_] := Block[{l=Length[CM],M},
	M=If[Length[#]==3,Append[#,0],#]&/@CM;
	Print[M];
	{\!\(
\*UnderoverscriptBox[\(\[Product]\), \(i = 1\), \(l\)]
\*SuperscriptBox[\(M[\([i, 1]\)]\), \(M[\([i, 4]\)]\ M[\([i, 2]\)]\)]\), \!\(
\*UnderoverscriptBox[\(\[Product]\), \(i = 1\), \(l\)]
\*SuperscriptBox[\(M[\([i, 1]\)]\), \(M[\([i, 4]\)]\ M[\([i, 3]\)]\)]\)}
];
(* returns True of False depending on whether resource r is feasible for request q or not *)
FeasibleF[r_, q_]:=Block[{},
	r\[LongRightArrow]type==q\[LongRightArrow]type && 
	r\[LongRightArrow]context==q\[LongRightArrow]context && 
	OverlappingF[r\[LongRightArrow]availability, q\[LongRightArrow]slot] &&
	CapFeasibleF[r\[LongRightArrow]capabilities, r\[LongRightArrow]capabilitiesr, q\[LongRightArrow]capabilities, q\[LongRightArrow]capabilitiesr]
];
(* returns True of False depending on resource capabilities capr and request capabilities capq, If retuns false capr is not feasible for capq*)
CapFeasibleF[capr_, caprr_, capq_, capqr_]:=Block[{},
	Mod[capq[[1]](caprr[[1]]/capqr[[1]]), capr[[1]](capqr[[1]]/caprr[[1]])] == 0 && 
	Mod[capr[[2]](capqr[[2]]/caprr[[2]]), capq[[2]](caprr[[2]]/capqr[[2]])] == 0
];
(* If capr is feasible for capq How much capabilities of capr will be under utilized if capr is used for capq *)
WastageF[capr_, caprr_, capq_, capqr_]:= Block[{caph, caphr},
	caph  = (capq[[1]]*capr[[2]])/(capq[[2]]*capr[[1]]);
	caphr = (caprr[[1]]*capqr[[2]])/(caprr[[2]]*capqr[[1]]);
	caph*(caphr^2)
];
(* If capr is infeasible for capq returns infinite amount of wastage, otherwise returns WastageF *)
Wastage[capr_, caprr_, capq_, capqr_]:=\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{
RowBox[{"WastageF", "[", 
RowBox[{"capr", ",", " ", "caprr", ",", " ", "capq", ",", " ", "capqr"}], "]"}], 
RowBox[{"CapFeasibleF", "[", 
RowBox[{"capr", ",", " ", "caprr", ",", " ", "capq", ",", " ", "capqr"}], "]"}]},
{"\[Infinity]", "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\);
(* returns Boolean feasibility matrix *)
FeasibilityMatrix[R_, Q_]:= Table[FeasibleF[R[[i]], Q[[j]]], {i, Length[R]}, {j, Length[Q]}];
(* returns numeric wastage matric (infeasible cells have infinite wastage) *)
WastageMatrix[R_, Q_]:= Table[Wastage[R[[i]]\[LongRightArrow]capabilities, R[[i]]\[LongRightArrow]capabilitiesr, Q[[j]]\[LongRightArrow]capabilities, Q[[j]]\[LongRightArrow]capabilitiesr], {i, Length[R]}, {j, Length[Q]}];
(* Appropriateness matrix, inappropriate cells are 0/\[Infinity] = 0 *)
AppropriatenessMatrix[R_, Q_]:= (FeasibilityMatrix[R, Q]//Boole)/WastageMatrix[R, Q];
(* Favourability cube of resource,partition,request infeasible/inappropriate cells are extremely unfavourable = 0 *)
FavourabilityCube[R_,Q_]:=Block[{A=AppropriatenessMatrix[R,Q]},Table[A[[i, k]]*(R[[i]]\[LongRightArrow]expectations)[[j, 4]],{i, Length[R]}, {j, Length[(R[[i]]\[LongRightArrow]expectations)[[All, 4]]]}, {k, Length[Q]}]];
(**
 * Given a resource's availability and a request slot returns on which instance do they overlap
 * overlapping True or False (if False the resource can not be allocated for the request)
 * a,b     index of begin and end instance of the resource availability
 * u,ue   time segment of instance a that overlaps with the request
 * v,ve    time segment of instance b that overlaps with the request
 * slots   number of total instances of the resource that partially or fully overlaps with the request
 *)
Options[OverlapsF]={Details->True,Visualization->False};
OverlapsF[availability_,request_,OptionsPattern[]]:=With[{s=availability["start"],k=availability["recurrence"],d=availability["duration"],ts=request[[1]],te=request[[2]]},
	If[te<s,Return[If[OptionValue[Details],<|"overlapping"->False,"a"->-1,"b"->-1,"u"->-1,"ue"->-1,"v"->-1,"ve"->-1,"slots"->0,"coverage"->0|>,False]]];
	Block[{m,n,a,b,u,v,ue,ve,coverage,res},
		m=\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{"0", 
RowBox[{"ts", "<", "s"}]},
{
RowBox[{"Floor", "[", 
FractionBox[
RowBox[{"ts", "-", "s"}], "k"], "]"}], "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\);n=\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{"0", 
RowBox[{"te", "<", "s"}]},
{
RowBox[{"Floor", "[", 
FractionBox[
RowBox[{"te", "-", "s"}], "k"], "]"}], "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\);
		a=\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{"m", 
RowBox[{
RowBox[{"ts", "-", "s"}], "<", 
RowBox[{
RowBox[{"m", " ", "k"}], " ", "+", "d"}]}]},
{
RowBox[{"m", "+", "1"}], 
RowBox[{
RowBox[{
RowBox[{"(", 
RowBox[{"m", "+", "1"}], ")"}], "k"}], "<", 
RowBox[{"te", "-", "s"}]}]},
{
RowBox[{"-", "1"}], "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\);b=\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{
RowBox[{"-", "1"}], 
RowBox[{"a", "==", 
RowBox[{"-", "1"}]}]},
{"n", "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\);
		If[a==-1,
			{u,ue,v,ve}={-1,-1,-1,-1};
			coverage=0,
			
			u=Max[s+a k, ts]; ue=Min[s+a k+d,te];
			v=Max[s+b k, ts]; ve=Min[s+b k+d,te];
			coverage=0;
			coverage+=If[a==b,(ue-u),(ue-u)+(ve-v)];
			coverage+=If[b>a+1,((b-a)-1)*d,0];
		];
		res=<|"overlapping"->(a!=-1),"a"->a,"b"->b,"u"->u,"ue"->ue,"v"->v,"ve"->ve,"slots"->If[a== -1,0,(b-a)+1],"coverage"->N[coverage/(te-ts)]|>;
		If[OptionValue[Visualization],Print[OverlapVisualization[s,k,d,ts,te,res]]];
		If[OptionValue[Details],res,(a!=-1)]
]];
OverlappingF[availability_,request_] := OverlapsF[availability,request,Details->False];
Options[CoveringF]={FullCoverage->False};
CoveringF[availability_,request_,OptionsPattern[]]:=With[{c=OverlapsF[availability,request]["coverage"]},If[OptionValue[FullCoverage],If[c==1.0,1.0,0.0],c]];
OverlapVisualization[s_,k_,d_,ts_,te_,details_]:=With[{a=details["a"],b=details["b"],
	u=details["u"],ue=details["ue"],v=details["v"],ve=details["ve"]},
	Block[{lines, joints,points,gres,greq,rlabels,qlabels,lossline},
		lines={Thick,Line[#]}&/@Table[{{s+i k,0},{s+i k+d,0}},{i,a-1,Max[a,b]+1}];
		joints={Thin,Line[#]}&/@Table[{{s+i k+d,0},{s+(i+1)k,0}},{i,a-1,Max[a,b]+1}];
		points=Flatten[Join[lines,joints[[;;Length[joints]-1]] ],1];
		gres=Graphics[points];
		rlabels=Flatten[Table[{Text[s+i k,{s+i k,1}],Text[s+i k+d,{s+i k+d,1}],Text[i,{s+i k,-0.5}]},{i,a-1,Max[a,b]+1}],1]//Graphics;

		greq={
			{Thick,If[te>u,Blue,Red],Arrow[{{u,4},{ue,4}}]}, 
			If[te>u,{Thick,Blue,Arrow[{{v,4},{ve,4}}]},Nothing]
		};
		If[te>u,
			AppendTo[greq,{Thick,Blue, Line[{{s+# k,4},{s+# k+d,4}}]}]& /@Range[a+1,b-1]
		];
		qlabels={
			Text[ts,{ts,4.5}],Text[u,{u,3.5}],Text[ue,{ue,3.5}],
			Text[te,{te,4.5}],Text[v,{v,3.5}],Text[ve,{ve,3.5}]
		};
		lossline={{Dashed,Line[{{ts,4},{u,4}}]},{Dashed,Line[{{ve,4},{te,4}}]}};
		If[te>u,
			AppendTo[lossline,{Dashed,Line[{{s+# k+d,4},{s+(# +1)k,4}}]}]& /@Range[a,b-1]
		];
		Show[gres,Sequence[Graphics/@greq],rlabels,qlabels//Graphics,Sequence[Graphics/@lossline]]
	]
];
(**
 * returns index of the first performance partition of resource that can accomodate the request
 * if it is possible to acomodate the request in resource's availability slot but start time of
 * the request is before start time of resource's availability slot, we can still allocate the
 * resource for the request, however given resource cannot be the first one, so this resource
 * can not have any first slot with respect to this request. Otherwise request start time (ts)
 * must obey s+nk < ts \[LessEqual] s+nk+\[Delta] . Start and end times in performance partitions as phased with s
 * and n can be positive integer. So if n > 1 direct comparison between ts and start time of the
 * performance partitions will not work. We assume that resource is feasible for the request with
 * respect to availabality slot and request slot. We first normalize qbegin (request start time)
 * as if n is 0 so that the performance partition start and end times are properly phased
 *)
FindFirstSlot[resource_, request_]:=Block[{i, region, shift, rslots=resource\[LongRightArrow]expectations, qslot=request\[LongRightArrow]slot},
	region=OverlapsF[resource\[LongRightArrow]availability, qslot];
	If[region\[LongRightArrow]u == qslot[[1]],
		shift=With[{u=region\[LongRightArrow]u,s=resource\[LongRightArrow]availability\[LongRightArrow]start,k=resource\[LongRightArrow]availability\[LongRightArrow]recurrence,a=region\[LongRightArrow]a}, u-(s+a*k)];
		For[i=1,i<=Length[rslots],i++, 
			With[{rbegin=rslots[[i,1]], rend=rslots[[i,2]], qbegin=shift},
				If[rend > qbegin, Break[], Nothing]
			]
		];i,-1
	]
];
FindLastSlot[resource_, request_]:=Block[{i, region, shift, rslots=resource\[LongRightArrow]expectations, qslot=request\[LongRightArrow]slot, index},
	region=OverlapsF[resource\[LongRightArrow]availability, qslot];
	index=If[region\[LongRightArrow]v == qslot[[2]],
		shift=With[{v=region\[LongRightArrow]v,s=resource\[LongRightArrow]availability\[LongRightArrow]start,k=resource\[LongRightArrow]availability\[LongRightArrow]recurrence,b=region\[LongRightArrow]b}, v-(s+b*k)];
		For[i=1,i<=Length[rslots],i++, 
			With[{rbegin=rslots[[i,1]], rend=rslots[[i,2]], qbegin=shift},
				If[rend > qbegin, Break[], Nothing]
			]
		];i,-1
	];
	index+(Length[resource\[LongRightArrow]expectations]*(region\[LongRightArrow]slots-2))+(Length[resource\[LongRightArrow]expectations]-FindFirstSlot[resource, request])
];
Pieces[expr_]:=Block[{super,subs},
	super=Cases[expr,(Power)[_,y_]:>y,{0,Infinity}];
	subs=Cases[expr,(Subscript)[_,y__]:>y,{0,Infinity}];
	{super,subs}
];
(**
 * Pair of start and end times of resource r's instance m partition p
 *)
TimeSpan[r_,m_,p_]:=With[{a=(r\[LongRightArrow]availability)},With[{s=a\[LongRightArrow]start,k=a\[LongRightArrow]recurrence,E=r\[LongRightArrow]expectations},{s+m k+E[[p,1]],s+m k+E[[p,2]]}]];
(**
 * Competetion of resource r's instance m partition p w.r.t. resources R
 *)
Competetion[R_,r_,m_,p_]:=Total[CoveringF[#\[LongRightArrow]availability,TimeSpan[r,m,p],FullCoverage->True]&/@R];
RequestSubset[r_,Q_]:=Block[{Fq=FeasibleF[r,#]&/@Q},
	Table[If[Fq[[k]],{k,Q[[k]]},Nothing],{k,Length[Q]}]
];
ResourceSubset[R_,q_]:=Block[{Fr=FeasibleF[#,q]&/@R},
	Table[If[Fr[[i]],{i,R[[i]]},Nothing],{i,Length[R]}]
];
ExplodeResource[r_,t_Integer]:=Table[<|"b"->#[[1]],"e"->#[[2]]|>&@TimeSpan[r,t,p],{p,Length[r\[LongRightArrow]expectations]}];
ExplodeResource[r_,t_List]:=ExplodeResource[r,#]&/@t;
ExplodeResourcePoints[r_,t_List]:=DeleteDuplicates[Values@(ExplodeResource[r,t]//Flatten)//Flatten];
SliceResource[R_,T_,r_,t_]:=Block[{slices={},bounds,E,Ep,chunks,reshaped},
	slices=Sort[Table[ExplodeResourcePoints[R[[i]],Range[T[[i,1]],T[[i,2]]]],
		{i,Length[R]}
	]//Flatten];
	E=ExplodeResourcePoints[r,{t}];
	bounds=MinMax[E];
	slices=slices/.s_/;(s<bounds[[1]]||s>bounds[[2]])->Nothing;
	Ep=Complement[Union[E,slices],E]-TimeSpan[r,t,1][[1]];
	chunks=Table[Block[{hay=r\[LongRightArrow]expectations[[h]],niddles={}},
		With[{n=#},
			AppendTo[niddles,If[hay[[1]]<=n<=hay[[2]],n,Nothing]]
		]&/@Ep;
		Append[hay,niddles]
	],{h,Length[r\[LongRightArrow]expectations]}];
	reshaped=Table[Block[{C={}},
		AppendTo[C,
			Append[Fold[Block[{splitted=SplitF[#1,#2]},
				AppendTo[C,Append[splitted[[1]],p]];
			splitted[[2]]]&,r\[LongRightArrow]expectations[[p]],chunks[[p,6]]],p]];
	C],{p,Length[r\[LongRightArrow]expectations]}];
	reshaped=Flatten[reshaped,1];
	reshaped=With[{TS=TimeSpan[r,t,1]},
		<|"partition"->#[[6]],"length"->#[[3]],"gain"->#[[4]],"begin"->TS[[1]]+#[[1]],"end"->TS[[1]]+#[[2]]|>
	]&/@reshaped;
	reshaped
];
CreateResourceSlices[R_,Q_]:=Block[{ F=FeasibilityMatrix[R,Q],P = FavourabilityCube[R, Q]},
	Table[Block[{r=R[[i]],q=Q[[k]],regions,count=1},
		regions=With[{rgn=OverlapsF[#\[LongRightArrow]availability,Q[[k]]\[LongRightArrow]slot]},{rgn\[LongRightArrow]a,rgn\[LongRightArrow]b}]&/@R;
		Table[With[{sliced=If[m!=-1,SliceResource[R,regions,r,m],{}]},
			Append[#,<|"request"->k,"address"->Complex[i,count++],"bucket"->{i,m,#\[LongRightArrow]partition,k}|>]&/@sliced
		],{m,regions[[i,1]],regions[[i,2]]}]
	],{i, Length[R]},{k, Length[Q]}]
];
Options[OutSlices]={GuidedLocation->True};
(* returns a set of possible out-edges that originate from Subscript[R, i] n th instances p th partition w.r.t. k th request *)
OutSlices[R_,Q_,i_,n_,p_,k_,x_Symbol, y_Symbol,\[Epsilon]_,OptionsPattern[]]:=With[{slices=R[[i,k]]},
	Block[{neighbours={}},
		AppendTo[neighbours,If[p<Length[slices[[n]]]&&slices[[n,p+1]]\[LongRightArrow]end> Q[[k]]\[LongRightArrow]slot[[1]]&&slices[[n,p+1]]\[LongRightArrow]begin< Q[[k]]\[LongRightArrow]slot[[2]],Complex[i,p+1],Nothing]];
		neighbours=Table[With[{target=R[[t,k]]},
			Table[With[{tinstance=target[[ti]]},
				Table[With[{tipartition=tinstance[[tpi]]},
					(* if the target is reachable from the source build an edge connecting the neighbour otherwise put nothing *)
					If[(slices[[n,p]]\[LongRightArrow]end> Q[[k]]\[LongRightArrow]slot[[1]]&&slices[[n,p]]\[LongRightArrow]begin< Q[[k]]\[LongRightArrow]slot[[2]])&&(tipartition\[LongRightArrow]end> Q[[k]]\[LongRightArrow]slot[[1]]&&tipartition\[LongRightArrow]begin< Q[[k]]\[LongRightArrow]slot[[2]])&&tipartition\[LongRightArrow]begin==slices[[n]][[p]]\[LongRightArrow]end,
						Block[{res=<|
							"address"->tipartition["address"],
							"symbol"->Subscript[x,t,Im[tipartition["address"]],k],
							"edge"->Subscript[y,Re[slices[[n,p]]\[LongRightArrow]address],Im[slices[[n,p]]\[LongRightArrow]address],t,Im[tipartition["address"]],k],
							"gain"->tipartition["gain"]*tipartition["length"], (* gain of the target vertex *)
							"loss"->If[i!= t,HopLossF[tipartition["length"],\[Epsilon]],0] (* hop cost of the edge *)
							|>},
							res=If[OptionValue[GuidedLocation], (* add some extra information *)
								Append[res,<|"location"-> <|"resource"->t,"instance"->ti,"slice"->tpi|>|>],res
							];
							res
					],Nothing]
			],{tpi,Length[tinstance]}]/.{}->Nothing
		],{ti,Length[target]}]/.{}->Nothing],{t,Length[R]}]//Flatten;
	neighbours
]];
Options[InSlices]={GuidedLocation->True};
InSlices[R_,Q_,i_,n_,p_,k_,x_Symbol,y_Symbol,\[Epsilon]_,OptionsPattern[]]:=With[{slices=R[[i,k]]},Block[{neighbours={}},
	AppendTo[neighbours,If[p>1&&slices[[n,p-1]]\[LongRightArrow]end> Q[[k]]\[LongRightArrow]slot[[1]]&&slices[[n,p-1]]\[LongRightArrow]begin< Q[[k]]\[LongRightArrow]slot[[2]],Complex[i,p-1],Nothing]];
	neighbours=Table[With[{source=R[[s,k]]},
		Table[With[{sinstance=source[[si]]},
			Table[With[{sipartition=sinstance[[spi]]},
				If[(slices[[n,p]]\[LongRightArrow]end> Q[[k]]\[LongRightArrow]slot[[1]]&&slices[[n,p]]\[LongRightArrow]begin< Q[[k]]\[LongRightArrow]slot[[2]])&&(sipartition\[LongRightArrow]end> Q[[k]]\[LongRightArrow]slot[[1]]&&sipartition\[LongRightArrow]begin< Q[[k]]\[LongRightArrow]slot[[2]])&&sipartition\[LongRightArrow]end==slices[[n]][[p]]\[LongRightArrow]begin,
					Block[{res=<|
						"address"->sipartition["address"],
						"symbol"->Subscript[x,s,Im[sipartition["address"]],k],
						"edge"->Subscript[y,s,Im[sipartition["address"]],Re[slices[[n,p]]\[LongRightArrow]address],Im[slices[[n,p]]\[LongRightArrow]address],k],
						"gain"->slices[[n]][[p]]\[LongRightArrow]gain*slices[[n]][[p]]\[LongRightArrow]length,
						"loss"->If[i!= s,HopLossF[slices[[n]][[p]]\[LongRightArrow]length,\[Epsilon]],0]
						|>},
						res=If[OptionValue[GuidedLocation],
							Append[res,<|"location"-> <|"resource"->s,"instance"->si,"slice"->spi|>|>],res
						];
						res
					],Nothing]
			],{spi,Length[sinstance]}]/.{}->Nothing
	],{si,Length[source]}]/.{}->Nothing],{s,Length[R]}]//Flatten;
	neighbours
]];
HopLossF[len_, \[Epsilon]_] := \[Epsilon]*E^(-0.02 (len - 5));
Options[SliceVertices] = {GuidedLocation -> True};
SliceVertices[R_,Q_,x_Symbol,y_Symbol,\[Epsilon]_,OptionsPattern[]]:=Table[With[{instances=R[[i,k]],q=Q[[k]]},
	Table[Block[{self,sources,targets},
		self=instances[[n,p]];
		targets=OutSlices[R,Q,i,n,p,k,x,y,\[Epsilon],GuidedLocation->OptionValue[GuidedLocation]]; 
		sources=InSlices[R,Q,i,n,p,k,x,y,\[Epsilon],GuidedLocation->OptionValue[GuidedLocation]];
		Append[instances[[n]][[p]],
			<|"symbol"->Subscript[x,i,Im[instances[[n]][[p]]\[LongRightArrow]address],k],"sources"->sources,"targets"->targets|>
		]
	],{n,Length[instances]},{p,Length[instances[[n]]]}]
],{i, Length[R]},{k,Length[Q]}];
BoundingSliceVertices[R_,Q_,x_Symbol,y_Symbol,OptionsPattern[]]:=Table[With[{instances=R[[i,k]],q=Q[[k]]},
	Table[With[{self=instances[[n,p]]},
		If[self\[LongRightArrow]begin<=q\[LongRightArrow]slot[[1]]<self\[LongRightArrow]end,
			<|"type"->"initial","address"->Complex[0,0],"source"->Subscript[x,0,0,k],"destination"-><|"symbol"->Subscript[x,i,Im[self\[LongRightArrow]address],k],"location"-> <|"resource"->i,"instance"->n,"slice"->p|>|>,"edge"->Subscript[y,0,0,i,Im[self\[LongRightArrow]address],k]|>,
			If[self\[LongRightArrow]begin<q\[LongRightArrow]slot[[2]]<=self\[LongRightArrow]end,
				<|"type"->"terminal","address"->Complex[-1,-1],"source"-><|"symbol"->Subscript[x,i,Im[self\[LongRightArrow]address],k],"location"-> <|"resource"->i,"instance"->n,"slice"->p|>|>,"destination"->Subscript[x,-1,-1,k],"edge"->Subscript[y,i,Im[self\[LongRightArrow]address],-1,-1,k]|>
			,Nothing]
		]
	],{n,Length[instances]},{p,Length[instances[[n]]]}]//Flatten
],{i, Length[R]},{k,Length[Q]}];
BackupSlices[sym_, nQ_, tolerence_, x_Symbol] := Block[{p = Pieces[sym][[2]]},
   Table[Subscript[x, p[[1]], p[[2]], p[[3]] + k (nQ/(tolerence + 1))] -> k, {k, 0, tolerence}]
];
Options[LayerX]={ShowLegends->False,HighlightVertices->{},HighlightEdges->{},HighlightBlocks->{}, FaultTolerence -> 0, ShowEdges->True};
LayerX[R_, Vq_, q_, nQ_, x_Symbol, OptionsPattern[]]:=Block[{gap=40,margin=5,C={RGBColor[0.24720000000000014`, 0.24, 0.6],RGBColor[0.6, 0.24, 0.4428931686004542],RGBColor[0.6, 0.5470136627990908, 0.24],RGBColor[0.24, 0.6, 0.33692049419863584`],RGBColor[0.24, 0.3531726744018182, 0.6],RGBColor[0.6, 0.24, 0.5632658430022722],RGBColor[0.6, 0.42664098839727194`, 0.24],RGBColor[0.2634521802031821, 0.6, 0.24],RGBColor[0.24, 0.47354534880363613`, 0.6],RGBColor[0.5163614825959097, 0.24, 0.6],RGBColor[0.6, 0.3062683139954558, 0.24],RGBColor[0.3838248546049982, 0.6, 0.24],RGBColor[0.24, 0.5939180232054561, 0.6],RGBColor[0.39598880819409377`, 0.24, 0.6],RGBColor[0.6, 0.24, 0.2941043604063603]},bounds,walls,blocks={}},
	bounds=MinMax[(Vq//Flatten)[[All,"gain"]]];
	Table[With[{ymin=(i-1)*gap,Vr=Vq[[i]]//Flatten},
		With[{sv=#,tslices=#\[LongRightArrow]targets},Block[{height,vslice,arrows,hvslices},
			height=Rescale[sv\[LongRightArrow]gain,bounds,{1,gap-margin}];
			vslice=Graphics[{
				Opacity[0.6],
				C[[sv\[LongRightArrow]partition]],
				EdgeForm[{Thin,C[[sv\[LongRightArrow]partition]]}],
				{Rectangle[{sv\[LongRightArrow]begin,ymin},{sv\[LongRightArrow]end,ymin+height}],If[OptionValue[ShowLegends],Text[Style[Im[sv\[LongRightArrow]address],Bold,White],{sv\[LongRightArrow]begin+sv\[LongRightArrow]length/2,ymin+4}],Nothing]}
			}];
			(*
			hvslices = Table[With[{hv = OptionValue[HighlightVertices][[i]]}, (* loop into each highlight set assuming we have been given 2 sets of highlighted vertices *)
				If[MemberQ[hv, sv\[LongRightArrow]symbol],					  (* if sv the current source vertex is in the current highlight set *)
					Graphics[{Opacity[0.9], {Blue, Red}[[i]], EdgeForm[{Thin, Transparent}], {Rectangle[{sv\[LongRightArrow]begin, ymin + height + i}, {sv\[LongRightArrow]end, ymin + height + i + 0.9}]}, White, Table[Disk[{sv\[LongRightArrow]begin + (j - 1) + 0.5, ymin + height + i + 0.5}, 0.5], {j, i}]}]
					, With[{comps=Keys@BackupSlices[sv\[LongRightArrow]symbol, nQ, OptionValue[FaultTolerence], x]},
						If[i > 1 && OptionValue[FaultTolerence] > 0 && ContainsAny[comps, OptionValue[HighlightVertices][[2]]],
							With[{idx = First@First@Position[comps, First@Intersection[comps, OptionValue[HighlightVertices][[2]]]]},
								Graphics[{Opacity[0.9], C[[sv\[LongRightArrow]partition]], Disk[{sv\[LongRightArrow]begin + 2.0, ymin + 2.2}, 2.3], White, Text[idx - 1, {sv\[LongRightArrow]begin + 2.0, ymin + 2.2}]}]
							]
							, Nothing
						]
					]
				]], {i, 1, Length[OptionValue[HighlightVertices]]}];
			*)
			hvslices = Table[With[{hv = OptionValue[HighlightVertices][[i]]}, (* loop into each highlight set assuming we have been given 2 sets of highlighted vertices *)
				If[MemberQ[hv, sv\[LongRightArrow]symbol],					  (* if sv the current source vertex is in the current highlight set *)
					Graphics[{Opacity[0.5], {Blue, Red}[[i]], EdgeForm[{Thin, Transparent}], {Rectangle[{sv\[LongRightArrow]begin, ymin + 4.0*i}, {sv\[LongRightArrow]end, ymin + 4.0*i + 3.5}]}}]
					, With[{comps=Keys@BackupSlices[sv\[LongRightArrow]symbol, nQ, OptionValue[FaultTolerence], x]},
						If[i > 1 && OptionValue[FaultTolerence] > 0 && ContainsAny[comps, OptionValue[HighlightVertices][[2]]],
							With[{idx = First@First@Position[comps, First@Intersection[comps, OptionValue[HighlightVertices][[2]]]]},
								Graphics[{Opacity[0.9], C[[sv\[LongRightArrow]partition]], Disk[{sv\[LongRightArrow]begin + 2.0, ymin + 2.2}, 2.3], White, Text[idx - 1, {sv\[LongRightArrow]begin + 2.0, ymin + 2.2}]}]
							]
							, Nothing
						]
					]
				]], {i, 1, Length[OptionValue[HighlightVertices]]}];
			EdgeArrow[ti_, t_]:=Block[{},
				If[ti>i,
					Arrow[{{sv\[LongRightArrow]end,ymin+height},{t\[LongRightArrow]begin,(ti-1)*gap}}], 															(* Migraton Hop Edge going downward*)
					If[ti<i,
						Arrow[{{sv\[LongRightArrow]end,ymin},{t\[LongRightArrow]begin,(ti-1)*gap+Rescale[t\[LongRightArrow]gain,bounds,{1,gap-margin}]}}], 			(* Migration hop Edge going upward *)
						Arrow[{{sv\[LongRightArrow]begin+sv\[LongRightArrow]length/2,ymin-2},{t\[LongRightArrow]begin+t\[LongRightArrow]length/2,(ti-1)*gap-2}}] 	(* ti\[Equal]i Hop without migration (same resource) *)
					]
				]
			];
			arrows=Flatten@If[OptionValue[ShowEdges], 
				Block[{ti=(#\[LongRightArrow]location)\[LongRightArrow]resource,t=Vq[[(#\[LongRightArrow]location)\[LongRightArrow]resource]][[(#\[LongRightArrow]location)\[LongRightArrow]instance]][[(#\[LongRightArrow]location)\[LongRightArrow]slice]],arrow,graphic},
					arrow=EdgeArrow[ti, t];
					graphic=If[!MemberQ[Flatten@OptionValue[HighlightEdges],#\[LongRightArrow]edge],                          	(* only possible edge but not highlighted edge *)
						Graphics[{Directive[Thin, Opacity[0.05], C[[sv\[LongRightArrow]partition]],Arrowheads[0.008]], arrow}], (* edge not highlighted *)
						If[Length@OptionValue[HighlightEdges]!=2,                                                				(* pair of edge sets not provided *)
							Graphics[{Directive[Thick, Red, Arrowheads[0.008]], arrow}],                          				(* Single shade of highlight *)
							Block[{he=OptionValue[HighlightEdges], gs={}},
								AppendTo[gs, If[MemberQ[he[[1]], #\[LongRightArrow]edge], Graphics[{Directive[Thick, Blue, Opacity[1.0], Arrowheads[0.008], Dashing[0.001]], arrow}],Nothing]];
								AppendTo[gs, If[MemberQ[he[[2]], #\[LongRightArrow]edge], Graphics[{Directive[Thick, Red,  Opacity[1.0], Arrowheads[0.008], Dashing[0.01]], arrow}],Nothing]];
								gs
							]
						]
					];
					graphic
				]&/@tslices, 
				{}
			];
			{vslice,arrows,hvslices}
		]]&/@Vr],{i,Length[Vq]}
	]//Flatten
];
Options[CubeX]={ShowLegends->False,HighlightVertices->{},HighlightEdges->{},HighlightBlocks->{}};
CubeX[R_,V_,Q_,OptionsPattern[]]:=Block[{gap=40,thick=10,margin=5,C={RGBColor[0.24720000000000014`, 0.24, 0.6],RGBColor[0.6, 0.24, 0.4428931686004542],RGBColor[0.6, 0.5470136627990908, 0.24],RGBColor[0.24, 0.6, 0.33692049419863584`],RGBColor[0.24, 0.3531726744018182, 0.6],RGBColor[0.6, 0.24, 0.5632658430022722],RGBColor[0.6, 0.42664098839727194`, 0.24],RGBColor[0.2634521802031821, 0.6, 0.24],RGBColor[0.24, 0.47354534880363613`, 0.6],RGBColor[0.5163614825959097, 0.24, 0.6],RGBColor[0.6, 0.3062683139954558, 0.24],RGBColor[0.3838248546049982, 0.6, 0.24],RGBColor[0.24, 0.5939180232054561, 0.6],RGBColor[0.39598880819409377`, 0.24, 0.6],RGBColor[0.6, 0.24, 0.2941043604063603]},bounds,walls,blocks={},mints},
	bounds=MinMax[(V//Flatten)[[All,"gain"]]];
	mints=Min[Q[[All,"slot",1]]];
	walls=Table[Graphics3D[{Opacity[0.2],Black,EdgeForm[{Thin,Black}],{
		Cuboid[{Q[[k]]\[LongRightArrow]slot[[1]]-mints,-12,(k-1)*gap},{Q[[k]]\[LongRightArrow]slot[[2]]-mints,-10,(k-1)*gap+thick}],
		Text[Style[k,Bold,Black],{Q[[k]]\[LongRightArrow]slot[[1]]-4-mints,-12,(k-1)*gap}]
	}},Boxed->False],{k,Length[Q]}];
	walls=Append[walls,Table[Graphics3D[{Opacity[0.2],Brown,EdgeForm[{Thin,Brown}],{
		Cuboid[{0,(i-1)*gap,0},{2,i*gap-margin,(Length[Q]-1)*gap+thick}],
		Text[Style[i,Bold,Black],{0,(i-1)*gap+gap/2,(Length[Q]-1)*gap+thick}]
	}},Boxed->False],{i,Length[V]}]];
	blocks=With[{segments=OptionValue[HighlightBlocks]},Block[{ts=TimeSpan[R[[#[[1]]]],#[[2]],#[[3]]]},With[{colour=RandomColor[]},
		Graphics3D[{Opacity[0.2],colour(*C[[Mod[Length[C],#[[2]]+1]+1]]*),EdgeForm[{Thin,colour}],{
		Cuboid[{ts[[1]]-mints,(*(#[[1]]-1)*gap*)#[[1]]*gap-margin-2.5,0},{ts[[2]]-mints,#[[1]]*gap-margin,(Length[Q]-1)*gap+thick}]
	}},Boxed->False]]]&/@segments];
	Join[Table[With[{ymin=(i-1)*gap,Vr=V[[i,k]]//Flatten},With[{sv=#,tslices=#\[LongRightArrow]targets},Block[{height,vslice,arrows,hvslices},
		height=Rescale[sv\[LongRightArrow]gain,bounds,{1,gap-margin}];
		vslice=Graphics3D[{Opacity[0.6],C[[sv\[LongRightArrow]partition]],EdgeForm[{Thin,C[[sv\[LongRightArrow]partition]]}],{
			Cuboid[{sv\[LongRightArrow]begin-mints,ymin,(k-1)*gap},{sv\[LongRightArrow]end-mints,ymin+height,(k-1)*gap+thick}],
			If[OptionValue[ShowLegends],Text[Style[Im[sv\[LongRightArrow]address],Bold,White],{sv\[LongRightArrow]begin+sv\[LongRightArrow]length/2-mints,ymin+4,(k-1)*gap+5}],Nothing]
		}},Boxed->False];
		hvslices=If[MemberQ[OptionValue[HighlightVertices],sv\[LongRightArrow]symbol],
			Graphics3D[{Opacity[0.6],Red,EdgeForm[{Thin,C[[sv\[LongRightArrow]partition]]}],{
				Cuboid[{sv\[LongRightArrow]begin-mints,ymin+height+2,(k-1)*gap},{sv\[LongRightArrow]end-mints,ymin+height+4,(k-1)*gap+thick}]
			}},Boxed->False]
		,Nothing];
		arrows=With[{ti=(#\[LongRightArrow]location)\[LongRightArrow]resource,t=V[[(#\[LongRightArrow]location)\[LongRightArrow]resource,k]][[(#\[LongRightArrow]location)\[LongRightArrow]instance]][[(#\[LongRightArrow]location)\[LongRightArrow]slice]]},
			Graphics3D[{If[MemberQ[OptionValue[HighlightEdges],#\[LongRightArrow]edge],
				Directive[Thick,Red],Directive[Thin, Opacity[0.001],C[[sv\[LongRightArrow]partition]]]],Arrowheads[0.014],
				If[ti>i,Arrow[{{sv\[LongRightArrow]end-mints,ymin+height,(k-1)*gap},{t\[LongRightArrow]begin-mints,(ti-1)*gap,(k-1)*gap}}],
					If[ti<i,Arrow[{{sv\[LongRightArrow]end-mints,ymin,(k-1)*gap+thick},{t\[LongRightArrow]begin-mints,(ti-1)*gap+Rescale[t\[LongRightArrow]gain,bounds,{1,gap-margin}],(k-1)*gap+thick}}],
						Arrow[{{sv\[LongRightArrow]begin+sv\[LongRightArrow]length/2-mints,ymin-2,(k-1)*gap+(thick*1.5)},{t\[LongRightArrow]begin+t\[LongRightArrow]length/2-mints,(ti-1)*gap-2,(k-1)*gap+(thick*1.5)}}]
				]]
		},Boxed->False]]&/@tslices;
		{vslice,arrows,hvslices}
	]]&/@Vr],{i,Length[V]},{k,Length[Q]}]//Flatten,walls//Flatten,blocks//Flatten]
];
Options[CompetitionGraph] = {FaultTolerence -> 0};
CompetitionGraph[Vb_, nQ_, res_,resnc_, OptionsPattern[]]:=Block[{Vbf, deplst,keygrp,vbgrp,pltdt,pltdtnc,depplots,minrso, minrsp},
	Vbf=Vb[[All, 1;;(nQ/(1+OptionValue[FaultTolerence]))]];
	vbgrp=GroupBy[#,#["request"]&]&/@GroupBy[Flatten[Vbf], #["address"]&];
	
	(**
	 * returns list of requests that are being served at a given time span
	 *)
	DependencyGraphData[sln_] := Block[{slnf, slna, slng, slnr, slnt, slnv, slngg, slngt}, 
		slnf = (Pieces[#][[2]]&/@sln)/.{_,_,x_}/; x > (nQ/(1+OptionValue[FaultTolerence]))->Nothing;
		slngg = SortBy[#, #[[2]]&]&/@GroupBy[slnf, First];
		slngt = With[{req=#[[3]], addr=Complex[#[[1]], #[[2]]]},Block[{vs=vbgrp[addr],prt},
			prt = Select[Normal@vs, First[#[[2]]][ "request"] == req &][[1, 2, 1]];
			(FromUnixTime[#*60]&/@Values@prt[[{"begin", "end"}]])->req
		]]&/@#&/@slngg;
		slna = Merge[#, Identity]&/@slngt;
		slna
	];
	UtilizationGraphData[sln_] := Block[{slnf, slna, slng, slnr, slnt, slnv, slngg, slngt}, 
		slnf = (Pieces[#][[2]]&/@sln)/.{_,_,x_}/; x > (nQ/(1+OptionValue[FaultTolerence]))->Nothing;
		slngt = With[{resid=#[[1]], req=#[[3]], addr=Complex[#[[1]], #[[2]]]},Block[{vs=vbgrp[addr],prt},
			prt = Select[Normal@vs, First[#[[2]]][ "request"] == req &][[1, 2, 1]];
			(FromUnixTime[#*60]&/@Values@prt[[{"begin", "end"}]])->resid
		]]&/@slnf;
		slna = DeleteDuplicates[#]&/@Merge[slngt, Identity];
		KeySort[slna]
	];
	
	DependencyGraphDataMerged[] := Block[{cmpctd, blncdd, min, max, resources},
		cmpctd = DependencyGraphData[resnc];
		blncdd = DependencyGraphData[res];
		{min, max} = MinMax[Flatten[{Keys@Values[cmpctd], Keys@Values[blncdd]}]];
		resources = Union[Keys[cmpctd], Keys[blncdd]];
		If[!KeyExistsQ[cmpctd, #], 
			AppendTo[cmpctd, #-><|{min, max}->{}|>],
			With[{begin = First[Keys@cmpctd[#]][[1]], end = Last[Keys@cmpctd[#]][[2]]},
				If[begin != min, PrependTo[cmpctd[#], {min, begin} -> {}]];
				If[end   != max, AppendTo [cmpctd[#], {end, max}  -> {}]];
			]
		]&/@resources;
		If[!KeyExistsQ[blncdd, #], 
			AppendTo[blncdd, #-><|{min, max}->{}|>],
			With[{begin = First[Keys@blncdd[#]][[1]], end = Last[Keys@blncdd[#]][[2]]},
				If[begin != min, PrependTo[blncdd[#], {min, begin} -> {}]];
				If[end   != max, AppendTo [blncdd[#], {end, max}  -> {}]];
			]
		]&/@resources;
		{{min, max}, Prepend[Merge[{cmpctd, blncdd}, Identity], 0->{UtilizationGraphData[resnc], UtilizationGraphData[res]}]}
	];
	DependencyGraphDataMergedPlot[] := Block[{mdata, sdata, adata, gdata, pdata, headers, labels, plots, min, max, markcmpt, markblnc}, 
		{{min, max}, mdata} = DependencyGraphDataMerged[];
		
		sdata = With[{rnc=#[[1]], rc=#[[2]]},
			Block[{},{
				Flatten[{{#[[1, 1]], 0}, {#[[1, 1]], Length@#[[2]]}, {#[[1, 2]], Length@#[[2]]}, {#[[1, 2]], 0}} & /@ Normal[rnc], 1],
				Flatten[{{#[[1, 1]], 0}, {#[[1, 1]], Length@#[[2]]}, {#[[1, 2]], Length@#[[2]]}, {#[[1, 2]], 0}} & /@ Normal[rc], 1]
			}]
		]&/@mdata;
		adata = #[[1]] -> #[[2]] & /@ # & /@ # & /@ sdata;
		gdata = #&/@({#[[1]], #[[2]]}& /@ # & /@ # & /@ adata);
		
		headers = Prepend[ConstantArray[{"time", "requests served"}, Length[adata]], {"time", "resources utilized"}];
		labels  = Prepend[ConstantArray["Resource Usage", Length[adata]], "Resource Utilization"];
		
		markcmpt = Graphics[{Green, Disk[]}];
		markblnc = Graphics[{Red, Disk[]}];
		
		plots = DateListPlot[#, 
		   PlotLegends -> Placed[{"Compact", "Balanced"}, Above],
		   Joined -> True,
		   Filling -> Bottom,
		   Axes -> True,
		   PlotStyle -> {Green, Red}, 
		   PlotMarkers -> {{markcmpt, 0.05}, {markblnc, 0.03}},
		   Frame -> {True, True, False, False},
		   FrameLabel -> With[{label = First[headers]},  headers = Delete[headers, 1]; Style[#, 16]&/@label],
		   PlotLabel ->  With[{label = First[labels]},  labels = Delete[labels, 1]; label],
		   PlotRange -> {{min, max}, {0, (nQ/(1+OptionValue[FaultTolerence]))}},
		   FrameTicks -> {All, Range[(nQ/(1+OptionValue[FaultTolerence]))]}
		] & /@ gdata
	];
	KeySort@DependencyGraphDataMergedPlot[]
];
Options[ScheduleSlices]={FaultTolerence -> 0, Visualize->True,ShowGraphs->True,ShowLegends->True,HighlightBlocks->False,ExportCDF->Null,ExportSymbols->Null,ShowLayerEdges->True};
ScheduleSlices[R_, Qr_, x_Symbol, y_Symbol, \[Epsilon]_, OptionsPattern[]] := Block[{Q, Vb, V, S, B, continuity, vused = {}, gains = {}, constraints = {}, vertices = {}, edges = {}, capacity, blocks, gedges, graphs, gunion, ha, variables, domains, res, timing, solution, vis, qlease, qactions, vweights, eweights, resunc, solutionnc, dependencygraph, tolcns = {}},
 	Q = Join @@ ConstantArray[Qr, OptionValue[FaultTolerence] + 1]; (* Create duplicate sets of requests one for each fault tolerence degree *)
	S=CreateResourceSlices[R,Q];
	B=BoundingSliceVertices[S,Q,x,y];
	V=SliceVertices[S,Q,x,y,\[Epsilon]];
	Vb=V;
	Do[With[{Vq=B[[i,k]]},Block[{vq=#}, 																											(* add the initial and terminal vertices to Vb *)
		If[vq\[LongRightArrow]type=="initial",With[{dest=(vq\[LongRightArrow]destination)\[LongRightArrow]location},
			AppendTo[Vb[[dest\[LongRightArrow]resource,k,dest\[LongRightArrow]instance,dest\[LongRightArrow]slice,"sources"]],
				<|"address"->vq\[LongRightArrow]address,"symbol"->vq\[LongRightArrow]source,"edge"->vq\[LongRightArrow]edge,"gain"->1,"loss"->0|>
			]],
			If[vq\[LongRightArrow]type=="terminal",With[{src=(vq\[LongRightArrow]source)\[LongRightArrow]location},
				AppendTo[Vb[[src\[LongRightArrow]resource,k,src\[LongRightArrow]instance,src\[LongRightArrow]slice,"targets"]],
					<|"address"->vq\[LongRightArrow]address,"symbol"->vq\[LongRightArrow]destination,"edge"->vq\[LongRightArrow]edge,"gain"->1,"loss"->0|>
				]
			]]
		]]&/@Vq;
	],{i,Length[R]},{k,Length[Q]}];
	continuity=Table[With[{Vk=(Vb[[All,k]]//Flatten)[[All,{"symbol","sources","targets"}]]},
		Block[{ts=#\[LongRightArrow]targets,sedges=#\[LongRightArrow]sources[[All,"edge"]],tedges=#\[LongRightArrow]targets[[All,"edge"]]},
			If[Length[sedges]>0||Length[tedges]>0,
				AppendTo[vertices,#\[LongRightArrow]symbol];
				AppendTo[vused,#\[LongRightArrow]symbol==Total[tedges]];
				Do[
					AppendTo[edges,ts[[t]]\[LongRightArrow]edge];
					AppendTo[gains,(ts[[t]]\[LongRightArrow]gain-ts[[t]]\[LongRightArrow]loss) ts[[t]]\[LongRightArrow]edge]
				,{t,Length[ts]}];
				Total[sedges]==Total[tedges]
			,Nothing]
		]&/@Vk
	],{k,Length[Q]}]//Flatten;
	Do[
		AppendTo[vertices,Subscript[x,0,0,k]];
		AppendTo[vused,Subscript[x,0,0,k]==1];
	,{k,Length[Q]}];
	Do[
		AppendTo[vertices,Subscript[x,-1,-1,k]];
		AppendTo[vused,Subscript[x,-1,-1,k]==1];
	,{k,Length[Q]}];
	Do[With[{bedges=GroupBy[B[[All,k]]//Flatten,#\[LongRightArrow]type&]},
		With[{initials=bedges\[LongRightArrow]initial,terminals=bedges\[LongRightArrow]terminal},
			AppendTo[edges,#]&/@initials[[All,"edge"]];
			AppendTo[continuity,Total[initials[[All,"edge"]]]==1];
			AppendTo[continuity,Total[terminals[[All,"edge"]]]==1];
			With[{initial=#,dest=(#\[LongRightArrow]destination)\[LongRightArrow]location},
				PrependTo[gains,initial\[LongRightArrow]edge*Vb[[dest\[LongRightArrow]resource,k,dest\[LongRightArrow]instance,dest\[LongRightArrow]slice]]\[LongRightArrow]gain]
			]&/@initials;
	]],{k,Length[Q]}];

	variables=Join[vertices,edges];
	domains=0<=#<=1&/@variables;
	graphs = Block[{gs,gsu,gsp,sizes=<||>,esizes=<||>},
		gedges=Table[Block[{re,be},
			re=With[{Vk=(Vb[[All,k]]//Flatten)[[All,{"symbol","targets", "gain", "length"}]]},
				With[{self=#\[LongRightArrow]symbol,ts=#\[LongRightArrow]targets[[All,"symbol"]]},
					Function[targets,AppendTo[esizes,#\[LongRightArrow]edge -> Delete[#,"edge"]]&/@targets][#\[LongRightArrow]targets[[All,{"edge", "gain", "loss"}]]];
					AppendTo[sizes, self->( #\[LongRightArrow] gain*#\[LongRightArrow]length)];
					If[Length[ts]>0,self\[DirectedEdge]#&/@ts,Nothing]
			]&/@Vk];
			be=With[{bedges=GroupBy[B[[All,k]]//Flatten,#\[LongRightArrow]type&]},
				With[{initials=bedges\[LongRightArrow]initial,terminals=bedges\[LongRightArrow]terminal},
					Function[elist,
						PrependTo[esizes,(#\[LongRightArrow]edge -> <|"gain"->sizes[#\[LongRightArrow]vertex],"loss"->0|>)]&/@elist
					][<|"vertex"-> ((#\[LongRightArrow]destination)\[LongRightArrow]symbol),"edge"->(#\[LongRightArrow]edge)|>&/@initials];
					(#\[LongRightArrow]source \[DirectedEdge] (#\[LongRightArrow]destination)\[LongRightArrow]symbol) &/@initials
			]];
			Join[be,re]
		],{k,Length[Q]}];
		(* eweights: a dictionary with edge in the key and a pair of gain and loss on that edge as value e.g. (u\[DirectedEdge]v \[Rule]<|"gain"\[Rule]g, "loss"\[Rule]l|>) *)
		eweights = Association[Block[{pieces,source,destination},
			pieces = Pieces[#][[2]];
			source = Subscript[x,pieces[[1]],pieces[[2]],pieces[[5]]];
			destination = Subscript[x,pieces[[3]],pieces[[4]],pieces[[5]]];
			source\[DirectedEdge]destination -> esizes[#]
		]&/@Keys[esizes]];
		vweights=Association[<|#-> <|"gain"->sizes[#]|>|>&/@Keys[sizes]];
		(* gs: list of regular mathematica graph with edge weights as (e\[Rule]loss - t\[Rule]gain) *)
		gs=Table[Graph[gedges[[k]]//Flatten,VertexLabels->"Name",GraphLayout->"LayeredDrawing",EdgeWeight->((eweights[#]\[LongRightArrow]loss-eweights[#]\[LongRightArrow]gain)&/@(gedges[[k]]//Flatten))],{k,Length[Q]}];
		gsu=Table[Graph[Block[{e,s,sp,t,tp},
			e =#;
			{s,t}=List@@e;
			sp=Pieces[s];
			tp=Pieces[t];
			If[(sp[[2,1]]== 0 && sp[[2,2]]==0)||(tp[[2,1]]==-1&&tp[[2,2]]==-1),
				If[sp[[2,1]]== 0 && sp[[2,2]]==0,
					DirectedEdge[Subscript[x,sp[[2,1]],sp[[2,2]],sp[[2,3]]],Subscript[x,tp[[2,1]],tp[[2,2]]]],
					If[tp[[2,1]]==-1&&tp[[2,2]]==-1,
						DirectedEdge[Subscript[x,sp[[2,1]],sp[[2,2]]],Subscript[x,tp[[2,1]],tp[[2,2]],tp[[2,3]]]]
					]
				],
				DirectedEdge[Subscript[x,sp[[2,1]],sp[[2,2]]],Subscript[x,tp[[2,1]],tp[[2,2]]]]
			]
		]&/@(gedges[[k]]//Flatten),VertexLabels->"Name",GraphLayout->"LayeredDrawing",EdgeWeight->((eweights[#]\[LongRightArrow]loss-eweights[#]\[LongRightArrow]gain)&/@(gedges[[k]]//Flatten))],{k,Length[Q]}];
		(*Print[GraphUnion[gsu[[1]],gsu[[2]],VertexLabels->"Name"]];*)
		graphunion = GraphUnion[gsu[[1]],gsu[[2]],VertexLabels->"Name"];
		(* gsp: plot each graph in gs with different colors for each resources, different shapes for resource and request, vertex size is proportinal to its gain *)
		gsp = Block[{g = #,vlist=VertexList[#],lt=MinMax[Values[sizes]],VC={RGBColor[1., 0.749752, 0.501183],RGBColor[0.9948416170624096, 0.9695551064354674, 0.5226967649619281],RGBColor[0.5343102721082129, 0.9875098362783905, 0.3875381199137638],RGBColor[0.512227148190099, 0.42680966628910977`, 0.9998892092475745],RGBColor[0.90222, 0.101808, 0.198306],RGBColor[0.85, 0.9, 0.3],RGBColor[0.35957884292551023`, 0.8083001713070861, 1.],RGBColor[1., 0.749752, 0.501183],RGBColor[0.7494445735287143, 0.6918528382415734, 0.9996537797815309],RGBColor[0.9948416170624096, 0.9695551064354674, 0.5226967649619281],RGBColor[0.966153150692097, 0.42917541619317257`, 0.46]}},
			GraphPlot[g, 
				DirectedEdges->True, 
				VertexRenderingFunction->({With[{resid=Pieces[vlist[[#2]]][[2,1]]+2},
						VC[[resid]]
					],EdgeForm[Black],With[{bounding=!KeyExistsQ[sizes, vlist[[#2]]],sz=Rescale[If[KeyExistsQ[sizes, vlist[[#2]]],sizes[vlist[[#2]]],lt[[1]]], lt,{0.2,1}]},
						If[bounding,Disk[#,0.2],Rectangle[{#[[1]]-(sz/2),#[[2]]-(sz/5)},{#[[1]]+(sz/2),#[[2]]+(sz/5)}]]
					],Black,Text[vlist[[#2]],#1]}&)
			]]&/@gs;
			gsp
		];

		capacity=Block[{Vf=V//Flatten,groups,G,Gd,ccons},
			groups=GroupBy[Vf,#\[LongRightArrow]bucket&];
			G=GroupBy[Values[With[{key=#,values=groups[#]},
				Block[{rq,qr,competetion},
					rq=ResourceSubset[R,Q[[key[[4]]]]][[All,2]];	(* subset of resources that satisfy request  key[[4]]] *)
					qr=RequestSubset[R[[key[[1]]]],Q][[All,2]];		(* subset of requests  that satisfy resource key[[1]] *)
					competetion=Competetion[rq,R[[key[[1]]]],key[[2]],key[[3]]];
					(* If[competetion == 0, Print[rq];Print[key[[1]], key[[4]]];Print["SENSING HOLE SPOTTED ", FromUnixTime[#*60]&/@TimeSpan[R[[key[[1]]]],key[[2]],key[[3]]], " REQUEST ", key[[4]]]]; *)
					key-><|
						"slices"->values[[All,"symbol"]],
						"load"->Total[Table[If[MemberQ[variables,values[[it,"symbol"]]], values[[it,"length"]]*values[[it,"symbol"]],0],{it,Length[values]}]],
						"competetion"->competetion,
						"request"->key[[4]],
						"stamp"->{key[[1]],key[[2]],key[[3]]}
					|>
			]]&/@Keys[groups]],#\[LongRightArrow]stamp&];
			
			Block[{collection=Table[Values[G][[it]][[All,{"slices","competetion"}]],{it,Length[Values[G]]}],vsharing,chunks},
				vsharing=Table[With[{bucket=collection[[it]]},Total[If[#\[LongRightArrow]competetion == 0, 0, 1/#\[LongRightArrow]competetion]&/@bucket]//Ceiling],{it,Length[collection]}];
				chunks = Table[Append[#,"sharing"->vsharing[[it]]]&/@collection[[it]],{it,Length[collection]}];
				Do[Do[With[{chunk =chunks[[it]][[itt]]},(
						AppendTo[vweights[#],"competetion"-> chunk\[LongRightArrow]competetion];
						AppendTo[vweights[#],"sharing"-> chunk\[LongRightArrow]sharing];
					)&/@(chunk\[LongRightArrow]slices);
				],{itt,Length[chunks[[it]]]}],{it,Length[chunks]}];
			];
			
			blocks=Keys[G];
			Gd=With[{key=#,values=G[#]},<|
				"sharing"->Ceiling[Total[Table[If[values[[it,"competetion"]] == 0, 0, 1/values[[it,"competetion"]]],{it,Length[values]}]]],
				"slices"->Total[values[[All,"load"]]],
				"size"->R[[key[[1]]]]\[LongRightArrow]expectations[[key[[3]],3]]
			|>]&/@Keys[G];
			(*Print[Gd];*)
			ccons=With[{pslice=#},pslice\[LongRightArrow]slices<=pslice\[LongRightArrow]sharing*pslice\[LongRightArrow]size]&/@Gd;
			ccons
		];
		
		{timing,res}=Timing[Maximize[{Total[gains],Join[continuity,vused,capacity,domains]},variables,Integers]];
		resunc=Maximize[{Total[gains],Join[continuity,vused,domains]},variables,Integers];
		solution=<|
			"vertices"->Keys[(res[[2]]/.(Subscript[y,_,_,_,_,_]->_)->Nothing)/.(_->0)->Nothing],
			"edges"->Keys[(res[[2]]/.(Subscript[x,_,_,_]->_)->Nothing)/.(_->0)->Nothing]
		|>;
		solutionnc=<|
			"vertices"->Keys[(resunc[[2]]/.(Subscript[y,_,_,_,_,_]->_)->Nothing)/.(_->0)->Nothing],
			"edges"->Keys[(resunc[[2]]/.(Subscript[x,_,_,_]->_)->Nothing)/.(_->0)->Nothing]
		|>;
		Block[{svs=solution["vertices"],svsnc=solutionnc["vertices"],gs=GroupBy[Vb//Flatten,#\[LongRightArrow]symbol&],meta,qaction},
			svs=(svs/.(Subscript[x,0,0,_]-> Nothing))/.(Subscript[x,-1,-1,_]-> Nothing);
			svsnc=(svsnc/.(Subscript[x,0,0,_]-> Nothing))/.(Subscript[x,-1,-1,_]-> Nothing);
			dependencygraph=CompetitionGraph[Vb, Length@Q,svs,svsnc, FaultTolerence->OptionValue[FaultTolerence]];
			meta=GroupBy[<|"symbol"->#\[LongRightArrow]symbol,"begin"->#\[LongRightArrow]begin,"end"->#\[LongRightArrow]end, "request"->#\[LongRightArrow]request,"resource"->Re[#\[LongRightArrow]address],"address"->#\[LongRightArrow]address|>&/@(First@gs[#]&/@svs),#\[LongRightArrow]request&]//Values;
			meta=SortBy[#,#\[LongRightArrow]begin&]&/@meta;
			meta=Table[{#\[LongRightArrow]begin,#\[LongRightArrow]end,#\[LongRightArrow]resource}&/@meta[[i]],{i,1,Length[meta]}];
			meta=Table[Block[{chunk=Split[meta[[i]],#1[[3]]== #2[[3]]&]},
				{FromUnixTime[Min[#[[All,1]]]*60],FromUnixTime[Max[#[[All,2]]]*60],#[[1,3]]}&/@chunk
			],{i,1,Length[meta]}];
			qaction=Append[#,{#[[Length[#],2]],#[[Length[#],2]],0}]&/@meta;
			Do[(qaction[[i]][[it,2]]=If[it>1,qaction[[i]][[it-1,3]],0]),{i,Length[qaction]},{it,Length[qaction[[i]]]}];
			Do[qaction[[i]]=<|"time"->#[[1]],"move"->(#[[2]]->#[[3]])|>&/@qaction[[i]],{i,Length[qaction]}];
			qlease=meta;
			qactions=qaction;
		];
		(*Print[vweights];*)
		(*Print[eweights];*)
		storage = <|
			"layers"->Table[Show[LayerX[R, V[[All,k]], Q[[k]], Length@Q, x, HighlightVertices-> {solution["vertices"]}, HighlightEdges->solution["edges"], ShowEdges->OptionValue[ShowLayerEdges]]//Flatten],{k,Length[Q]}],
			"LayersCompact" -> Table[Show[LayerX[R, V[[All, k]], Q[[k]], Length@Q, x, HighlightVertices -> {solutionnc["vertices"]}, HighlightEdges -> solutionnc["edges"], ShowEdges->OptionValue[ShowLayerEdges]] // Flatten],{k,Length[Q]}],
			"LayersBalanced" -> Table[Show[LayerX[R, V[[All, k]], Q[[k]], Length@Q, x, HighlightVertices -> {solution["vertices"]}, HighlightEdges -> solution["edges"], ShowEdges->OptionValue[ShowLayerEdges]] // Flatten],{k,Length[Q]}],
			"LayersComparison" -> Table[Show[LayerX[R, V[[All, k]], Q[[k]], Length@Q, x, HighlightVertices -> {solutionnc["vertices"], solution["vertices"]}, FaultTolerence -> OptionValue[FaultTolerence], ShowEdges->OptionValue[ShowLayerEdges]] // Flatten], {k, Length[Q]}],
			"graphs"->graphs,
			"time"->timing,
			"epsilon"->\[Epsilon],
			"lease"->Table[i->(qlease[[i]]),{i,Length[qlease]}],
			"action"->Table[i->(With[{v=Values[#]}, {v[[1]],v[[2,1]],v[[2,2]]}]&/@ qactions[[i]]),{i,Length[qactions]}],
			"dependency"->Values[dependencygraph],
			"visualization"->CubeX[R,V,Q,ShowLegends->OptionValue[ShowLegends],HighlightVertices->solution["vertices"],HighlightEdges->solution["edges"],HighlightBlocks->If[OptionValue[HighlightBlocks],blocks,{}]]//Flatten,
			"VisualizationCompact"->CubeX[R,V,Q,ShowLegends->OptionValue[ShowLegends],HighlightVertices->solutionnc["vertices"],HighlightEdges->solutionnc["edges"],HighlightBlocks->If[OptionValue[HighlightBlocks],blocks,{}]]//Flatten,
			"VisualizationBalanced"->CubeX[R,V,Q,ShowLegends->OptionValue[ShowLegends],HighlightVertices->solution["vertices"],HighlightEdges->solution["edges"],HighlightBlocks->If[OptionValue[HighlightBlocks],blocks,{}]]//Flatten
		|>;
		If[Head@OptionValue[ExportSymbols]==String,Block[{sympath=OptionValue[ExportSymbols]<>If[FileExtension[OptionValue[ExportSymbols]] == "mx", "",".mx"]},
			Export[sympath,storage];
		]];
		vis=TabView[{"Problem"->Block[{},
				TabView[{
					"Description"->TabView[{
						"Resources"->Dataset[R],
						"Requests"->Dataset[Q]
					}],
					"Layers"->TabView[Table[Show[LayerX[R,V[[All,k]],Q[[k]], Length@Q, x]//Flatten],{k,Length[Q]}],ControlPlacement->Left],
					"Visualization"->Show[CubeX[R,V,Q,ShowLegends->OptionValue[ShowLegends]]//Flatten],
					"Graph"->TabView[graphs,ControlPlacement->Left],
					"Graph Union" -> graphunion
				},ImageSize->Full]
			],"Solution"->Block[{},
				TabView[{
					"Description"->Row[{
						Column[{"Vertices",solution["vertices"]//Dataset}],
						Column[{"Edges",solution["edges"]//Dataset}],
						Spacer[5],
						Column[{
							Row[{
								Column[{"Timing",InputField[timing,FieldSize->Tiny],"Variables",InputField[Length[variables],FieldSize->Tiny],"Constraints",InputField[Length[Join[continuity,vused,capacity,domains]],FieldSize->Tiny],Spacer[{50,80}]}],
								Column[{"Constraints",Pane[<|"continuity"->continuity,"vused"->vused,"capacity"->capacity,"domains"->domains|>//Dataset,ImageSize->{Automatic,200}]}]
							}],
							Column[{"\[Epsilon] (maximum hop cost)",\[Epsilon],"Logistic",Plot[HopLossF[l,\[Epsilon]],{l,0,100},Frame->True,FrameLabel->{"Length","Cost"}, ImageSize->Medium]}]
						}],
						Column[{
							Column[{"Lease", MenuView[Table[i->(qlease[[i]]//MatrixForm),{i,Length[qlease]}]]}],
							Column[{"Action", MenuView[Table[i->(qactions[[i]]//Dataset),{i,Length[qactions]}]]}]
						}]
					}],
					"LayersC" -> TabView[Table[Show[LayerX[R, V[[All, k]], Q[[k]], Length@Q, x, HighlightVertices -> {solutionnc["vertices"], solution["vertices"]}, FaultTolerence -> OptionValue[FaultTolerence], HighlightEdges -> {solutionnc["edges"], solution["edges"]}, ShowEdges->OptionValue[ShowLayerEdges], ShowLegends->OptionValue[ShowLegends]] // Flatten], {k, Length[Q]}]],
					"Layers" -> TabView[Table[TabView[{
						"Compact" -> Show[LayerX[R, V[[All, k]], Q[[k]], Length@Q, x, HighlightVertices -> {solutionnc["vertices"]}, HighlightEdges -> solutionnc["edges"], ShowEdges->OptionValue[ShowLayerEdges]] // Flatten],
						"Balanced" -> Show[LayerX[R, V[[All, k]], Q[[k]], Length@Q, x, HighlightVertices -> {solution["vertices"]}, HighlightEdges -> solution["edges"], ShowEdges->OptionValue[ShowLayerEdges]] // Flatten]
	     			}, ControlPlacement -> Top], {k, Length[Q]}], ControlPlacement -> Bottom],
					"Visualization" -> TabView[{
						"Compact" -> Show[CubeX[R, V, Q, ShowLegends -> OptionValue[ShowLegends], HighlightVertices -> solutionnc["vertices"], HighlightEdges -> solutionnc["edges"], HighlightBlocks -> If[OptionValue[HighlightBlocks], blocks, {}]] // Flatten],
				   		"Balanced" -> Show[CubeX[R, V, Q, ShowLegends -> OptionValue[ShowLegends], HighlightVertices -> solution["vertices"], HighlightEdges -> solution["edges"], HighlightBlocks -> If[OptionValue[HighlightBlocks], blocks, {}]] // Flatten]
				   }, ControlPlacement -> Top],
					"Dependency"->TabView[dependencygraph]
				},ImageSize->Full]
		]},ImageSize->Full];
		If[Head@OptionValue[ExportCDF]==String,Block[{cdfpath=OptionValue[ExportCDF]<>If[FileExtension[OptionValue[ExportCDF]] == "cdf", "",".cdf"]},
			UsingFrontEnd @ Export[cdfpath,vis];
		]];
		If[OptionValue[Visualize],Print[vis]];
		solution
];


End[]
EndPackage[];



