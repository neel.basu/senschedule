(* ::Package:: *)

BeginPackage["SenSchedule`"]
OscillatingFit::usage ="Transforms a time series to a characteristic expression which is a set of N consecutive Partitions where each partition is expressed as a tuple (begin, end, size, weight, flactuation)";
MakeZone::usage = "Transforms a set of K characteristic expressions into zones by intersecting the partitions";
PlotPartitions::usage = "Plot partitions of a sernsor";
FindSchedule::usage = "Finds a schedule for a given set of partitions of multiple sensors Using Shortest Path";
Resource::usage = "";
Begin["`Private`"]


Unprotect[LongRightArrow];
LongRightArrow[obj_,property_]:=obj[ToString[property]];
Protect[LongRightArrow];

Options[MakeResource]={id->0, type->Null, availability->Null, context->Null, contextuals->{}, capabilities->{}, expectations->{} UseHilbertCurve->False};
MakeResource[OptionsPattern[]]:=Block[{},
	<|"id"->OptionValue[id], "type"->OptionValue[type], "availability"->OptionValue[availability], "context"->OptionValue[context], "contextuals"->OptionValue[contextuals], "capabilities"->ScalarizeF[OptionValue[capabilities]], "expectations"->OptionValue[expectations]|>
];

Options[MakeRequest]={id->0, type->Null, slot->Null, context->Null, contextuals->{}, capabilities->{}, UseHilbertCurve->False};
MakeRequest[OptionsPattern[]]:=Block[{},
	<|"id"->OptionValue[id], "type"->OptionValue[type], "slot"->OptionValue[slot], "context"->OptionValue[context], "contextuals"->OptionValue[contextuals], "capabilities"->ScalarizeF[OptionValue[capabilities]]|>
];

Options[Resource]={UseHilbertCurve->False};
Resource[id_, type_, context_, availability_, caps_, expectations_, OptionsPattern[]]:=Block[{},
	{id, type, context, availability, ScalarizeF[caps], expectations}
];
RType[r_]:=r[[2]];
RContext[r_]:=r[[3]];
RSlots[r_]:=r[[6]];
Options[Request]={UseHilbertCurve->False};
Request[id_, type_, context_, slot_, caps_, OptionsPattern[]]:=Block[{},
	{id, type, context, slot, ScalarizeF[caps]}
];
(**
 * Takes a 2xN matrix and returns a 2x1 matrix
 *)
ScalarizeF[M_] := Block[{l=Length[M]},
		{\!\(
\*UnderoverscriptBox[\(\[Product]\), \(i = 1\), \(l\)]
\*SuperscriptBox[\(M[\([i, 1]\)]\), \(M[\([i, 2]\)]\)]\), \!\(
\*UnderoverscriptBox[\(\[Product]\), \(i = 1\), \(l\)]
\*SuperscriptBox[\(M[\([i, 1]\)]\), \(M[\([i, 3]\)]\)]\)}
	];
FeasibleF[r_, q_]:=Block[{},
	r[[2]]==q[[2]] && 
	r[[3]]==q[[3]] && 
	OverlapsF[r[[4]], q[[4]]][[1]] &&
	CapFeasibleF[r[[5]], q[[5]]]
];
CapFeasibleF[capr_, capq_]:=Mod[capq[[1]],capr[[1]]] == 0 && Mod[capr[[2]], capq[[2]]]==0;
WastageF[capr_, capq_]:= (capq[[1]]*capr[[2]])/(capq[[2]]*capr[[1]]);
OverlapsF[availability_, slot_]:=Block[{s=availability[[1]],\[Delta]=availability[[2]],k=availability[[3]], ts=slot[[1]],te=slot[[2]], m, l},
	m= (ts-s)-Floor[(ts-s)/k]k;
	l= ts+Min[te-ts, \[Delta]-m];
	{m<=\[Delta],l}
];
Wastage[capr_, capq_]:=\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{
RowBox[{"WastageF", "[", 
RowBox[{"capr", ",", " ", "capq"}], "]"}], 
RowBox[{"CapFeasibleF", "[", 
RowBox[{"capr", ",", " ", "capq"}], "]"}]},
{"\[Infinity]", "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\);
FeasibilityMatrix[R_, Q_]:= Table[FeasibleF[R[[i]], Q[[j]]], {i, Length[R]}, {j, Length[Q]}];
WastageMatrix[R_, Q_]:= Table[Wastage[R[[i]], Q[[j]]], {i, Length[R]}, {j, Length[Q]}];
AppropriatenessMatrix[R_, Q_]:= Table[FeasibilityMatrix[R, Q]//Boole/WastageMatrix[R, Q], {i, Length[R]}, {j, Length[Q]}];
FavourabilityCube[R_,Q_]=Block[{A=AppropriatenessMatrix[R,Q]},
	Table[A[[i, k]]*R[[i, 6]][[j, 4]],{i, Length[R]}, {j, Length[R[[i, 6]][[All, 4]]]}, {k, Length[Q]}]
];
WallQ=Function[{range,length},\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{
RowBox[{"-", "1"}], 
RowBox[{
RowBox[{"First", "[", "range", "]"}], "==", "1"}]},
{"1", 
RowBox[{
RowBox[{"Last", "[", "range", "]"}], "==", "length"}]},
{"0", "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\)];
SplitF=Function[{row,pos},
	Block[{bk,ek,ep,bq,s},
		bk=row[[1]];ek=row[[2]];ep=pos;bq=pos;s=row[[3]];
		With[{\[Rho]p=s/(pos-bk),\[Rho]q=s/(ek-pos)},({
		 {bk, ep, s/\[Rho]p, row[[4]], row[[5]]},
		 {bq, ek, s/\[Rho]q, row[[4]], row[[5]]}
		})]
	]
];
MergeF=Function[{l},With[{
	b=Min[l[[All,1]]],
	e=Max[l[[All,2]]],
	p=Total[l[[All,3]]],
	w=\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(Length[l]\)]\(l[\([i, 3]\)]*l[\([i, 4]\)]/Total[l[\([All, 3]\)]]\)\),
	f=\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(Length[l]\)]\(l[\([i, 3]\)]*l[\([i, 5]\)]/Total[l[\([All, 3]\)]]\)\)
	},{b,e,p,w,f}]
];
FixF=Function[{begin,range,end,matrix},
	Block[{k,s=Flatten[{begin,range,end}],l,mat=matrix},
		l=MinMax[(s/.Null->Nothing)];
		k=MergeF[matrix[[First[range];;Last[range]]]];
		k=MergeF[{If[TrueQ[begin==Null],Nothing,matrix[[begin]]],k,If[TrueQ[end==Null],Nothing,matrix[[end]]]}];
		mat[[First[l]]]=k;
		mat[[First[l]+1;;Last[l]]]=0Range[Length[k]];
		mat
	]
];
DistributeF=Function[{begin,range,end,matrix},
	Block[{m=With[{pl=matrix[[begin,3]],pr=matrix[[end,3]],	a=matrix[[First[range],1]],b=matrix[[Last[range],2]]},(pr*a+pl*b)/(pl+pr)],db,adb,i,v,r,left,right,mat=matrix},
		db=Select[matrix[[First[range];;Last[range],1]]-m,#1<=0&];
		adb=MapThread[Flatten[{#1,#2}]&,{db, Range[Length[db]]+begin}];
		{v,i}=Last[adb];(*index of the nearest partition*)
		r=SplitF[matrix[[i]],m];
		left=MergeF[Append[matrix[[begin;;i-1]],r[[1]]]];
		right=MergeF[Prepend[matrix[[i+1;;end]],r[[2]]]];
		mat[[begin]]=left;
		mat[[end]]=right;
		mat[[begin+1;;end-1]]=0Range[Length[left]];
		mat
	]
];
SmoothF=Function[{mat,row},
	Block[{sum,r=Reverse[row]},
		sum=Total[row[[All,3]]];
		If[sum>=threshold,
			FixF[Null,{First[r][[6]],Last[r][[6]]},Null,mat],
			Block[{wall},
				wall=WallQ[r[[All,6]],Length[mat]];
				Switch[wall,
					-1,FixF[Null,{First[r][[6]],Last[r][[6]]},Last[r][[6]]+1,mat],
					+1,FixF[First[r][[6]]-1,{First[r][[6]],Last[r][[6]]},Null,mat],
					0,If[EvenQ[Length[r]],
						DistributeF[First[r][[6]]-1,{First[r][[6]],Last[r][[6]]},Last[r][[6]]+1,mat],
						FixF[First[r][[6]]-1,{First[r][[6]],Last[r][[6]]},Last[r][[6]]+1,mat]
					]
				]
			]
		]
	]
];
PlotPartitions[data_, partitions_, smooth_, t_Symbol:Null, fourierfit_:Null, apprxfit_:Null]:=Block[
{mean,intersections=Prepend[partitions[[All,2]],0],texts,cuts={},switch=0,plottables={}},
	mean = Mean[data];
	AppendTo[cuts,#]&/@{{#[[1]],#[[3]]+mean},{#[[2]],#[[3]]+mean}}&/@smooth[[All,{1,2,4}]];
	texts=Tooltip[ Text[StringForm["(`1`) `2`/`3`",switch+1,#[[4]]+mean,#[[5]]], 
		{#[[2]]-#[[3]]/2, (#[[4]]+mean)*(Mod[switch++,3]+1.2)}, Background->LightYellow],
		#[[4]]+mean]&/@smooth;
	AppendTo[plottables,ListLinePlot[data,PlotStyle->Red]];
	If[Not[t===Null],
		If[Not[fourierfit===Null],AppendTo[plottables,Plot[fourierfit+mean,{t, 0,Length[data]},PlotStyle->{Green,{}}]]];
		If[Not[apprxfit===Null],AppendTo[plottables,Plot[apprxfit+mean,{t, 0,Length[data]},PlotStyle->{Pink,{}}]]];
	];
	AppendTo[plottables,ListLinePlot[cuts,PlotStyle->Blue]];
	AppendTo[plottables,Graphics[texts]];
	Show[plottables,GridLines->{intersections, {}},GridLinesStyle->Directive[Magenta,Dashed]]
];
Options[OscillatingFit]={MeasureFlactuation->True, ThresholdValue->64,ApproximationHarmonics->8};
OscillatingFit[data_, t_Symbol, OptionsPattern[]]:=Block[
{d,mean,complex , mpf, magphases, freqs, eqs, fourierfit, unsorted, sorted, apprxfit, intersections, partitions={},regions, smoothregions, cuts={},apprx=OptionValue[ApproximationHarmonics]},
	mean = Mean[data];
	d= data - mean;
	complex = Fourier[d, FourierParameters-> {1,-1}];
	mpf = Block[
	{i=0, T =Length[d](* observation time *)},
	Function[x, { Abs[x], Arg[x], i++/T}]/@complex
	 ] ;
	mpf = mpf[[1;;Floor[Length[mpf]/2]]];
	eqs = Function[{f}, With[
	{amplitude = f[[1]]/Length[d], phase = f[[2]], w=f[[3]]},
	2*amplitude* Cos[2 \[Pi] w*t+phase]
	]]/@ mpf ;
	unsorted=Thread[{mpf[[All, 1]], eqs}];
	sorted = Reverse[SortBy[unsorted, #[[1]] &]];
	(*Print[unsorted//MatrixForm, sorted //MatrixForm];*)
	fourierfit = \!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(Length[eqs]\)]\(eqs[\([i]\)]\)\);
	apprxfit={\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(apprx\)]\(sorted[\([i, \ 2]\)]\)\), \!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(apprx\)]\(unsorted[\([i, \ 2]\)]\)\)};
	intersections=Block[{sfreqs, gcd, period},
		sfreqs = Reverse[SortBy[mpf[[All, {1, 3}]], #[[1]] &]][[1;;apprx, 2]];
		gcd = Apply[GCD,sfreqs];
		period = gcd^-1;
		t/.Normal[Solve[Rationalize[apprxfit[[1]]]==0&&0<=t<=period, t, Reals]]
	];
	PrependTo[intersections, 0];
	AppendTo[intersections, Length[d]];
	partitions = Table[{intersections[[i]], intersections[[i+1]]}, {i, 1, Length[intersections]-1}];
	(*Print[partitions//MatrixForm];*)
	regions=Block[{integral,weights},
		integral=1/(v-u) \!\(
\*SubsuperscriptBox[\(\[Integral]\), \(u\), \(v\)]\(Rationalize[fourierfit] \[DifferentialD]t\)\);
		weights = Block[{b,e, w,s},
			{b,e}=#;
			w=integral/.{u->b,v->e};
			s=If[TrueQ@OptionValue[MeasureFlactuation],
				t/.Normal[Solve[Rationalize[fourierfit]==w && b<=t<=e, t, Reals]],
				0
			];
			(*s = t/.Normal[Solve[Rationalize[fourierfit]\[Equal]w && b\[LessEqual]t\[LessEqual]e, t, Reals]];*)
			{e-b, w,Length[s]/(e-b)}
		]&/@partitions;
		MapThread[Flatten[{#1,#2}]&,{partitions, weights}]
	];
	(*Print[regions//MatrixForm];*)
	smoothregions=Block[{annotated,sortedregions, diffs, max,threshold, small, g},
		annotated=MapThread[Flatten[{#1,#2}]&,{regions, Range[Length[regions]]}];
		sortedregions = Reverse[SortBy[annotated, #[[3]] &]];
		diffs = Differences[sortedregions[[All, 3]]];
		threshold=OptionValue[ThresholdValue];
		max = Length[(sortedregions[[All, 3]]-threshold)/.x_/;x<0->Nothing];
		small =Reverse[SortBy[sortedregions[[max+1;;Length[sortedregions]]], #[[6]]&]];
		g=Block[{groups={},back=0},
			Function[row,
				If[row[[6]]+1!=back,
					AppendTo[groups,{row}],
					groups[[Length[groups]]]=Append[groups[[Length[groups]]],row]
				];
				back=row[[6]];
			]/@small;
			groups
		];
		Fold[SmoothF,regions,g] /. ConstantArray[0,Length[First@regions]]->Nothing
	];
	(*Print[regions//MatrixForm,smoothregions//MatrixForm];*)
	(*{"partitions"-> regions, "smooth"-> smoothregions,"fit"->fourierfit,"approximated"->apprxfit[[1]]}*)
	Print[PlotPartitions[data,regions,smoothregions,t,fourierfit,apprxfit[[1]]]];
	smoothregions+ConstantArray[{0, 0, 0, mean, 0}, Length[smoothregions]]
];
MakeZone[ps_]:= Block[{ends=#[[All,2]]&/@ps,intersections,lines,zones,f},
	intersections = Sort[Union[Flatten[ends]]];
	f=Function[{psi},
		Block[{u},
			u = Complement[intersections,psi[[All,2]]];
			Fold[Block[{points=#1[[All,2]],pos,w,splitted},
				pos = Length[(points-#2)/.x_/;x>0->Nothing]+1;
				splitted=SplitF[#1[[pos,All]], #2];
				ArrayReshape[#1/.#1[[pos,All]]->splitted,{Length[#1]+1,5}]
			]&,psi,u]
		]
	];
	zones =f/@ps
];
HopCostF[ps_,i_,j_,p_,q_,\[Epsilon]_]:=Block[{source,target},
	source = ps[[i,j]];(*j'th partition row of i'th resource*)
	target = ps[[p,q]];(*q'th partition row of p'th resource*)
	\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{"0", 
RowBox[{"p", "==", "i"}]},
{"\[Epsilon]", "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False], 
RowBox[{
RowBox[{
RowBox[{"target", "[", 
RowBox[{"[", "1", "]"}], "]"}], "<=", 
RowBox[{"source", "[", 
RowBox[{"[", "2", "]"}], "]"}]}], "&&", " ", 
RowBox[{
RowBox[{"target", "[", 
RowBox[{"[", "2", "]"}], "]"}], ">", " ", 
RowBox[{"source", "[", 
RowBox[{"[", "2", "]"}], "]"}], " "}]}]},
{"\[Infinity]", "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\)
];
Options[HopCostEdges]={IncludeInfinity->False,WeightCombined->False};
HopCostEdges[ps_,\[Epsilon]_,OptionsPattern[]]:=Block[{nr = Length[ps] (*n resources*),np=Length/@ps (*n partitions*),edges={},c=0},
	For[i=1,i<=nr,i++,
		For[j=1,j<= np[[i]],j++,
			For[p=1,p<=nr,p++,
				For[q=1,q<= np[[p]],q++,
					Block[{h},
						h=HopCostF[ps,i,j,p,q,\[Epsilon]];
						If[OptionValue[WeightCombined],h= (ps[[p,q,4]]*ps[[p,q,3]])-h];
						If[OptionValue[IncludeInfinity]||Abs[h]!=\[Infinity],
							AppendTo[edges,
								{Complex[i,j]\[DirectedEdge]Complex[p,q], h, Complex[i,j]\[DirectedEdge]Complex[p,q]->Placed[h, (4+(Mod[c++,3]))/8]}
							]
						]
					]
				]
			]
		]
	];
	edges
];

HighlightGraphR[g_, vertices_] := Block[{vt, gt},
    vt = ToString /@ vertices;
    gt = Fold[SetProperty[{#1,#2},EdgeLabels->Block[{e=#2,vp=List@@#2},
        ToString[First[vp]]\[DirectedEdge]ToString[Last[vp]]->PropertyValue[{#1,e},EdgeWeight]
    ]&]&, g, EdgeList[g]];    
    gt = VertexReplace[gt, Thread[VertexList[gt] -> ToString /@ VertexList[gt]]];    
    HighlightGraph[gt, PathGraph[vt, DirectedEdges->True]]
]

VertexShapeF[{xc_, yc_}, name_, {w_, h_}] := {
	LightYellow, 
	Rectangle[{xc - w, yc - h}, {xc + w, yc + h}], 
	Inset[Style[Text[Piecewise[{
		{"S", (First[#]==0 && Last[#]==0)}, 
		{"T", (First[#]==-1 && Last[#]==-1)}
	}, Subscript["P", First[#],Last[#]]]]&[{Re[name], Im[name]}], 12, Black], {xc, yc}]
};

Options[HopCostGraph]={IncludeInfinity->False,WeightCombined->False,NegeteEdgeWeight->False};
HopCostGraph[ps_,\[Epsilon]_,OptionsPattern[]]:=Block[{edges={},vertices={},vlabels={},nr = Length[ps] (*n resources*),np=Length/@ps (*n partitions*)},
	edges=HopCostEdges[ps,\[Epsilon],IncludeInfinity->OptionValue[IncludeInfinity],WeightCombined->OptionValue[WeightCombined]];
	For[i=1,i<=nr,i++,
		For[j=1,j<= np[[i]],j++,
			AppendTo[vertices, (* map of vertex weights *)
				If[OptionValue[WeightCombined], (* when weightcombined is on cost is put on edges, so vertex weight (reward) is 0; otherwise reward is s*w *)
					0, 
					Complex[i,j]->(ps[[i,j,4]]*ps[[i,j,3]])
				]
			];
			AppendTo[vlabels,
				If[OptionValue[WeightCombined],
					Complex[i,j]->Subscript["P", i, j],
					Complex[i,j]->ps[[i,j,4]]Subscript["P", i, j]
				]
			];
		]
	];
	Block[{},
		For[i=1,i<=nr,i++,
			AppendTo[edges, (* a list of tuple {edge, weight, edge\[Rule]weight} where weight = s*w  *)
				{Complex[0,0]\[DirectedEdge]Complex[i,1], (ps[[i,1,4]]*ps[[i,1,3]]), Complex[0,0]\[DirectedEdge]Complex[i,1]->(ps[[i,1,4]]*ps[[i,1,3]])}
			];
			AppendTo[edges,
				{Complex[i,Length[ps[[i]]]]\[DirectedEdge]Complex[-1,-1], 0, Complex[i,Length[ps[[i]]]]\[DirectedEdge]Complex[-1,-1]->0}
			];
			AppendTo[vertices,#]&/@{Complex[0,0]->0,Complex[-1,-1]->0};
			AppendTo[vlabels,#]&/@{Complex[0,0]->"S",Complex[-1,-1]->"T"};
		]
	];
	Graph[edges[[All,1]],
		EdgeWeight->If[OptionValue[NegeteEdgeWeight],-1edges[[All,2]],edges[[All,2]]],
		EdgeLabels->edges[[All,3]],
		(*VertexLabels->vlabels,*)
		VertexWeight->vertices,
		VertexShapeFunction -> VertexShapeF,
		VertexSize->Medium
	]
];
Options[FindSchedule]={HighlightShortestPath->False};
FindSchedule[ps_,\[Epsilon]_]:=Block[{zones,g,sg,v},
	zones=MakeZone[ps];
	Print[#//MatrixForm]&/@zones;
	g=HopCostGraph[zones,\[Epsilon], IncludeInfinity->False,WeightCombined->True,NegeteEdgeWeight->True];
	v=FindShortestPath[g,Complex[0,0],Complex[-1,-1]];
	sg=HighlightGraph[g,Style[PathGraph[v,DirectedEdges->True], {Red, Thick}]];
	Print[sg];
	Block[{p={Re[#], Im[#]}},
		Piecewise[{
			{"S", First[p]==0},
			{"T", First[p]==-1}
		}, p]
	]&/@ v 
];
FindFirstSlot[resource_, request_]:=Block[{i, rslots=resource[[6]], qslot=request[[4]]},
	For[i=1,i<=Length[rslots],i++, 
		With[{rbegin=rslots[[i,1]], qbegin=qslot[[1]]},
			If[rbegin >= qbegin, Break[], Nothing]
		]
	];
	If[i>Length[rslots], -1, i]
];
FindLastSlot[resource_, request_]:=Block[{i, rslots=resource[[6]], qslot=request[[4]]},
	For[i=1,i<=Length[rslots],i++, 
		With[{rbegin=rslots[[i,1]], qend=qslot[[2]]},
			If[rbegin >= qend, Break[], Nothing]
		]
	];
	If[i>Length[rslots], -1, i]
];

Options[OverlapsF]={Visualization->False};
OverlapsF[availability_,request_,OptionsPattern[]]:=With[{s=availability["start"],k=availability["recurrence"],d=availability["duration"],b=request[[1]],e=request[[2]]}, Block[{n,m,u,v,g},
	n=Ceiling[(b-s)/k];
	{m,u}=\!\(\*
TagBox[GridBox[{
{"\[Piecewise]", GridBox[{
{
RowBox[{"{", 
RowBox[{"0", ",", "s"}], "}"}], 
RowBox[{"b", "<=", "s"}]},
{
RowBox[{"{", 
RowBox[{
RowBox[{"n", "-", "1"}], ",", "b"}], "}"}], 
RowBox[{
RowBox[{"n", "-", "1"}], ">", 
FractionBox[
RowBox[{"b", "-", 
RowBox[{"(", 
RowBox[{"s", "+", "d"}], ")"}]}], "k"]}]},
{
RowBox[{"{", 
RowBox[{"n", ",", 
RowBox[{"s", "+", 
RowBox[{"n", " ", "k"}]}]}], "}"}], "True"}
},
AllowedDimensions->{2, Automatic},
Editable->True,
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.84]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
Selectable->True]}
},
GridBoxAlignment->{"Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxItemSize->{"Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}},
GridBoxSpacings->{"Columns" -> {Offset[0.27999999999999997`], {Offset[0.35]}, Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {Offset[0.2], {Offset[0.4]}, Offset[0.2]}, "RowsIndexed" -> {}, "Items" -> {}, "ItemsIndexed" -> {}}],
"Piecewise",
DeleteWithContents->True,
Editable->False,
SelectWithContents->True,
Selectable->False]\);
	v=Min[s+m k+d,e];
	If[OptionValue[Visualization],
		g=Block[{lines, joints,points,gres,greq,rlabels,qlabels,lossline},
			lines={Thick,Line[#]}&/@Table[{{s+i k,0},{s+i k+d,0}},{i,m-1,m+1}];
			joints={Thin,Line[#]}&/@Table[{{s+i k+d,0},{s+(i+1)k,0}},{i,m-1,m+1}];
			points=Flatten[Join[lines,joints[[;;Length[joints]-1]] ],1];
			gres=Graphics[points];
			greq=Graphics[{Thick,If[e>u,Blue,Red],Line[{{u,4},{v,4}}]}];
			rlabels=Flatten[Table[{Text[s+i k,{s+i k,1}],Text[s+i k+d,{s+i k+d,1}],Text[i,{s+i k,-0.5}]},{i,m-1,m+1}],1]//Graphics;
			qlabels={Text[b,{b,4.5}],Text[u,{u,3.5}], Text[v,{v,3.5}]}//Graphics;
			lossline=Graphics[{Dashed,Line[{{b,4},{u,4}}]}];
			Show[gres,greq,rlabels,qlabels,lossline]
		];
		Print[g];
		Print[<|"n"->n,"m"->m,"u"->u,"v"->v,"overlaps"->e>u|>];
	];
	e>v
]];
(**
 * R Vector of resources
 * Q Vector of Requests
 * C Matrix of cost of each slot
 * U Vector of Maximum affordable price for each requests
 * \[Epsilon] Scaler minimum hop cost
 * \[Gamma] Matrix of competitiveness of each slot of each resource
 **)
Schedule[R_, Q_, C_, U_, \[Epsilon]_]:=Block[{N=Length[R], P = FavourabilityCube[R, Q],\[Gamma], variables={}, cubex, edgey, startx, endx, starty, endy, constraints={}, continuity, capacity, vused, price, domains={}, objective, gain},
	(*compitivity of resource slots*)
	\[Gamma]=Table[\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(l = 1\), \(Length[R]\)]\(OverlapsF[R[\([l, 4]\)], R[\([i, 6, j, 1]\)], \ R[\([i, 6, j, 2]\)]]\)\),{i, Length[R]}, {j, Length[R[[i, 6]]]}];
	
	cubex=Table[If[P[[i,j,k]]!=0,
		Subscript[x, i,j,k],
		Nothing
	], {i, Length[R]}, {j, Length[R[[i, 6]]]}, {k, Length[Q]}];
	startx=Table[Subscript[x, 0,0,k],{k, Length[Q]}];
	endx=Table[Subscript[x, -1,-1,k],{k, Length[Q]}];
	
	edgey=Table[If[P[[i,j,k]]!=0 && P[[p,q,k]]!=0 && !(i==j && p==q),
		Subscript[y, i,j,p,q,k]->HopCostF[i,j,p,q,\[Epsilon]],
		Nothing
	],{i, Length[R]}, {j, Length[R[[i, 6]]]}, {p, Length[R]}, {q, Length[R[[i, 6]]]}, {k, Length[Q]}];
	starty=Table[With[{begin=FindFirstSlot[R[[p]], Q[[k]]]},
		If[begin > 0,Subscript[y, 0,0,p,begin,k], Nothing]
	],{p, Length[R]},{k,Length[Q]}];
	endy=Table[With[{end=FindLastSlot[R[[i]], Q[[k]]]},
		If[end > 0,Subscript[y, i,end,-1,-1,k], Nothing]
	],{i, Length[R]},{k,Length[Q]}];
	
	variables=Join[Flatten[#]&/@{cubex, startx, endx, edgey, starty, endy}];
	
	(* Continuity constraint, out degree of a vertex should be same is in degree *)		
	continuity=Table[Table[(\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(Length[R]\)]\(
\*UnderoverscriptBox[\(\[Sum]\), \(j = 1\), \(Length[R[\([i, \ 6]\)]]\)]Subscript[y, i, j, p, q, k]\)\))==(\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(Length[R]\)]\(
\*UnderoverscriptBox[\(\[Sum]\), \(j = 1\), \(Length[R[\([i, \ 6]\)]]\)]Subscript[y, p, q, i, j, k]\)\)),{p,1,Length[R]},{q,1,Length[R]}],{k,1,Length[Q]}];
	AppendTo[constraints,#]&/@Flatten[continuity];
	
	(* If an edge is used mark its source vertex as used *)	
	vused=Table[Subscript[x,i,j,k]=\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(p = 1\), \(Length[R]\)]\(
\*UnderoverscriptBox[\(\[Sum]\), \(q = 1\), \(Length[R[\([i, \ 6]\)]]\)]Subscript[y, i, j, p, q, k]\)\),{i, Length[R]}, {j, Length[R[[i, 6]]]},{k, Length[Q]}];
	AppendTo[constraints,#]&/@Flatten[vused];
	
	(* Maximum number of requests using the same slot *)
	capacity=Table[\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(k = 1\), \(Length[Q]\)]\(Subscript[x, i, j, k]\)\) <= Ceiling[Length[Q]/\[Gamma][[i,j]]],{i, Length[R]}, {j, Length[R[[i, 6]]]}];
	AppendTo[constraints,#]&/@Flatten[capacity];
	
	(* Restrict total price of all allocated resources for request k to Subscript[U, k] (max affordable price for request k) *)
	price=Table[\!\(
\*UnderoverscriptBox[\(\[Sum]\), \(i = 1\), \(Length[R]\)]\(
\*UnderoverscriptBox[\(\[Sum]\), \(j = 1\), \(Length[R[\([i, \ 6]\)]]\)]Subscript[x, i, j, k] C[\([i, j]\)] \(R[\([i, 6]\)]\)[\([j, 3]\)]\)\) <= U[[k]],{k, Length[Q]}];
	AppendTo[constraints,#]&/@Flatten[price];
	
	AppendTo[domains,Table[Subscript[x, 0,0,k]==1,{k,Length[Q]}]//Flatten];
	AppendTo[domains,Table[Subscript[x, -1,-1,k]==1,{k,Length[Q]}]//Flatten];
	AppendTo[domains,0<=#<=1]&/@Flatten[cubex];
	AppendTo[domains,0<=#<=1]&/@Keys[Flatten[edgey]];
	
	AppendTo[constraints, domains];
	
	gain=Plus[Total[With[{i,j,k},
		Cases[{#}, Subscript[y, 0,0,i,j,k]];
		(R[[i,6]][[j,3]])#
	]&/@Flatten[starty]],
	Total[With[{i,j,p,q,k,c},
		Cases[{#}, (Subscript[y, i_,j_,p_,q_,k_]->c_) :> {i,j,p,q,k,c}];
		(R[[p,6]][[q,3]]-c)#
	]&/@Flatten[edgey]]];
	
	objective=gain;
	
	Print[variables];
	Print[objective];
	Print[constraints];
	
	Maximize[Flatten[{objective, constraints}], variables, Integers]
];


End[]
EndPackage[];



